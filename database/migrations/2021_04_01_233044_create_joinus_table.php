<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJoinusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('joinus', function (Blueprint $table) {
            $table->id();
            $table->text('content'); 
            $table->string('file')->nullable();
            $table->unsignedBigInteger('user_id'); 
            $table->timestamps();
        });

        Schema::create('testimotinial', function (Blueprint $table) {
            $table->id();
            $table->text('content'); 
            $table->string('link')->nullable();
            $table->string('lang'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('joinus');
        Schema::dropIfExists('testimotinial');
    }
}
