<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('roles', function (Blueprint $table) {
            $table->id('id');
            $table->string('title'); // 1 user // 2 instructor / 3 admin
            $table->timestamps();
        });


        Schema::create('users', function (Blueprint $table) {
            $table->id('id');
            $table->string('name', 30)->nullable();
            $table->string('email')->unique();
            $table->string('password', 60)->nullable();
            $table->integer('phone')->nullable();
            $table->integer('status')->default(0);
            $table->string('photo')->nullable();
            $table->integer('gender')->default(1); // 1 - homme / 2 - femme
            $table->string('validation')->nullable();
            $table->integer('country_id'); 
            $table->string('city'); 
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });


        Schema::create('user_roles', function (Blueprint $table) {
            $table->id('id');
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_roles');
        Schema::dropIfExists('users');
        Schema::dropIfExists('roles');
    }
}
