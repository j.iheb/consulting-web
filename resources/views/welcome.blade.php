<!DOCTYPE html>
<html id="ctl00_HTMLTag" lang="ar">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, maximum-scale=1" />
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;700;900&display=swap" rel="stylesheet">
    <link href="{{ asset('css/frontOffice/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="/images/favicon.ico" />
    <link href="{{ asset('css/frontOffice/bootstrap-rtl.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/frontOffice/animate.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/frontOffice/owl.carousel.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/frontOffice/owl.theme.default.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/frontOffice/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/frontOffice/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/frontOffice/StyleUp.css') }}" rel="stylesheet" type="text/css" />

    <meta property="fb:pages" content="255721971299115" />
    <meta property='og:title' content=' اتصـل بنـا' />
    <meta property='og:type' content='website' />
    <meta property='og:image' content='https://manaraa.com/manaraa.png' />
    <meta property='og:description' content='في حال وجود أي ملاحظة تود أن تصل للإدارة العامة عبئ نموذج تواصل معنا' />
    <meta name='description' content='في حال وجود أي ملاحظة تود أن تصل للإدارة العامة عبئ نموذج تواصل معنا' />
    <meta name='keywords' content='المغترب,المغترب للاستشارات,مركز المغترب للاستشارات,دليل الطالب,خدمات الطلبة,المغترب لخدمات الطلبة,استشارات, ترجمة,ابحاث, مقالات,مؤتمرات,ترجمة مواقع,ترجمة كتب,ترجمة مؤتمرات,تنقيح أعمال مترجمة,ترجمة أبحاث,ترجمة رسائل ماجستير,رسائل دكتوراه,تدقيق لغوي,ترجمة أفلام,ترجمة مقالات,مؤسسة المغترب للترجمة, المغترب للترجمة'>
    <meta property='fb:app_id' content='1426295687698066'>
    <meta name='twitter:card' content='photo'>
    <meta name='twitter:url' content='https://manaraa.com/'>
    <meta name='twitter:title' content=' اتصـل بنـا'>
    <meta name='twitter:description' content='في حال وجود أي ملاحظة تود أن تصل للإدارة العامة عبئ نموذج تواصل معنا'>
    <meta name='twitter:image' content='https://manaraa.com/mlogo.png'>
    <meta name='twitter:image:width' content='600'>
    <meta name='twitter:image:height' content='600'>
    <meta name='twitter:site' content='@Manara4Reports'>
    <meta name='twitter:creator' content='@Manara4Reports'>
    <meta name="”robots”" content="index, follow" />
    <link href="{{ asset('css/frontOffice/_Css.css') }}" rel="stylesheet" />
    <meta name="p:domain_verify" content="7dd1d75701c17407b1b574466bb43a1b" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
   

    <meta property="og:site_name" content="مؤسسة المغترب للإستشارات‎" />
    <meta property="article:author" content="https://www.facebook.com/Almanara.Consultancy.Institution" />
    <meta property="article:publisher" content="https://www.facebook.com/Almanara.Consultancy.Institution" />
    <title>
        اتصـل بنـا - المغترب للاستشارات
    </title>
</head>

<body class="inner-page">
   <!--  <a href="#" style="font-size:30px;" class="whatapp-fixed-icon fa fa-whatsapp"></a> -->
    <header>
            <div class="top-bar">
                <div class="container">
                    <div class="top-bar-wrap">
                        <div class="contact-links">
                            <a href="#">
                                <i class="fa fa-whatsapp"></i>
                                <span> +21621241251</span>
                            </a>
                            <a href="#">
                                <i class="fa fa-envelope-o"></i>
                                <span><span class="__cf_email__" data-cfemail="0e417c6a6b7c4e636f606f7c6f6f206d6163"> me.jabri.iheb@gmail.com </span></span>
                            </a>
                        </div>
                        <div class="top-bar-menu">
                            <a href="/ContactUS">تواصل معنا</a>
                            <a href="/FAQ">الأسئلة الشائعة</a>
                        </div>
                        <a id="ctl00_hlLang" class="lang" href="#">English</a>
                    </div>
                </div>
            </div>
            <div class="main-nav">
                <div class="container">
                    <div class="nav-wrap">
                        <a href="/" class="logo">
                            <img src="/img/logo.png" alt="المغترب للاستشارات" /></a>
                        <a href="/" class="menu-open visible-xs visible-sm"><i class="fa fa-bars"></i></a>
                        <div class="navbar-content" style="z-index:999;">
                            <a href="#" class="menu-close visible-xs visible-sm"><i class="fa fa-times"></i></a>
                            <ul class="main-menu">
                                <li><a href="/">الرئيسية</a></li>
                                <li class="dropdown-item">
                                    <a href="javascript:;" class="about">عن المؤسسة</a>
                                    <ul class="dropdown-menu-item">
                                        <li><a href="#">من نحن</a></li>
                                        <li><a href="#">قالوا عنا</a></li>
                                        <li><a href="#">هل هذا غش؟</a></li>
                                        <li><a href="#">معلومات عن المختصين</a></li>
                                        <li><a href="#">لا تكن ضحية المواقع المخادعة</a></li>
                                        <li><a href="#">انضم للعمل معنا</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">المدونة</a></li>
                                <li><a href="#">المكتبة</a></li>
                                <li><a href="#">خدماتنا</a></li>
                                <li><a href="#">الضمانات</a></li>
                                <li><a href="#">طلب خدمة</a></li>
                            </ul>
                            <a href="#" id="ctl00_pnlLogin" class="login"><i class="fa fa-user-o"></i>تسجيل الدخول </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section class="breadcrumb-section wow fadeIn">
            <div class="container">
                <div class="breadcrumb-wrap">
                    <a href="#">الرئيسية</a>
                    <i>/</i>
                    <span>تواصل معنا </span>
                </div>
            </div>
        </section>
        <section class="inner-page-section wow fadeIn">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="row contact-info">
                            <div class="col-xs-12 col-md-4">
                                <a href="#" class="info-item wow fadeInUp">
                                    <i class="fa fa-arrow-circle-o-up"></i>
                                    <span>طلب خدمة </span>
                                </a>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <a href="#" class="info-item wow fadeInUp" data-wow-delay="0.2s">
                                    <i class="fa fa-whatsapp"></i>
                                    <span>00970595510045</span>
                                </a>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <a href="#" class="info-item wow fadeInUp" data-wow-delay="0.4s">
                                    <i class="fa fa-envelope-o"></i>
                                    <span><span class="__cf_email__" data-cfemail="460934222334062b2728273427276825292b">me.jabri.iheb@gmail.com</span></span>
                                </a>
                            </div>
                        </div>
                        <div class="contact-form row">
                            <div class="col-xs-12 form-text wow fadeInUp">
                                <div class="MyFont">
                                    <div style="text-align: center;">
                                        <div style="line-height: 20.8px; text-align: center;"><span style="font-size:14px"><strong>أهلا وسهلا بكم، يمكنكم التواصل معنا من خلال تعبئة النموذج التالي</strong></span></div>
                                        <div style="line-height: 20.8px; text-align: center;"><span style="font-size:14px"><strong>أما الطلبات تتم من خلال صفحة &quot;<a href="#"><span style="color:#008000">طلب خدمة</span></a>&quot; فقط.</strong></span></div>
                                        <div style="line-height: 20.8px; text-align: center;"><span style="font-size:14px"><strong>هل ترغب في العمل معنا؟ <a href="#"><span style="color:#A52A2A">انضم إلينا</span>.</a></strong></span></div>
                                    </div>
                                </div>
                            </div>
                            <div id="ctl00_ContentPlaceHolder1_UpdatePanel1">
                                <div class="col-md-6 form-group wow fadeInUp">
                                    <label for="username">
                                        الإسم رباعي
                                        <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator4" class="color-red" style="color:Red;display:none;">*</span></label>
                                    <input name="ctl00$ContentPlaceHolder1$TxtName" type="text" id="ctl00_ContentPlaceHolder1_TxtName" class="input-item" />
                                </div>
                                <div class="col-md-6 form-group wow fadeInUp" data-wow-delay="0.2s">
                                    <label for="email">
                                        البريد الإلكتروني
                                        <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator5" class="color-red" style="color:Red;display:none;">*</span>
                                        <span id="ctl00_ContentPlaceHolder1_RegularExpressionValidator2" class="color-red MyFont" style="color:Red;display:none;">الرجاء إدخال بريد إلكتروني صحيح</span></label>
                                    <input name="ctl00$ContentPlaceHolder1$TxtEmail" type="text" id="ctl00_ContentPlaceHolder1_TxtEmail" class="input-item" />
                                </div>
                                <div class="col-md-12 form-group wow fadeInUp" data-wow-delay="0.4s">
                                    <label for="subject">
                                        عنوان الرسالة
                                        <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator1" class="color-red" style="color:Red;display:none;">*</span></label>
                                    <input name="ctl00$ContentPlaceHolder1$TxtSubject" type="text" id="ctl00_ContentPlaceHolder1_TxtSubject" class="input-item" />
                                </div>
                                <div class="col-md-12 form-group wow fadeInUp">
                                    <label for="msg">
                                        تفاصيل الرسالة
                                        <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator2" class="color-red" style="color:Red;display:none;">*</span></label>
                                    <textarea name="ctl00$ContentPlaceHolder1$TxtDetails" rows="5" cols="20" id="ctl00_ContentPlaceHolder1_TxtDetails" class="input-item"></textarea>
                                </div>
                          
                                <div class="col-md-12 form-group wow fadeInUp">
                                    <input type="submit" name="ctl00$ContentPlaceHolder1$btnSend" value="ارســال" id="ctl00_ContentPlaceHolder1_btnSend" class="contact-btn btn btn-bordered" />
                                    <span id="ctl00_ContentPlaceHolder1_lblMsg" class="MyFont" style="font-weight: bold; margin-right: 50px; color: Green;"></span>
                                    <div id="ctl00_ContentPlaceHolder1_UpdateProgress1" style="display:none;">
                                        <img src="#" alt="loading" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer>
            <div class="top-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-4">
                            <div class="footer-widget">
                                <h2 class="footer-widget-title">المغترب للاستشارات</h2>
                                <div class="footer-text">
                                    <p>
                                        نحن مؤسسة تقدم خدمات البحث العلمي. نساعد الباحثين وطلبة الدراسات العليا في المهام التي تصعب عليهم في الرسالة. ونقدم خدمات الترجمة ونخطط في المستقبل للعمل في مجال التدريب.
                                    </p>
                                    <ul>
                                        <li><a href="/">الرئيسية </a></li>
                                        <li><a href="/About">من نحن </a></li>
                                        <li><a href="/Terms">شروط وأحكام</a></li>
                                        <li><a href="/Privacy">سياسة الخصوصية</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="footer-widget">
                                <h2 class="footer-widget-title">بيانات التواصل</h2>
                                <div class="footer-text">
                                    <p class="MyFont">
                                        الطلب والاستفسار عن الخدمات يتم من خلال <a href="https://manaraa.com/CustomersLogin" style="color:#57C8EB">التسجيل في موقعنا</a> و"<a href="https://manaraa.com/OrdersRequest" style="color:#57C8EB">طلب خدمة جديدة</a>"
                                        أو من خلال الواتس على الرقم <a href="https://goo.gl/BhVvDk" style="color:#57C8EB">00970595510045</a> كما يمكنكم التواصل معنا عبر الإيميل  <a href="#" style="color:#57C8EB"><span class="__cf_email__" data-cfemail="266954424354664b4748475447470845494b">[email&#160;protected]</span></a> وفي حال وجود أي ملاحظة تود أن تصل للإدارة العامة عبئ نموذج "<a href="https://manaraa.com/ContactUS"
                                            style="color:#57C8EB">تواصل معنا</a>"</p>
                                </div>
                                <div class="social-media">
                                    <a href="https://business.facebook.com/manaraao/?business_id=1588413977919462"><i class="fa fa-facebook"></i></a>
                                    <a href="https://twitter.com/manaraao"><i class="fa fa-twitter"></i></a>
                                    <a href="https://www.linkedin.com/in/manaraao"><i class="fa fa-linkedin"></i></a>
                                    <a href="https://www.youtube.com/channel/UCgvONsa2Meri__ADC7KjKIg/videos"><i class="fa fa-youtube"></i></a>
                                    <a href="https://www.instagram.com/manaraao/"><i class="fa fa-instagram"></i></a>
                                </div>
                                <div id="ctl00_EmailListUserAdd_Palen1" >
                                    <div id="ctl00_EmailListUserAdd_UpdatePanelEmail">
                                        <div id="ctl00_EmailListUserAdd_pnlEmailList" >
                                            <div class="maillist">
                                                <h3>القائمة البريدية</h3>
                                                <p>أضف بريدك الإلكتروني لدينا ليصلك جديدنا</p>
                                                <div style="display: flex; align-items: center; margin-top: 20px; width: 300px; max-width: 100%;">
                                                    <input name="ctl00$EmailListUserAdd$Txt_Email" type="text" id="ctl00_EmailListUserAdd_Txt_Email" placeholder="youremail@mail.com" />
                                                    <input type="submit" name="ctl00$EmailListUserAdd$btnEmailAdd" value="إضافة" 
                                                        id="ctl00_EmailListUserAdd_btnEmailAdd" style="width: 80px; height: 40px; text-align: center; background: #5893dd; color: #ffffff; transition: all 0.3s; outline: none; border: none; padding: 0;" />
                                                </div>
                                                <p class="MyFont">
                                                    <span id="ctl00_EmailListUserAdd_lblMsg" style="font-weight: bold;"></span>
                                                    <span id="ctl00_EmailListUserAdd_RequiredFieldValidator2" class="color-red" style="color:Red;display:none;"></span>
                                                    <span id="ctl00_EmailListUserAdd_RegularExpressionValidator1" class="color-red" style="color:Red;display:none;">الرجاء إدخال بريد إلكتروني صحيح</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="footer-widget">
                                <h2 class="footer-widget-title">تابعنا على تويتر</h2>
                                <div class="twitter-widget">
                                    <div class="c-line-left hide"></div>
                                    <div id="ctl00_pnlTwitterAr">

                                        <a class="twitter-timeline" data-lang="ar" data-height="260" data-width="280" data-tweet-limit="1" data-theme="dark" data-link-color="#57C8EB" href="https://twitter.com/Manara4Reportss" data-chrome="noheader nofooter noscrollbar noborders transparent">Tweets by Manara4Reportss</a>
                                   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-footer">
                <div class="container">
                    <div class="col-xs-12 col-md-5">
                        <div class="copyrights">جميع الحقوق محفوظة للمنارة للاستشارات <b>2010</b> - <b>2021</b> &copy; </div>
                    </div>
                    <div class="col-xs-12 col-md-7">
                        <div class="footer-imgs">
                            <ul class="list-unstyled list-inline pull-left">
                                <li>
                                    <img src="https://www.manaraa.com/images/MasterCard.jpg" alt="We accept mada Pay" title="توفير الدفع عن طريق مدى"></li>
                                <li>
                                    <img src="https://www.manaraa.com/images/MasterCard.jpg" alt="We accept MasterCard" title="نوفر الدفع عبر الماستر كارد"></li>
                                <li>
                                    <img src="https://www.manaraa.com/images/MasterCard.jpg" alt="We accept PayPal" title="نوفر الدفع عبر الباى بال"></li>
                                <li>
                                    <img src="https://www.manaraa.com/images/MasterCard.jpg" alt="We accept Visa" title="نوفر الدفع عبر الفيزا كارد"></li>
                                <li>
                                    <a href="//www.dmca.com/Protection/Status.aspx?ID=f63f81d3-adfc-47fa-a0a3-065a3246c5fb" title="DMCA.com Protection Status" class="dmca-badge">
                                        <img src="//images.dmca.com/Badges/_dmca_premi_badge_4.png?ID=f63f81d3-adfc-47fa-a0a3-065a3246c5fb" alt="DMCA.com Protection Status"></a>
                                    <script src="//images.dmca.com/Badges/DMCABadgeHelper.min.js">
                                    </script>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
      
  
 
    <script type="text/javascript" src="{{ asset('js/frontOffice/jquery-1.9.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/script.js?c=1') }}"></script>

</body>

</html>