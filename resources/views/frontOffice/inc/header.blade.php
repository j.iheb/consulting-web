   <!--  <a href="#" style="font-size:30px;" class="whatapp-fixed-icon fa fa-whatsapp"></a> -->
<script>
   @if(isset($errors) and count($errors) > 0)
    $(function () {
        @foreach ($errors->all() as $error)
        toastr.warning('{{ $error }}');
        @endforeach
    });
    @endif
</script>    

   <header  
 @if(in_array($lang, config('app.rtl-languages')))
      dir="rtl"
   @else 
   dir="ltr"
   @endif  
   
   >
            <div class="top-bar" style="height:45px">
                <div class="container">
                    <div class="top-bar-wrap">
                        <div class="contact-links">
                            <a href="https://wa.me/+923009367790">
                                <i class="fa fa-whatsapp"></i>
                                <span>   +923009367790 </span>
                            </a>
                            <a href="mailto:info@almughtrib.com">
                                <i class="fa fa-envelope-o"></i>
                                <span>  info@almughtrib.com  </span>
                            </a>
                        </div>
                        <div class="top-bar-menu">
                            <a href="{{ route('showContact', ['lang' => $lang]) }}"> {{ucfirst(trans('lang.contact_us'))}} </a>
                            <a href="{{ route('showFaq', ['lang' => $lang])}}" > {{ucfirst(trans('lang.faq'))}} </a>
                          
                        </div> 
   
                        @foreach(Config::get('app.languages') as $acronym => $data)
                        <a id="ctl00_hlLang" class="lang" href="{{ changeLocaleInRoute(Route::current(), $acronym) }}" style="margin-left:5px">{{  $data['title']  }}</a>
                        @endforeach
                        
                      
                    </div>
                </div>
            </div>
            <div class="main-nav">
                <div class="container">
                    <div class="nav-wrap">
                        <a href="{{route('showHome',['lang' => $lang])}}" class="logo">
                            <img src="{{asset('img/logo-ss.png')}}" alt="المغترب للاستشارات"  style="width:200px;"/></a>
                        <a href="/" class="menu-open visible-xs visible-sm"><i class="fa fa-bars"></i></a>
                        <div class="navbar-content" style="z-index:999;">
                            <a href="#" class="menu-close visible-xs visible-sm"><i class="fa fa-times"></i></a>

                            @if ($lang == 'en')

                            <!-- english -->

                            <ul class="main-menu">
                                <li><a href="{{ route('showHome',['lang' => $lang]) }}">{{ ucfirst( App\Modules\General\Models\Page::find(11)->name )}}</a></li>
                                <li class="dropdown-item">
                                    <a href="javascript:;" class="about"> {{ ucfirst( App\Modules\General\Models\Page::find(10)->name )}} </a>

                                    <ul class="dropdown-menu-item">
                                        <li><a href="{{ route('showAbout',['lang' => $lang])}}"> {{ ucfirst( App\Modules\General\Models\Page::find(1)->name )}}   </a></li>
                                        <li><a href="{{ route('showTestimonial',['lang' => $lang])}}"> {{ ucfirst(App\Modules\General\Models\Page::find(3)->name)}} </a></li>
                                        <li><a href="{{ route('showTeam',['lang' => $lang])}}"> {{ ucfirst(App\Modules\General\Models\Page::find(4)->name)}} </a></li>
                                        <li><a href="{{ route('showJoinUs',['lang' => $lang])}}"> {{ucfirst( App\Modules\General\Models\Page::find(5)->name)}}  </a></li> 
                                    </ul>

                                </li>
                                 <li><a href="{{route('showBlog',['lang' => $lang])}}">{{ucfirst( App\Modules\General\Models\Page::find(6)->name)}} </a></li>
                                <li><a href="{{route('showServices',['lang' => $lang])}}"> {{ucfirst( App\Modules\General\Models\Page::find(7)->name)}}  </a></li>
                                <li><a href="{{route('showAddConsultation',['lang' => $lang])}}"> {{ucfirst( App\Modules\General\Models\Page::find(8)->name)}} </a></li>
                                  @auth
                                        @if (checkAdministratorRole(auth()->user()))                    
                                             <li><a href="{{route('showManagerHome',['lang' => $lang])}}"> {{ucfirst(trans('lang.dashboard'))}} </a></li>
                                        @endif
                                  <li>  <a href="{{route('showUserConsulationsList',['lang' => $lang])}}"  >  {{ucfirst( App\Modules\General\Models\Page::find(9)->name)}} </a></li>
                                @endauth
                           
                            </ul>

                            @else 


                            <!-- arabic -->
                       


                            <ul class="main-menu">
                                <li><a href="{{ route('showHome',['lang' => $lang]) }}">{{ ucfirst( App\Modules\General\Models\Page::find(11)->name_ar )}}</a></li>
                                <li class="dropdown-item">
                                    <a href="javascript:;" class="about"> {{ ucfirst( App\Modules\General\Models\Page::find(10)->name_ar )}} </a>

                                    <ul class="dropdown-menu-item">
                                        <li><a href="{{ route('showAbout',['lang' => $lang])}}"> {{ ucfirst( App\Modules\General\Models\Page::find(1)->name_ar )}}   </a></li>
                                        <li><a href="{{ route('showTestimonial',['lang' => $lang])}}"> {{ ucfirst(App\Modules\General\Models\Page::find(3)->name_ar)}} </a></li>
                                        <li><a href="{{ route('showTeam',['lang' => $lang])}}"> {{ ucfirst(App\Modules\General\Models\Page::find(4)->name_ar)}} </a></li>
                                        <li><a href="{{ route('showJoinUs',['lang' => $lang])}}"> {{ucfirst( App\Modules\General\Models\Page::find(5)->name_ar)}}  </a></li> 
                                    </ul>

                                </li>
                                 <li><a href="{{route('showBlog',['lang' => $lang])}}">{{ucfirst( App\Modules\General\Models\Page::find(6)->name_ar)}} </a></li>
                                <li><a href="{{route('showServices',['lang' => $lang])}}"> {{ucfirst( App\Modules\General\Models\Page::find(7)->name_ar)}}  </a></li>
                                <li><a href="{{route('showAddConsultation',['lang' => $lang])}}"> {{ucfirst( App\Modules\General\Models\Page::find(8)->name_ar)}} </a></li>
                                  @auth
                                        @if (checkAdministratorRole(auth()->user()))                    
                                             <li><a href="{{route('showManagerHome',['lang' => $lang])}}"> {{ucfirst(trans('lang.dashboard'))}} </a></li>
                                        @endif
                                  <li>  <a href="{{route('showUserConsulationsList',['lang' => $lang])}}"  >  {{ucfirst( App\Modules\General\Models\Page::find(9)->name_ar)}} </a></li>
                                @endauth
                           
                            </ul>


                            @endif









                            @auth 
                            <a id="ctl00_UCLogin1_btnLogout" href="{{route('handleLogout',['lang' => $lang])}}" class="login">
                            <i class="fa fa-lock"></i> {{ ucfirst(trans('lang.logout')) }}</a>

                            @else 
                            <a href="{{route('showLogin',['lang' => $lang])}}" id="ctl00_pnlLogin" class="login"><i class="fa fa-user-o"></i> {{ ucfirst(trans('lang.login')) }} </a>
                           @endauth
                        


                            
                       </div>
                    </div>
                </div>
            </div>
        </header>