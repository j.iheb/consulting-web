

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, maximum-scale=1" />
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;700;900&display=swap" rel="stylesheet">



   
    <link rel="shortcut icon" href="{{asset('img/logo 01.png')}}" />


    @if ($lang =="ar")
    <link href="{{ asset('css/frontOffice/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/frontOffice/bootstrap-rtl.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/frontOffice/animate.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/frontOffice/owl.carousel.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/frontOffice/owl.theme.default.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/frontOffice/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/frontOffice/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/frontOffice/StyleUp.css') }}" rel="stylesheet" type="text/css" />
    <meta name="”robots”" content="index, follow" />
    <link href="{{ asset('css/frontOffice/_Css.css') }}" rel="stylesheet" />
    @else  
    <link href="https://manaraa.com/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://manaraa.com/css/animate.css" rel="stylesheet" type="text/css" />
    <link href="https://manaraa.com/css/owl.carousel.min.css" rel="stylesheet" type="text/css" />
    <link href="https://manaraa.com/css/owl.theme.default.min.css" rel="stylesheet" type="text/css" />
    <link href="https://manaraa.com/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://manaraa.com/css/style.css?v=3.2" rel="stylesheet" type="text/css" />
    <link href="https://manaraa.com/css/style-ltr.css?v=2" rel="stylesheet" type="text/css" />
    <link href="https://manaraa.com/css/responsive.css?v=2" rel="stylesheet" type="text/css" />
    <link href="https://manaraa.com/css/responsive-ltr.css?v=2" rel="stylesheet" type="text/css">
    <link href="https://manaraa.com/css/StyleUp.css?v=2" rel="stylesheet" type="text/css" />
    @endif

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">



    <meta property="og:site_name" content="مؤسسة المغترب للإستشارات‎" />

    <title>
        اتصـل بنـا - المغترب للاستشارات
    </title>

    <!-- <link rel="stylesheet" href="{{ asset('css/frontOffice/blueimp-gallery.min.css')}}"> -->

    <link rel="stylesheet" href="{{ asset('css/frontOffice/jquery.fileupload.css')}}">
    <link rel="stylesheet" href="{{ asset('css/frontOffice/jquery.fileupload-ui.css')}}">
    <link href="{{ asset('css/frontOffice/mystyle.css')}}" rel="stylesheet" />


    <script type="text/javascript" src="{{ asset('js/frontOffice/jquery-1.9.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/script.js?c=1') }}"></script>

    <script src="{{ asset('plugins/jquery/jquery-v4.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset ('plugins/toastr/toastr.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.js') }}"></script>





</head>
