<!DOCTYPE html>
<html lang="en">
@yield('head')

<body class="vertical-layout vertical-menu-modern 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    {!! Toastr::render() !!}
    <link rel="stylesheet" type="text/css" href="{{ asset ('plugins/bootstrap/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontOffice/private/components.min.css') }}">

 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
            @yield('content')
      </div>
    </div>
  </div>
  <!-- END: Content-->
@include('backOffice.inc.scripts')
</body>
</html>
