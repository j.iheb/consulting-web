<!DOCTYPE html>
<html lang="en">
@yield('head')

<body>
<div class="app-admin-wrap">
    @yield('header')
    @yield('sidebar')
    <script>
    @if(isset($errors) and count($errors) > 0)
    $(function () {
        @foreach ($errors->all() as $error)
        toastr.warning('{{ $error }}');
        @endforeach
    });
    @endif
</script>
    <div class="main-content-wrap sidenav-open d-flex flex-column">
    @yield('content')
    @include('backOffice.inc.footer')
    </div>
</div>
@include('backOffice.inc.scripts')
{!! Toastr::message() !!}

</body>
</html>
