<div class="main-header">
    <div class="logo" style="font-size: 22px;">
    <a href="{{route('showHome',['lang' => $lang])}}">
      <img src="{{asset('img/logo 01.png')}}" alt="" style="margin-left: 20px;" >
    </a>  
   </div>

    <div class="menu-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>

    <div class="d-flex align-items-center">
    </div>

    <div style="margin: auto"></div>

    <div class="header-part-right">
        <!-- Full screen toggle -->
        <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
        <!-- Grid menu Dropdown -->

        <!-- User avatar dropdown -->
        <div class="dropdown">
            <div  class="user col align-self-end">
                <img @if(Auth::user()->photo) src="{{ asset( Auth::user()->photo) }}" @else src="{{ asset('img/unknown.png') }}" @endif id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <div class="dropdown-header">
                        <i class="i-Lock-User mr-1"></i> {{ Auth::user()->name }}
                    </div>

                    <a class="dropdown-item" href="{{ route('showHome',['lang' => $lang]) }}">{{ trans('lang.back_home') }}</a>
                    <a class="dropdown-item" href="{{ route('handleLogout',['lang' => $lang]) }}">{{ trans('lang.logout') }}</a>
                    @foreach(Config::get('app.languages') as $acronym => $data)
                        <a  class="dropdown-item" href="{{ changeLocaleInRoute(Route::current(), $acronym) }}" > {{  $data['title']  }}</a>
                     @endforeach
                </div>
            </div>
        </div>
    </div>

</div>