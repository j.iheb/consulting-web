<div class="side-content-wrap">
    <div class="sidebar-left open" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left navigation-main">
            <li class="nav-item @if($current == 'dashboard') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerHome',['lang' => $lang])}}">
                    <i class="nav-icon i-Bar-Chart"></i>
                    <span class="nav-text">{{ trans('lang.home') }}</span>
                </a>
                <div class="triangle"></div>
            </li>

            <li class="nav-item @if($current == 'users') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerUsers',['lang' => $lang])}}">
                    <i class="nav-icon i-Male-21"></i>
                    <span class="nav-text">{{ trans('lang.users') }}</span>
                </a>
                <div class="triangle"></div>
            </li>


            <li class="nav-item @if($current == 'adviser') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerAdvisers',['lang' => $lang])}}">
                    <i class="nav-icon i-Love-User"></i>
                    <span class="nav-text">{{ trans('lang.adviser') }}</span>
                </a>
                <div class="triangle"></div>
            </li>


            <li class="nav-item @if($current == 'officer') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerOfficers',['lang' => $lang])}}">
                    <i class="nav-icon i-Lock-User"></i>
                    <span class="nav-text">{{ trans('lang.officer') }}</span>
                </a>
                <div class="triangle"></div>
            </li>

            <li class="nav-item @if($current == 'partner') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerPartners',['lang' => $lang])}}">
                    <i class="nav-icon i-Circular-Point"></i>
                    <span class="nav-text">{{ trans('lang.our_partners')  }}  </span>
                </a>
                <div class="triangle"></div>
            </li>
            



            <li class="nav-item @if($current == 'services') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerServices',['lang' => $lang])}}">
                    <i class="nav-icon  i-Optimization"></i>
                    <span class="nav-text">{{ trans('lang.services') }}</span>
                </a>
                <div class="triangle"></div>
            </li>

            <li class="nav-item @if($current == 'blog') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerArticlesList',['lang' => $lang])}}">
                    <i class="nav-icon  i-Book"></i>
                    <span class="nav-text">{{ucfirst(trans('lang.blog'))}} </span>
                </a>
                <div class="triangle"></div>
            </li>

         

            <li class="nav-item @if($current == 'consultations') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerConsultations',['lang' => $lang])}}">
                    <i class="nav-icon i-Money1"></i>
                    <span class="nav-text">{{ trans('lang.paid_services') }}</span>
                </a>
                <div class="triangle"></div>
            </li>
         



            <li class="nav-item @if($current == 'free') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerFreeConsult',['lang' => $lang])}}">
                    <i class="nav-icon i-File-Fire"></i>
                    <span class="nav-text">{{ trans('lang.free_consultations')  }}  </span>
                </a>
                <div class="triangle"></div>
            </li>
         

         <!--   <li class="nav-item">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-Share"></i>
                    <span class="nav-text">{{ trans('lang.social_media') }}</span>
                </a>
            </li>
    -->
            <li class="nav-item @if($current == 'about') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerAboutList',['lang' => $lang])}}">
                    <i class="nav-icon i-Information"></i>
                    <span class="nav-text">{{ trans('lang.about_us')  }}  </span>
                </a>
                <div class="triangle"></div>
            </li>

            <li class="nav-item @if($current == 'testimotinial') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerTestimotinialList',['lang' => $lang])}}">
                    <i class="nav-icon i-Support"></i>
                    <span class="nav-text"> {{ucfirst(trans('lang.said_about_us'))}} </span>
                </a>
                <div class="triangle"></div>
            </li>

            <li class="nav-item @if($current == 'joinus') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerJoinUs',['lang' => $lang])}}">
                    <i class="nav-icon i-Add-User"></i>
                    <span class="nav-text">{{ trans('lang.join_us')  }}  </span>
                </a>
                <div class="triangle"></div>
            </li>

            <li class="nav-item @if($current == 'slider') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerSliders',['lang' => $lang])}}">
                    <i class="nav-icon i-Folder-Upload"></i>
                    <span class="nav-text">{{ trans('lang.slider')  }}  </span>
                </a>
                <div class="triangle"></div>
            </li>


            <li class="nav-item @if($current == 'contact') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerContacts',['lang' => $lang])}}">
                    <i class="nav-icon i-Post-Sign"></i>
                    <span class="nav-text">{{ trans('lang.contact_us')  }}  </span>
                </a>
                <div class="triangle"></div>
            </li>

            <li class="nav-item @if($current == 'faq') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerFaqs',['lang' => $lang])}}">
                    <i class="nav-icon i-Ticket"></i>
                    <span class="nav-text">{{ trans('lang.faq')  }}  </span>
                </a>
                <div class="triangle"></div>
            </li>

            <li class="nav-item @if($current == 'page') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerPages',['lang' => $lang])}}">
                    <i class="nav-icon i-Globe"></i>
                    <span class="nav-text">{{ trans('lang.pages')  }}  </span>
                </a>
                <div class="triangle"></div>
            </li>

            

            <li class="nav-item @if($current == 'our-service') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerOurServices',['lang' => $lang])}}">
                    <i class="nav-icon i-Dropbox"></i>
                    <span class="nav-text">{{ trans('lang.our_business')  }}  </span>
                </a>
                <div class="triangle"></div>
            </li>


            <li class="nav-item @if($current == 'countries') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerCountries',['lang' => $lang])}}">
                    <i class="nav-icon i-Dropbox"></i>
                    <span class="nav-text">{{ trans('lang.countries')  }}  </span>
                </a>
                <div class="triangle"></div>
            </li>


        </ul>
    </div>

    <div class="sidebar-left-secondary" data-perfect-scrollbar data-suppress-scroll-x="true">
        <!-- Submenu Dashboards -->
        <ul class="childNav" data-parent="courses">
            <li class="nav-item">
                <a class="" href="#">
                    <i class="nav-icon i-Loop"></i>
                    <span class="item-name">{{ trans('lang.under_review') }}</span>
                </a>
            </li>

        </ul>
        <ul class="childNav" data-parent="courses">
            <li class="nav-item">
                <a class="" href="#">
                    <i class="nav-icon i-Full-View-Window"></i>
                    <span class="item-name">{{ trans('lang.online') }}</span>
                </a>
            </li>

        </ul>
        <ul class="childNav" data-parent="webinars">
            <li class="nav-item">
                <a class="" href="#">
                    <i class="nav-icon i-Loop"></i>
                    <span class="item-name">{{ trans('lang.under_review') }}</span>
                </a>
            </li>

        </ul>
        <ul class="childNav" data-parent="webinars">
            <li class="nav-item">
                <a class="" href="#">
                    <i class="nav-icon i-Full-View-Window"></i>
                    <span class="item-name">{{ trans('lang.online') }}</span>
                </a>
            </li>

        </ul>

    </div>

    <div class="sidebar-overlay"></div>
</div>
