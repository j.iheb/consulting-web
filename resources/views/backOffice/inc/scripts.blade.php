<script src="{{ asset('js/backOffice/script.js') }}"></script>
<script src="{{ asset('js/backOffice/es5.js') }}"></script>
<script>
    @foreach ($errors->all() as $error)
    toastr.error('{{ $error }}');
    @endforeach
</script>