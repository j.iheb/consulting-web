<?php
Route::group(['module' => 'Consult', 'middleware' => ['web'], 'prefix' => '{lang}'], function () {

Route::get('/consultation/new', 'WebController@showAddConsultation')->name('showAddConsultation'); 
Route::get('/consultation/free/new', 'WebController@showAddFreeConsultation')->name('showAddFreeConsultation');
Route::get('/manager/consultations', 'WebController@showManagerConsultations')->name('showManagerConsultations')->middleware('auth', 'manager');; 
Route::post('/consultation/new', 'WebController@handleUserAddConsultation')->name('handleUserAddConsultation')->middleware('auth');
Route::get('/consultation/list', 'WebController@showUserConsulationsList')->name('showUserConsulationsList')->middleware('auth');
Route::get('/consultation/show/{id}', 'WebController@showUserSingleConsult')->name('showUserSingleConsult')->middleware('auth');  
Route::post('/message/new', 'WebController@handleUserAddMessage')->name('handleUserAddMessage')->middleware('auth'); 

Route::get('/manager/consult/free' , 'WebController@showManagerFreeConsult')->name('showManagerFreeConsult')->middleware('auth', 'manager');

Route::get('/manager/consult/switch/status/{id}', 'WebController@handleManagerSwitchConsultStatus')->name('handleManagerSwitchConsultStatus')->middleware('auth', 'manager'); 
});