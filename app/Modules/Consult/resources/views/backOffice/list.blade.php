@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection


@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => $current
])





@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins') }}/datatable/datatable.css">
<script type="text/javascript" src="{{ asset('plugins') }}/datatable/datatable.js"></script>

<script type="text/javascript">
    /* Datatables responsive */

    $(document).ready(function() {
        $('#datatable-responsive').DataTable({
            responsive: true,
            language: {
                url: "{{ asset('plugins/datatable/lang/'.\Illuminate\Support\Facades\Session::get('youfors_applocale').'.js') }}"
            }
        });
        $('.dataTables_filter input').attr("placeholder", "{{ trans('lang.search') }}...");
    });
</script>


<div class="breadcrumb">
    <h1> {{ucfirst(trans('lang.consultations'))}} </h1>
    <ul>
        <li><a href="{{ route('showManagerHome',['lang' => $lang]) }}">{{ trans('lang.dashboard') }}</a></li>
        <li> {{ucfirst(trans('lang.consultations'))}}</li>
    </ul>
</div>

<div class="separator-breadcrumb border-top"></div>

<div class="row mb-4">

    <div class="col-md-12 mb-4">
        <div class="card text-left">

            <div class="card-body">
                <table id="datatable-responsive" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{ trans('lang.title') }}</th>
                            <th>{{ trans('lang.description') }}</th>
                            <th>{{ trans('lang.owner') }}</th>
                            <th>{{ trans('lang.service') }}</th>
                            <th>{{ trans('lang.status') }}</th>
                      
                            <th>{{ trans('lang.date') }}</th>
                            <th>{{ trans('lang.action') }}</th>
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                        <th>{{ trans('lang.title') }}</th>
                            <th>{{ trans('lang.description') }}</th>
                            <th>{{ trans('lang.owner') }}</th>
                            <th>{{ trans('lang.service') }}</th>
                            <th>{{ trans('lang.status') }}</th>
                     
                            <th>{{ trans('lang.date') }}</th>
                            <th>{{ trans('lang.action') }}</th>
                        </tr>
                    </tfoot>

                    <tbody>
                        @foreach ($consultations as $consult) <tr>
                            <td> {{ $consult->title }} </td>
                            <td> {{ $consult->description }} </td>
                            <td> {{ $consult->user->name}} </td>
                            <td> {{ $consult->service->title }} </td>
                            <td>
                               @if ($consult->status == 0)
                                <span class="badge badge-pill p-2 m-1 badge-outline-danger  "> {{ucfirst(trans('lang.closed'))}}  </span> &nbsp;
                                @else 

                                <span class="badge badge-pill p-2 m-1  badge-outline-success  ">{{ucfirst(trans('lang.active'))}}   </span> &nbsp;

                                @endif
                                
                                
                                
                          
                                @if ($consult->type == 0)
                                <span class="badge badge-pill p-2 m-1 badge-outline-info  ">{{ucfirst(trans('lang.free'))}}    </span> &nbsp;
                                @else 

                                <span class="badge badge-pill p-2 m-1  badge-outline-danger  ">{{ucfirst(trans('lang.paid'))}}   </span> &nbsp;

                                @endif
                        
                        
                           </td>
                    
                            <td> {{ $consult->created_at->format('d-M-Y H:m:s') }} </td>
                        
                            <td>
                              

                            <span class="badge badge-pill  p-2 m-1" title="{{ trans('lang.delete') }}" onclick="location='{{ route('handleManagerDeleteService', ['lang'=> $lang, 'id' => $consult->id]) }}';"> <img src="  {{asset('img/remove.png')}}" alt="" style="width:25px"> </span>
                              
                              <span class="badge badge-pill p-2 m-1" title="{{ trans('lang.details') }}" onclick="location='{{ route('showUserSingleConsult',['lang'=> $lang, 'id' => $consult->id]) }}';">  <img src="  {{asset('img/chat.png')}}" alt="" style="width:25px"> </span>

                        
                              <span class="badge badge-pill  p-2 m-1" title="{{ trans('lang.switch') }}" onclick="location='{{ route('handleManagerSwitchConsultStatus',['lang'=> $lang, 'id' => $consult->id]) }}';">     <img src="  {{asset('img/switch.png')}}" alt="" style="width:25px"> </span>
                   

                            </td>
                            </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection