@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head')
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection



@section('content')
<section class="breadcrumb-section wow fadeIn">
    <div class="container">
        <div class="breadcrumb-wrap">
            <a href="{{route('showHome',['lang' => $lang])}}">  {{ ucfirst(trans('lang.home')) }}</a>
            <i>/</i>
            <span> {{ucfirst(trans('lang.service_request'))}} </span>
        </div>
    </div>
</section>
<br> 


<div class="container">
   <div class="row">
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <h3 class="panel-title"> User</h3>
             <!--   <div class="pull-right">
                    <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                </div> -->
            </div>
            <table class="table">
                <thead>
                    <tr class="filters">
                          <th>#</th>
                           <th>{{ trans('lang.title') }}</th>
                            <th>{{ trans('lang.description') }}</th>
                            <th>{{ trans('lang.service') }}</th>
                            <th>{{ trans('lang.status') }}</th>
                            <th>{{ trans('lang.date') }}</th>
                            <th>{{ trans('lang.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($consultations as $consult)
                    <tr>
                        <td> {{ $loop->index}} </td>
                        <td> {{$consult->title}}</td>
                        <td> {{$consult->description}}</td>
                        <td> {{$consult->service->title}} </td>
                        <td> 
                        @if ($consult->status == 0)
                                <span class="badge badge-pill p-2 m-1 badge-outline-danger  "> {{ucfirst(trans('lang.closed'))}}   </span> &nbsp;
                                @else 

                                <span class="badge badge-pill p-2 m-1  badge-outline-success  ">{{ucfirst(trans('lang.active'))}}  </span> &nbsp;

                                @endif
                        </td>
                        <td>{{ $consult->created_at->format('Y-m-d')}}</td>
                        <td>
                        <a href="{{route('showUserSingleConsult',['lang'=> $lang,'id' => $consult->id])}}" type="button" class="btn btn-info"> <span class="fa fa-info"></span> </a>
                        </td>
                     
                    </tr>

                    @endforeach
                
                </tbody>
            </table>
        </div>
    </div>
</div>



<style>
.filterable {
    margin-top: 15px;
}
.filterable .panel-heading .pull-right {
    margin-top: -20px;
}
.filterable .filters input[disabled] {
    background-color: transparent;
    border: none;
    cursor: auto;
    box-shadow: none;
    padding: 0;
    height: auto;
}
.filterable .filters input[disabled]::-webkit-input-placeholder {
    color: #333;
}
.filterable .filters input[disabled]::-moz-placeholder {
    color: #333;
}
.filterable .filters input[disabled]:-ms-input-placeholder {
    color: #333;
}

</style>




@endsection