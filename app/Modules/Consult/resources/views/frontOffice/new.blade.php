@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head')
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection



@section('content')

<div class="c-layout-page MyFont">


<section class="breadcrumb-section wow fadeIn" >
    <div class="container">
        <div class="breadcrumb-wrap">
            <a href="{{route('showHome',['lang' => $lang])}}"> {{ucfirst(trans('lang.home'))}} </a>
            <i>/</i>
            <span>
            @if ($type == 0)
                {{ucfirst(trans('lang.free_consultations'))}}
            @else
                {{ ucfirst(trans('lang.consultations'))}}
            @endif
        
            </span>
        </div>
    </div>
</section>
<section class="inner-page-section wow fadeIn">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="service-order">
                  <form id="form"  method="POST" action="{{route('handleUserAddConsultation', ['lang' => $lang])}}" enctype="multipart/form-data">
                    {{ csrf_field() }} 
                    <input type="hidden" name="type" value="{{$type}}">
                    <h3>

                    @if ($type == 0)
                    {{ ucfirst(trans('lang.add_consult_sentence_two'))}}
                    @else
                    {{ ucfirst(trans('lang.add_consult_sentence'))}}
                    @endif

           
                    </h3>
                    <div id="ctl00_ContentPlaceHolder1_lblMsg" class="form-group wow fadeInUp">
                    </div>
                    <div class="form-group wow fadeInUp">
                        <input name="title" type="text" id="ctl00_ContentPlaceHolder1_TxtSubject" class="input-item" data-enter="false" placeholder="{{ucfirst(trans('lang.title'))}}" />
                    </div>
                    <label class="form-group wow fadeInUp select-wrap">
                        <select name="service" class="input-item " style="height: 40px;">
                           @foreach ($services as $service)
                            <option value="{{$service->id}}">{{$service->title}}</option>
                            @endforeach
                         
                            
                        </select>
                    </label>
             
                 
                    <div class="form-group wow fadeInUp">
                        <textarea name="description" rows="5" cols="20"  class="input-item" placeholder="{{ucfirst(trans('lang.details'))}}"></textarea>
                    </div>

                  
                    <div class="form-group wow fadeInUp urgent-order">
                        <label class="radio-label"> {{ucfirst(trans('lang.urgent_request'))}}  </label>
                        <div class="radio-btns">
                            <div class="radio-item">
                                <input type="radio" name="urgent" value="0" id="no" checked style="display: none;" />
                                <label for="no"><i class="fa fa-times"></i></label>
                            </div>
                            <div class="radio-item">
                                <input type="radio" name="urgent" value="1" id="yes" style="display: none;" />
                                <label for="yes"><i class="fa fa-check"></i></label>
                            </div>
                        </div>
                   <!--     <div class="urgent-order-hint" style="display: none;">
                            يرجى الاطلاع على تفاصيل وشروط الطلبات العاجلة
                            <a href="UrgentRequest" target="_blank"> من هنا</a>
                        </div>
                        -->
                    </div>
                    <div class="form-group wow fadeInUp ">
                        <div id="fileupload">


                            <div class="row fileupload-buttonbar MyOpenUploader">
                                <div class="col-lg-12">

                                    <span class="btn btn-success fileinput-button">
                                        <i class="glyphicon glyphicon-plus"></i>
                                        <span>{{ ucfirst(trans('lang.attached_file'))}}</span>
                                        <input  type="file" name="file">
                                    </span>
                                    <button type="submit" class="btn btn-primary start" id="MyStart" style="display: none;">
                                        <i class="glyphicon glyphicon-upload"></i>
                                        <span>Start upload</span>
                                    </button>
                                    <button type="reset" class="btn btn-warning cancel" id="MyCancel" style="display: none;">
                                        <i class="glyphicon glyphicon-ban-circle"></i>
                                        <span>Cancel upload</span>
                                    </button>
                                    <button type="button" class="btn btn-danger delete" style="display: none;">
                                        <i class="glyphicon glyphicon-trash"></i>
                                        <span>Delete</span>
                                    </button>
                                    <input type="checkbox" class="toggle" style="display: none;">

                                    <span class="fileupload-process"></span>
                                </div>

                                <div class="col-lg-12 fileupload-progress fade" style="display: none;">

                                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                        <div class="progress-bar progress-bar-success" style="width: 0%;"></div>
                                    </div>

                                    <div class="progress-extended">&nbsp;</div>
                                </div>
                            </div>

                            <table role="presentation" class="table table-striped" style="margin: 0px;">
                                <tbody class="files"></tbody>
                            </table>
                            <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
                            <script id="template-upload" type="text/x-tmpl">
       
                                </script>

                            <div style="display: none;">
                                <script id="template-download" type="text/x-tmpl">
                      </script>
                            </div>
                        </div>
                    </div>
                    <div class="form-group wow fadeInUp">
                        <button type="submit" id="StartUp" class="contact-btn btn btn-bordered">{{ucfirst(trans('lang.send'))}}</button>
                    </div>
                  </form>
                </div>
            </div>
         <!--   <div class="col-md-4">
                <div class="sidebar-wrapper">
                    <div class="sidebar-item">
                        <div class="sidebar-box">
                            <div class="sidebar-title wow fadeInUp">روابط مهمة </div>
                            <ul class="sidebar-cats-ul important-links-ul">
                                <li class="wow fadeInUp"><a href="/Services">
                                        <i class="fa fa-share"></i>
                                        <span>الإطلاع على خدماتنا</span>
                                    </a></li>
                                <li class="wow fadeInUp"><a href="/Mailto.aspx">
                                        <i class="fa fa-envelope-open-o"></i>
                                        <span>الطلب عبر الاميل</span>
                                    </a></li>
                                <li class="wow fadeInUp">
                                    <a href="/WApp.aspx">
                                        <i class="fa fa-whatsapp"></i>
                                        <span>الطلب عبر الواتس اب</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="sidebar-item">
                        <div class="sidebar-box">
                            <div class="sidebar-title wow fadeInUp">خدمات المغترب للاستشارات</div>
                            <ul class="sidebar-cats-ul">
                                <li class="wow fadeInUp"><a href="/Service/9/اقتراح-عنوان-الرسالة">اقتراح عنوان الرسالة</a></li>
                                <li class="wow fadeInUp"><a href="/Service/10/كتابة-خطة-البحث">كتابة خطة البحث</a></li>
                                <li class="wow fadeInUp"><a href="/Service/12/إعداد-الإطار-النظري">إعداد الإطار النظري</a></li>
                                <li class="wow fadeInUp"><a href="/Service/14/التحليل-الإحصائي-ومناقشة-النتائج">التحليل الإحصائي ومناقشة النتائج</a></li>
                                <li class="wow fadeInUp"><a href="/Service/17/الترجمة">الترجمة</a></li>
                                <li class="wow fadeInUp"><a href="/Service/20/التدقيق-اللغوي">التدقيق اللغوي</a></li>
                                <li class="wow fadeInUp"><a href="/Service/21/تنسيق-الرسائل">تنسيق الرسائل</a></li>
                                <li class="wow fadeInUp"><a href="/Service/22/فحص-السرقة-الأدبية">فحص السرقة الأدبية</a></li>
                                <li class="wow fadeInUp"><a href="/Service/25/توفير-القبول-للدراسة-واللغة">توفير القبول للدراسة واللغة</a></li>
                                <li class="wow fadeInUp"><a href="/Service/26/توفير-المراجع-وتلخيص-الدراسات-السابقة">توفير المراجع وتلخيص الدراسات السابقة</a></li>
                                <li class="wow fadeInUp"><a href="/Service/27/توفير-أدوات-الدراسة">توفير أدوات الدراسة</a></li>
                                <li class="wow fadeInUp"><a href="/Service/28/تحكيم-الدراسات-والاستبانات">تحكيم الدراسات والاستبانات</a></li>
                                <li class="wow fadeInUp"><a href="/Service/29/نشر-الأبحاث-في-المجلات-العلمية">نشر الأبحاث في المجلات العلمية</a></li>
                                <li class="wow fadeInUp"><a href="/Service/100/خدمات-أخرى">خدمات أخرى</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</section>
</div>
@endsection