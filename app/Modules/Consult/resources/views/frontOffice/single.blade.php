@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head')
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection



@section('content')



<div class="c-layout-page MyFont"
@if(in_array($lang, config('app.rtl-languages')))
      dir="rtl"
   @else 
   dir="ltr"
   @endif  


 
 >
    <div class="c-layout-breadcrumbs-1 c-bgimage c-subtitle c-fonts-uppercase c-fonts-bold c-bg-img-center" style="background-image: url(/assets_home/base/img/content/backgrounds/bg-17.jpg)">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-bold c-font-white c-font-20 c-font-slim"></h3>
            </div>
        </div>
    </div>
    <div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase  c-font-sbold MyFont">
                    {{ucfirst(trans('lang.consultations'))}}
                </h3>
            </div>
        </div>
    </div>
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">

            <div class="row margin-bottom-40">
                <div class="col-md-12 col-sm-12">
                    <style>
                        .MyLink {
                            text-decoration: none;

                        }
                    </style>
                
                    <ul class="breadcrumb MyFont">
                        <li><a href="CustomersOrders.aspx"> {{ucfirst(trans('lang.home'))}} </a></li>
                        <li><a id="ctl00_ContentPlaceHolder1_lblOrderTitle"> 
                        @if ($consult->type == 0)
                        {{ucfirst(trans('lang.free_consultations'))}}
                        @else 
                        {{ucfirst(trans('lang.consultations'))}}
                        @endif
                         </a></li>
                    </ul>
                    <div class="c-content-tab-1">
                        <div class="row">
                            <div class="col-md-3">
                              <!--   <ul class="nav">
                                    <li class="active"><a href="#tab_1" data-toggle="tab" data-subname="Message">تفاصيل الطلب</a></li>
                                    <li><a href="#tab_2" data-toggle="tab" data-name="Contract" data-subname="Payment">عقد العمل</a></li>
                                    <li><a href="#tab_5" data-toggle="tab" data-name="Work">تسليم العمل</a></li>
                                </ul> -->
                            </div>
                            <div class="col-md-9 col-sm-9">
                                <div class="tab-content" style="padding: 0; background: #fff;">

                                    <div class="tab-pane active" id="tab_1">
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="portlet boxs">
                                                    <div class="portlet-body">
                                                        <div class="table-scrollable">
                                                            <table class="table table-hover">
                                                                <thead class="bg-primary">
                                                                    <tr>
                                                                        <th >
                                                                         {{ucfirst(trans('lang.details'))}}
                                                                        </th>
                                                                        <th>
                                                                            <a class="btn default btn-xs red" style="display:none;">
                                                                                <i class="fa fa-share"></i>&nbsp;   &nbsp;
                                                                            </a>
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                         {{ucfirst(trans('lang.title'))}}
                                                                        </td>
                                                                        <td> {{$consult->title}} </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>  {{ucfirst(trans('lang.author'))}} </td>
                                                                        <td> {{$consult->user->name}} </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> {{ ucfirst(trans('lang.service')) }} </td>
                                                                        <td> {{ $consult->service->title }}  </td>
                                                                    </tr>
                                                                  
                                                                    <tr>
                                                                        <td> 
                                                                         {{ucfirst(trans('lang.language'))}}
                                                                        </td>
                                                                        <td> {{ucfirst($consult->lang)}} </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>  {{ucfirst(trans('lang.date'))}}</td>
                                                                        <td>
                                                                        {{ $consult->created_at->format('H:s') }}
                                                                            &nbsp &nbsp
                                                                            {{ $consult->created_at->format('Y-m-d') }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                         {{ucfirst(trans('lang.status'))}}
                                                                        </td>
                                                                        <td>
                                                                        @if ($consult->status == 0)
                                                                        <span class="badge badge-pill p-2 m-1 badge-outline-danger  "> {{ucfirst(trans('lang.closed'))}}   </span> &nbsp;
                                                                        @else 

                                                                        <span class="badge badge-pill p-2 m-1  badge-outline-success  ">{{ucfirst(trans('lang.active'))}}  </span> &nbsp;

                                                                        @endif

                                                                        @if ($consult->type == 0)
                                                                        <span class="badge badge-pill p-2 m-1 badge-outline-info  ">{{ucfirst(trans('lang.free'))}}    </span> &nbsp;
                                                                        @else 

                                                                        <span class="badge badge-pill p-2 m-1  badge-outline-danger  ">{{ucfirst(trans('lang.paid'))}}   </span> &nbsp;

                                                                        @endif
                                                                         </td>
                                                                    </tr>


                                                                    <tr>
                                                                        <td>
                                                                         {{ucfirst(trans('lang.attached_file'))}}
                                                                        </td>
                                                                        <td>
                                                                           @if ($consult->file)
                                                                           <a href="{{asset($consult->file)}}" download="download">{{ucfirst(trans('lang.attached_file'))}} <span class="fa fa-download"></span> </a>
                                                                           @endif
                                                                        </td>
                                                                    </tr>
                                                               
                                                                
                                                                
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <br />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($consult->status == 1)
                                            <div class="col-md-12">
                                                <div class="portlet box">
                                                   
                                                    <div class="portlet-body">

                                                    @foreach($consult->messages as $message)
                                                        <div class="media
                                                        @if (checkOfficerRole($message->user))
                                                         bg-info
                                                        @endif 
                                                         ">
                                                            <a href="#" class="pull-left" style='display: block;'>
                                                               <img alt="" src="https://www.manaraa.com/Images/Account/profile.jpg" width="70px" class="media-object" style="border-radius:50%;">
                                                            </a>
                                                            <div class="media-body  p-3" >
                                                                <h4 class="media-heading MyFont">
                                                                    
                                                                    <b> {{$message->user->name}} </b>
                                                                     <span style="float: left;">
                                                                    {{$message->created_at->format('H:s')}}
                                                                        &nbsp &nbsp
                                                                    {{$message->created_at->format('Y-m-d')}}
                                                                    </span>
                                                                </h4>
                                                                <hr />
                                                                <p class="MyFont MYMESSAGETEXT">
                                                                {{$message->description}}
                                                                </p>
                                                                @if ($message->file)
                                                                    <a href="{{asset($message->file)}}" download="download"> {{ ucfirst(trans('lang.attached_file'))}} </a>
                                                                @endif
                                                            </div>
                                                        </div>


                                                    @endforeach

                                                      





                                                        <hr style="height: 2px; background-color: gray;" />

                                                        <div class="post-comment">
                                                          <form id="form"  method="POST" action="{{route('handleUserAddMessage',['lang' => $lang])}}" enctype="multipart/form-data">
                                                           {{ csrf_field() }} 

                                                           <input type="hidden" name="consult" value="{{$consult->id}}">
                                                            <hr />
                                                            <h4 class="MyFont">
                                                                <b>
                                                                {{ ucfirst(trans('lang.new'))}}
                                                                </b>                                                                </b>
                                                            </h4>
                                                            <div class="form-group">
                                                                <label class="control-label MsgSendDone">
                                                                </label>
                                                                <textarea name="description" rows="8" cols="20" id="ctl00_ContentPlaceHolder1_TxtDetails" class="col-md-10 form-control"></textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <div id="fileupload">


                                                                    <div class="row fileupload-buttonbar MyOpenUploader">
                                                                        <div class="col-lg-12">

                                                                            <span class="btn btn-success fileinput-button">
                                                                                <i class="glyphicon glyphicon-plus"></i>
                                                                                <span> {{ucfirst(trans('lang.upload'))}} </span>
                                                                                <input class="MyUpFile" type="file" name="file">
                                                                            </span>
                                                                            <button type="submit" class="btn btn-primary start" id="MyStart" style="display: none;">
                                                                                <i class="glyphicon glyphicon-upload"></i>
                                                                                <span>Start upload</span>
                                                                            </button>
                                                                            <button type="reset" class="btn btn-warning cancel" id="MyCancel" style="display: none;">
                                                                                <i class="glyphicon glyphicon-ban-circle"></i>
                                                                                <span>Cancel upload</span>
                                                                            </button>
                                                                            <button type="button" class="btn btn-danger delete" style="display: none;">
                                                                                <i class="glyphicon glyphicon-trash"></i>
                                                                                <span>Delete</span>
                                                                            </button>
                                                                            <input type="checkbox" class="toggle" style="display: none;">

                                                                            <span class="fileupload-process"></span>
                                                                        </div>

                                                                        <div class="col-lg-12 fileupload-progress fade">

                                                                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                                                                <div class="progress-bar progress-bar-success" style="width: 0%;"></div>
                                                                            </div>

                                                                            <div class="progress-extended">&nbsp;</div>
                                                                        </div>
                                                                    </div>

                                                                    <table role="presentation" class="table table-striped">
                                                                        <tbody class="files"></tbody>
                                                                    </table>
                                                                    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
                              

                                                                    <div style="display: none;">
                                                                       
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <button type="submit" id="StartUp" class="btn btn-primary"> {{ ucfirst(trans('lang.send'))}} </button>
                                                          </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
<br> <br>

                                    <div class="tab-pane" id="tab_2">
                                        <div class="row">
                                            <div class="col-md-12 blog-page">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="portlet box blue">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-cogs"></i>عقد العمل
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body">

                                                                <span id="ctl00_ContentPlaceHolder1_lblNoContract">
                                                                    <div class='note note-info'>
                                                                        <blockquote>
                                                                            <p>لا يوجد عقد عمل حتى الآن</p>
                                                                        </blockquote>
                                                                    </div>
                                                                </span>
                                                                <div id="ctl00_ContentPlaceHolder1_Uppp">
                                                                </div>
                                                                <hr />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="tab-pane" id="tab_5">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">

                                                    <div class="portlet box red">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <i class="fa fa-cogs"></i>تسليم العمل
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body">
                                                            <table class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th colspan="2">ملفات العمل
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                            </table>

                                                        </div>
                                                    </div>
                                                    <span id="lblRagingMsg"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection