<?php

namespace App\Modules\Consult\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class Consult extends Model
{
  
    use Notifiable;
    use SoftDeletes;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'service_requests';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'status',
        'price',
        'type',
        'lang',
        'user_id',
        'priority', 
        'service_id',
        'file'
    ];


    public function messages()
    {
        return $this->hasMany('App\Modules\Consult\Models\Message', 'consult_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User', 'user_id');
    }

    public function service()
    {
        return $this->belongsTo('App\Modules\General\Models\Service', 'service_id');
    }



}
