<?php

namespace App\Modules\Consult\Models;


use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use Notifiable;
    use SoftDeletes;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'consult_id',
        'description',
        'created_at',
        'file'
    ];

    public function user()
    {
        return $this->belongsTo(
            'App\Modules\User\Models\User',
            'user_id',
       
        );
    }



    public function consult()
    {
        return $this->belongsTo(
            'App\Modules\Consult\Models\Consult',
            'consult_id',
       
        );
    }

}
