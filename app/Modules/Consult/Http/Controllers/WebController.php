<?php

namespace App\Modules\Consult\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Consult\Models\Consult;
use App\Modules\User\Models\User;
use App\Modules\Consult\Models\Message;
use App\Modules\General\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Brian2694\Toastr\Facades\Toastr;
use Brian2694\Toastr\Toastr as ToastrToastr;
use Facade\FlareClient\View;
use Illuminate\Support\Facades\Auth;

class WebController extends Controller
{

    public function showAddConsultation($lang)
    {
        
        if (Auth::check()) {
            return view('Consult::frontOffice.new', [
                'services' => Service::where('lang',$lang)->where('status', 1)->get(),
                'lang' => $lang,
                'type' => 1
            ]); 
        }else{
           return redirect(route('showLogin',['lang'=>$lang, 'redirect' => 'showAddConsultation'])); 
        }

      
    }

    public function showAddFreeConsultation($lang)
    {
        if (Auth::check()) {
            return view('Consult::frontOffice.new', [
                'services' => Service::where('lang',$lang)->where('status', 1)->get(),
                'lang' => $lang,
                'type' => 0
            ]); 
        }else{
           return redirect(route('showLogin',['lang'=>$lang, 'redirect' => 'showAddFreeConsultation'])); 
        }
    }
    public function showManagerConsultations($lang)
    {
        return view('Consult::backOffice.list', [
            'consultations' => Consult::where('type', 1)->get(), 
            'current' => 'consultations',
            'lang' => $lang
            ]); 
    }


    public function handleUserAddConsultation($lang,Request $request)
    {
        $data = $request->all(); 

        $rules = [
            'title' => 'required|min:3',
            'description' => 'required|min:10',
            'service' => 'required'
        ]; 

        $messages = [
            'title.required' => trans('lang.required', ['string' => ucfirst(trans('lang.title'))]),
            'title.min' => trans('lang.min', ['string' => ucfirst(trans('lang.title'))]),
            'description.required' => trans('lang.required', ['string' => ucfirst(trans('lang.description'))]),
            'description.min' => trans('lang.min', ['string' => ucfirst(trans('lang.description'))]),
            'service.required' => trans('lang.required', ['string' => ucfirst(trans('lang.service'))]),
        ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors());
        }

        //file


        if ($request->file()){
            $fileName = time().'_'.$request->file->getClientOriginalName();
            $filePath = $request->file('file')->storeAs('/uploads/consultations/files/', $fileName, 'public');
            $path = '/storage/' . $filePath;

   
        }else{
            $path = null ;
        }


        Consult::create([
            'title' => $data['title'],
            'description' => $data['description'], 
            'service_id' => $data['service'], 
            'status' => 0,  
            'lang' => app()->getLocale(), 
            'user_id' => Auth::User()->id,
            'priority' => 0, 
            'type' => $data['type'],
            'file' => $path
        ])  ; 


        Toastr::success(trans('lang.operation_success')); 
        return redirect(route('showUserConsulationsList', ['lang' => $lang])); 
    }


    public function showUserConsulationsList($lang)
    {
        $user = User::find(Auth::id()) ; 
       
        
        return View('Consult::frontOffice.list',[
            'consultations' => $user->consultations,
            'lang' => $lang
           
        ]);
    }


    public function showUserSingleConsult($lang, $id)
    {
        $consult = Consult::find($id); 

        if ($consult){
            if (checkAdviserRole(Auth::User()) || checkAdministratorRole(Auth::user()) || $consult->user_id == Auth::id() || checkOfficerRole(Auth::user()))
            {
                return View('Consult::frontOffice.single', [
                    'consult' => $consult,
                    'lang' => $lang
                ]); 
            }else{
    
                Toastr::error(trans('lang.admin_permission'));
                return redirect(route('showHome', ['lang' => $lang]));
            }
        }else{
            Toastr::error(trans('lang.operation_failed'));
            return redirect(route('showHome', ['lang' => $lang]));
        }
      
     
    }


    public function handleUserAddMessage($lang, Request $request){
        $data = $request->all(); 

        $rules = [
            'description' => 'required|min:10',
        ]; 

        $messages = [
            'description.required' => trans('lang.required', ['string' => ucfirst(trans('lang.description'))]),
        ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors());
        }




        if ($request->file()){
            $fileName = time().'_'.$request->file->getClientOriginalName();
            $filePath = $request->file('file')->storeAs('/uploads/consultations/files/', $fileName, 'public');
            $path = '/storage/' . $filePath;

   
        }else{
            $path = null ;
        }


        Message::create([
            'description' => $data['description'], 
            'user_id' => Auth::User()->id, 
            'consult_id' => $data['consult'], 
            'file' => $path

        ]) ; 


        Toastr::success(trans('lang.operation_success')); 
        return back(); 


    }


    public function showManagerFreeConsult($lang)
    {
        return View('Consult::backOffice.list',[
            'consultations' => Consult::where('type' , 0)->get(),
            'current' => 'free', 
            'lang' => $lang, 
               ]);
    }



    public function handleManagerSwitchConsultStatus($lang, $id)
    {
        $consult = Consult::find($id); 

        if ( $consult->status == 0)
        {
            $consult->status = 1 ; 
            $consult->update();
        }else{
            $consult->status = 0 ; 
            $consult->update(); 
       }

       Toastr::success(trans('lang.operation_success')); 
       return back(); 
    }
}
