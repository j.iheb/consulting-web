<?php

namespace App\Modules\User\Http\Controllers;
use App\Modules\User\Models\User;

use App\Http\Controllers\Controller;
use App\Modules\General\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Validator;
use App\Modules\User\Models\Provider;
use Brian2694\Toastr\Facades\Toastr;

use Intervention\Image\Facades\Image;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Str;

class WebController extends Controller
{

    public function showLogin($lang, $redirect = null)

    {
     
        return view('User::frontOffice.login', ['lang' => $lang, 'redirect' => $redirect]); 
    }

    public function showRegistration($lang)
    {

        return view('User::frontOffice.registration',[
            'countries' => Country::all(),
            'lang' => $lang
        ]); 
    }

    public function showManagerEditUser($lang, $id)
    {
        $user = User::find($id); 
        if($user){
            return view('User::backOffice.edit',[
                'user' => $user, 
                'lang' => $lang
            ]); 
        }else{
            Toastr::error(trans('operation_failed')); 
            return back(); 
        }
    }

    public function handleManagerEditUser($lang, Request $request)
    {
        $data = $request->all(); 

        $rules = [
            'name' => 'required',
         ] ; 

         $messages = [
             'name.required' => trans('lang.required', ['string' => ucfirst(trans('lang.name'))])
         ]; 

         $validation = Validator::make($data, $rules, $messages); 

         if ($validation->fails())
         {
             return redirect()->back()->withErrors($validation->errors()); 
         }

         $user = User::find($data['id']); 

         if (isset($data['photo'])) {

      

            $fileName = time().'_'.$request->photo->getClientOriginalName();
            $filePath = $request->file('photo')->storeAs('/uploads/users/', $fileName, 'public');
            $path = '/storage/' . $filePath;
        }else{
            $path = null ; 
        }


         if($user)
         {
             $user->name = $data['name']; 
             $user->name_ar = $data['name_ar'] ? $data['name_ar'] : null; 
             $user->title_ar = $data['title_ar'] ? $data['title_ar'] : null; 
             $user->title = $data['title'] ? $data['title'] : null ; 
             $user->short_description =  $data['short_description'] ? $data['short_description'] : null ; 
             $user->short_description_ar =  $data['short_description_ar'] ? $data['short_description_ar'] : null ; 
             $user->photo = $path ; 
             $user->update(); 
             Toastr::success(trans('operation_success')); 
             return redirect(route('showManagerUsers',['lang' => $lang])); 
             
             
         }else{
             Toastr::error(trans('lang.operation_failed')); 
             return  back(); 
         }

    }

    public function showManagerUsers($lang)
    {
        return view('User::backOffice.list',[
            'users' => User::all(),
            'lang' => $lang,
            'title' => 'users', 
        ]); 
    }




    public function handleUserLogin($lang, Request $request)
    {
        $data = $request->all();

        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        $messages = [
            'email.required' => trans('lang.required', ['string' => ucfirst(trans('lang.email'))]),
            'password.required' => trans('lang.required', ['string' => ucfirst(trans('lang.password'))]),
            'email.email' =>  trans('lang.email_correct', ['string' => ucfirst(trans('lang.email'))]),
        ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors());
        }

        $credentials = [
            'email' => $data['email'],
            'password' => $data['password'],
        ];

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            
            Toastr::success(trans('lang.welcome',['string' => $user->name])); 
            if($data['redirect']){

                return redirect(route($data['redirect'],['lang' => $lang])); 
            }

            if (checkAdministratorRole($user))
            {
                return redirect(route('showManagerHome', ['lang' => $lang]));
            }else{
                
                return redirect(route('showHome', ['lang' => $lang]));
            }
           
       
        }

        Toastr::warning(trans('lang.verify_credentials'));
        return back();
    }


    public function showManagerAddUser($lang){
        return View('User::backOffice.register',[
            'lang' => $lang,
            'countries' => Country::all(),
            ]); 
    }

    public function handleUserRegister($lang, Request $request)
    {
        $data = $request->all();

   

        $rules = [
            'name' => 'required|min:3|max:50',
            'email' => 'required|email',
            'password' => 'required|min:6', 
            'country' => 'required', 
            'city' => 'required', 
            'phone' => 'required'
        ];

        $messages = [
            'name.required' => trans('lang.required', ['string' => ucfirst(trans('lang.full_name'))]),
            'name.min' => trans('lang.min_string', ['string' => ucfirst(trans('lang.full_name')), 'min' => 3]),
            'name.max' => trans('lang.max_string', ['string' => ucfirst(trans('lang.full_name')), 'max' => 50]),
            'email.required' => trans('lang.required', ['string' => ucfirst(trans('lang.email'))]),
            'email.email' => trans('lang.email', ['string' => ucfirst(trans('lang.email'))]),
            'email.regex' => trans('lang.email', ['string' => ucfirst(trans('lang.email'))]),
            'password.required' => trans('lang.required', ['string' => ucfirst(trans('lang.password'))]),
            'password.min' => trans('lang.min_string', ['string' => ucfirst(trans('lang.password')), 'min' => 6]),
            'country.required' => trans('lang.required', ['string' => ucfirst(trans('lang.country'))]),
            'city.required' => trans('lang.required', ['string' => ucfirst(trans('lang.state'))]),
            'phone.required' => trans('lang.required', ['string' => ucfirst(trans('lang.phone'))]),
        ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors());
        }


        $country = Country::where('title', $data['country'])->orWhere('title_ar', $data['country'])->first(); 

        

        $email = $data['email'];
        $password = $data['password'];
        $country = $country->id; 
        $city = $data['city']; 

        $user = User::where('email', '=', $email)->first();
        $validation = Str::random(30);

        if ($user) {
            
            
            if (isset($data['name_ar']))
            {
                
                
            if (isset($data['photo'])) {


                $fileName = time().'_'.$request->photo->getClientOriginalName();
                $filePath = $request->file('photo')->storeAs('/uploads/users/', $fileName, 'public');
                $path = '/storage/' . $filePath;
            }else{
                $path = null ; 
            }
            
            
              if($user)
             {
                
                 $user->name_ar = $data['name_ar'] ? $data['name_ar'] : null; 
                 $user->title_ar = $data['title_ar'] ? $data['title_ar'] : null; 
                 $user->title = $data['title'] ? $data['title'] : null ; 
                 $user->short_description =  $data['short_description'] ? $data['short_description'] : null ; 
                 $user->short_description_ar =  $data['short_description_ar'] ? $data['short_description_ar'] : null ; 
                 $user->photo = $path ; 
                 $user->update(); 
                 Toastr::warning(trans('lang.email_validate'));
                 Toastr::success(trans('lang.operation_success')); 
                 return redirect(route('showManagerUsers',['lang' => $lang])); 
                 
                 
             }
            
    
            }
            Toastr::warning(trans('lang.email_used'));
            return back();
        } else {
            
            $userData = [
                'email' => $email,
                'password' => bcrypt($password),
                'validation' => $validation,
                'name' => $data['name'], 
                'country_id' => $country, 
                'city' => $city,
                'phone' => $data['phone'],
                'language' => $lang, 

            ];


            $user = User::create($userData);
            User::find($user->id)->roles()->attach(1);
            
            


     
           
         
        }

        Toastr::warning(trans('lang.email_validate'));

      /*  $content = ['email' => $email, 'validation' => $validation];

        Mail::send('User::mail.validate', $content, function ($message) use ($email, $data) {
            $message->to($email, $data['name']);
            $message->subject(trans('lang.email_validation'));
        });*/


        return back();
    }



    public function handleManagerAllowUser($lang, $id){
        $user = User::find($id);

        $user->status = 2;
        $user->save();

        Toastr::success(trans('lang.operation_success'));
        return back();
    }

    public function handleManagerBanUser($lang, $id)
    {
        $user = User::find($id);

        $user->status = 3;
        $user->save();

        Toastr::success(trans('lang.user_banned'));
        return back();
    }

    public function handleManagerAuthorizeManager($lang, $id)
    {
        User::find($id)->roles()->attach(2);

        Toastr::success(trans('lang.user_add_manager'));
        return back();
    }

    public function handleManagerRevokeManager($lang, $id)
    {
        User::find($id)->roles()->detach(2);

        Toastr::success(trans('lang.user_revoke_manager'));
        return back();
    }

    public function handleManagerAuthorizeOfficer($lang, $id)
    {
        User::find($id)->roles()->attach(3);

        Toastr::success(trans('lang.operation_success'));
        return back();
    }

    public function handleManagerRevokeOfficer($lang, $id)
    {
        User::find($id)->roles()->detach(3);

        Toastr::success(trans('lang.operation_success'));
        return back();
    }


    public function handleManagerAuthorizeAdviser($lang, $id)
    {
        User::find($id)->roles()->attach(4);

        Toastr::success(trans('lang.operation_success'));
        return back();
    }

    public function handleManagerRevokeAdviser($lang, $id)
    {
        User::find($id)->roles()->detach(4);

        Toastr::success(trans('lang.operation_success'));
        return back();
    }


    public function handleLogout($lang)
    {
        Auth::logout();
        return redirect(route('showHome', ['lang' => $lang]));
    }



    public function showManagerAdvisers($lang)
    {
        return View('User::backOffice.list',[
            'users' =>  User::whereHas('roles' , function ($q) { $q->where('title', 'adviser'); })->get(), 
            'lang' => $lang,
            'title' => 'adviser',
        ]);
    }

    public function showManagerOfficers($lang)
    {
        return View('User::backOffice.list',[
            'users' =>  User::whereHas('roles' , function ($q) { $q->where('title', 'officer'); })->get(), 
            'lang' => $lang, 
            'title' => 'officer',
        ]);
    }



    public function handleSocialRedirect($lang, $provider)
    {
        switch($provider){

            case 'facebook': return Socialite::driver($provider)->redirect();
        }

    }

    public function handleSocialCallback($lang, $provider)
    {

    try {
        $providerData = Socialite::driver($provider)->stateless()->user();

        $user = User::where('email', '=', $providerData->email)->first();

        if (!$user) {
            if ($providerData->avatar != null) {

                $photo = $providerData->id . '-' . time() . '.png';
                $imagePath = 'storage/uploads/users/' . $photo;
                $fullImagePath = public_path('storage/uploads/users/' . $photo);

                Image::make($providerData->avatar)->save($fullImagePath);
            }else{
                $imagePath = '';
            }

            $user = User::create([
                'email' => $providerData->email,
                'name'=> $providerData->name,
                'status' => 1,
                'photo'=> $imagePath,
                'language' => 'en',
            ]);

            User::find($user->id)->roles()->attach(1);

            Provider::create([
                'provider'=>$provider,
                'user_id'=>$user->id
            ]);


            Auth::login($user);
            return redirect(route('showHome',['lang' => 'en']));
        }

        $userProvider = $user->whereHas('providers', function ($query) use ($provider) {
            $query->where('provider', $provider);
        })->count();

        if ($userProvider == 0) {
            Provider::create([
                'provider' => $provider,
                'user_id' => $user->id
            ]);
        }

        if ($user->status === 3) {
            Toastr::error(ucfirst(trans('User::lang.banned_user')));
            return back();
        }

        if ($user->status == 0) {
            $user->status = 1;
            $user->save();

            Auth::login($user);
            return redirect(route('showHome',['lang' => 'en']));
        }

        Auth::login($user);
        return redirect(route('showHome',['lang' => 'en']));
    }
    catch (\Exception $e){
        dd($e);
        die(); 
        Toastr::error(ucfirst(trans('User::lang.error_occurred_social', ['network' => ucfirst($provider)])));
        return redirect(route('showHome',['lang' => 'en']));
    }

    }

    public function handleUserSocialDisconnect($lang, $provider){
        $provider = Provider::where('provider', '=', $provider)->where('user_id', '=', Auth::id())->first();

        if ($provider){
            Toastr::success(ucwords(trans('User::lang.provider_disconnected')));
            $provider->delete();
        } else {
            Toastr::error(ucwords(trans('User::lang.provider_not_found')));
        }

        return back();
    }


}
