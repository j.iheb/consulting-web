@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => $title
])
@endsection

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins') }}/datatable/datatable.css">
<script type="text/javascript" src="{{ asset('plugins') }}/datatable/datatable.js"></script>

<script type="text/javascript">
    /* Datatables responsive */

    $(document).ready(function() {
        $('#datatable-responsive').DataTable({
            responsive: true,
            language: {
                url: "{{ asset('plugins/datatable/lang/'.\Illuminate\Support\Facades\Session::get('youfors_applocale').'.js') }}"
            }
        });
        $('.dataTables_filter input').attr("placeholder", "{{ trans('lang.search') }}...");
    });
</script>





<div class="row">
        <div class="col-md-9">
        <div class="breadcrumb">
            <h1>{{ trans('lang.users') }} </h1>
            <ul>
                <li><a href="{{ route('showManagerHome',['lang' => $lang]) }}">{{ trans('lang.dashboard') }}</a></li>
                <li>{{ trans('lang.users') }}</li>
            </ul>
        </div>
        </div>
        <div class="col-md-3 pull-right">
            <div class="float-right">
                <a href="{{route('showManagerAddUser',['lang' => $lang])}}" class="btn btn-primary">{{ ucwords(trans('lang.register')) }}</a>
            </div>
        </div>
    </div>


<div class="separator-breadcrumb border-top"></div>

<div class="row mb-4">

    <div class="col-md-12 mb-4">
        <div class="card text-left">

            <div class="card-body">
                <table id="datatable-responsive" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{ trans('lang.full_name') }}</th>
                            <th>{{ trans('lang.email') }}</th>
                            <th>{{ trans('lang.status') }}</th>
                            <th>{{ trans('lang.roles') }}</th>
                            <th>{{ trans('lang.date') }}</th>
                            <th>{{ trans('lang.action') }}</th>
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                            <th>{{ trans('lang.full_name') }}</th>
                            <th>{{ trans('lang.email') }}</th>
                            <th>{{ trans('lang.status') }}</th>
                            <th>{{ trans('lang.roles') }}</th>
                            <th>{{ trans('lang.date') }}</th>
                            <th>{{ trans('lang.action') }}</th>
                        </tr>
                    </tfoot>

                    <tbody>
                    @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <span class="badge badge-pill badge-outline-secondary p-2 m-1">{{ transformUserStatus($user->status) }}</span>
                                </td>
                                <td>
                                    @foreach($user->roles as $role)
                                        <span class="badge badge-pill p-2 m-1
                                                 @if($role->id == 1)
                                                badge-outline-primary
                                                @elseif($role->id == 2)
                                                badge-outline-info
                                                @else
                                                badge-outline-danger
                                                @endif
                                                ">{{ transformRoleStatus($role->id) }}
                                            </span> &nbsp;
                                    @endforeach
                                </td>
                                <td>{{ $user->created_at->format('d-M-Y H:m:s') }}</td>
                                <td>
                                    @if($user->status !== 3)
                                        <span class="badge badge-pill badge-outline-danger p-2 m-1" title="{{ trans('lang.user_ban_action') }}" onclick="location='{{ route('handleManagerBanUser', [ 'lang' => $lang, 'id' => $user->id]) }}';">{{ trans('lang.user_ban_action')}} </span>
                                    @else
                                        <span class="badge badge-pill badge-outline-info p-2 m-1" title="{{ trans('lang.user_authorize_action') }}" onclick="location='{{ route('handleManagerAllowUser', [ 'lang' => $lang,'id' => $user->id]) }}';">{{ trans('lang.user_authorize_action')}}  </span>
                                    @endif
                                    @if(checkAdministratorRole($user))
                                        <span class="badge badge-pill badge-outline-warning p-2 m-1" title="{{ trans('lang.user_revoke_action') }}" onclick="location='{{ route('handleManagerRevokeManager', [ 'lang' => $lang,'id' => $user->id]) }}';"> {{trans('lang.user_revoke_admin_action')}}  </span>
                                    @else
                                        <span class="badge badge-pill badge-outline-danger p-2 m-1" title="{{ trans('lang.user_admin_action') }}" onclick="location='{{ route('handleManagerAuthorizeManager', ['lang' => $lang, 'id' => $user->id]) }}';"> {{trans('lang.user_authorize_admin_action')}}</span>
                                    @endif


                                    @if(checkOfficerRole($user))
                                        <span class="badge badge-pill badge-outline-warning p-2 m-1" title="revoke officer" onclick="location='{{ route('handleManagerRevokeOfficer', [ 'lang' => $lang, 'id' => $user->id]) }}';"> {{trans('lang.user_revoke_officer_action')}} </span>
                                    @else
                                        <span class="badge badge-pill badge-outline-danger p-2 m-1" title="add officer role" onclick="location='{{ route('handleManagerAuthorizeOfficer', [ 'lang' => $lang, 'id' => $user->id]) }}';"> {{trans('lang.user_authorize_offcier_action')}} </span>
                                    @endif


                                    @if(checkAdviserRole($user))
                                        <span class="badge badge-pill badge-outline-warning p-2 m-1" title="revoke adviser" onclick="location='{{ route('handleManagerRevokeAdviser', ['lang' => $lang,'id' => $user->id]) }}';"> {{trans('lang.user_revoke_adviser_action')}} </span>
                                    @else
                                        <span class="badge badge-pill badge-outline-danger p-2 m-1" title="add adviser role" onclick="location='{{ route('handleManagerAuthorizeAdviser', ['lang' => $lang,'id' => $user->id]) }}';"> {{trans('lang.user_authorize_adviser_action')}} </span>
                                    @endif

                                    
                                    <span class="badge badge-pill badge-outline-primary p-2 m-1" title="revoke adviser" onclick="location='{{ route('showManagerEditUser', ['lang' => $lang,'id' => $user->id]) }}';">{{ trans('lang.edit')}}</span>
                                 


                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection