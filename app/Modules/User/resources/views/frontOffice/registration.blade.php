@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head')
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection



@section('content')




<section class="breadcrumb-section wow fadeIn">

    <div class="container">
        <div class="breadcrumb-wrap">
            <a href="{{route('showHome',['lang' => $lang])}}"> {{ucfirst(trans('lang.home'))}} </a>
            <i>/</i>
            <span> {{ucfirst(trans('lang.login'))}} </span>

             
          
            
        </div>
    </div>
</section>

<section class="inner-page-section wow fadeIn">
    <div class="container">
    <form  method="post"    action="{{ route('handleUserRegister',['lang' => $lang]) }}" enctype="multipart/form-data">
      @csrf
        <div class="row margin-bottom-40">

            <div class="col-md-12 col-sm-12">
                <div class="content-form-page">
                    <div class="row">
                        <div class="col-md-7 col-sm-7">
                            <div class="form-horizontal form-without-legend" role="form">
                                <div id="ctl00_ContentPlaceHolder1_pnlCustomerLogin">
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label"></label>
                                        <div id="ctl00_ContentPlaceHolder1_lblMsg" class="col-lg-8">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-lg-4 control-label">
                                            {{ucfirst(trans('lang.full_name'))}}
                                            <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator2" class="color-red" style="color:Red;display:none;">*</span>
                                        </label>
                                        <div class="col-lg-8">
                                            <input name="name" type="text" id="ctl00_ContentPlaceHolder1_TxtUserName" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-lg-4 control-label">
                                            {{ucfirst(trans('lang.email'))}}     
                                           <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator4" class="color-red" style="color:Red;display:none;">*</span>
                                        </label>
                                        <div class="col-lg-8">
                                            <input name="email" type="email" id="ctl00_ContentPlaceHolder1_TxtEmail" class="MyCtrl form-control" />
                                            <span id="ctl00_ContentPlaceHolder1_RegularExpressionValidator2" class="color-red MyFont" style="color:Red;display:none;">الرجاء إدخال بريد إلكتروني صحيح</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="col-lg-4 control-label">
                                        {{ucfirst(trans('lang.password'))}}  
                                            <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator1" class="color-red" style="color:Red;display:none;">*</span>
                                        </label>
                                        <div class="col-lg-8">
                                            <input name="password" type="password" id="ctl00_ContentPlaceHolder1_TxtPassword" class="form-control" />
                                        </div>
                                    </div>
   
                                    <div class="form-group">
                                        <label for="email" class="col-lg-4 control-label">
                                         {{ucfirst(trans('lang.phone'))}}    
                                        <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator7" class="color-red" style="color:Red;display:none;">*</span>
                                        </label>
                                        <div class="col-lg-8">
                                            <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator444" class="color-red" style="color:Red;display:none;">الرجاء كتابة رقم الجوال بالمفتاح الدولي بدون صفرين مثال : 970592146192</span>
                                            <input name="phone" type="phone" id="ctl00_ContentPlaceHolder1_TxtMobileNo" class=" MyCtrl form-control" />
                                        </div>
                                    </div>
                                    {{--<div class="form-group">
                                        <label for="email" class="col-lg-4 control-label">
                                            {{ucfirst(trans('lang.country'))}}
                                            <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator8" class="color-red" style="color:Red;display:none;">*</span>
                                        </label>
                                        <div class="col-lg-8">
                                            <select name="country" id="ctl00_ContentPlaceHolder1_ddlCountry" class="form-control MyFont" style="height: 40px;">
                                            

                                                @foreach ($countries as $country)
                                                <option value="{{ $country->id }}"> @if ($lang == 'ar') {{$country->title_ar}} @else {{$country->title}}  @endif</option>
                                                @endforeach
                                               
                                            </select>
                                        </div>
                                    </div>
                                    --}}
                                    <div class="form-group">
                                        <label for="email" class="col-lg-4 control-label">
                                            {{ucfirst(trans('lang.country'))}}
                                            <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator8" class="color-red" style="color:Red;display:none;">*</span>
                                        </label>
                                        <div class="col-lg-8">

                                            <input list="browsers" name="country" id="browser"  class=" MyCtrl form-control">
                                            <datalist id="browsers" name="country" >

                                              @foreach ($countries as $country)
                                                <option value=" @if ($lang == 'ar') {{$country->title_ar}} @else {{$country->title}}  @endif">
                                                @endforeach
                                              
                                            </datalist>

                                        </div>
                                    </div>    


                                    <div class="form-group">
                                        <label for="email" class="col-lg-4 control-label">
                                        {{ucfirst(trans('lang.city'))}}
                                            <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator3" class="color-red" style="color:Red;display:none;">*</span>
                                        </label>
                                        <div class="col-lg-8">
                                        <input name="city" type="text" id="ctl00_ContentPlaceHolder1_TxtMobileNo" class=" MyCtrl form-control" />
                                        </div>
                                    </div>
                               
                                    <div class="row">
                                        <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
                                            <input type="submit" name="ctl00$ContentPlaceHolder1$btnLogin" value="{{ucfirst(trans('lang.register'))}}" id="ctl00_ContentPlaceHolder1_btnLogin" class="btn btn-primary" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form> 

    </div>
</section>

@endsection