@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head')
@endsection

@section('header')
    @include('frontOffice.inc.header')
@endsection



@section('content')

<section class="breadcrumb-section wow fadeIn">

    <div class="container">
        <div class="breadcrumb-wrap">
            <a href="{{route('showHome',['lang' => $lang])}}"> {{ucfirst(trans('lang.home'))}} </a>
            <i>/</i>
            <span> {{ucfirst(trans('lang.login'))}}   </span>

             
          
            
        </div>
    </div>
</section>
<section class="inner-page-section wow fadeIn">
    <div class="container">
    <form  method="post"    action="{{ route('handleUserLogin',['lang' => $lang]) }}" >
      @csrf

     

        <div class="login-register-wrap">
            <span id="ctl00_ContentPlaceHolder1_lblMsg"></span>
            <div id="ctl00_ContentPlaceHolder1_Panel1">
            <div class="form-group wow fadeInUp login-options">
                        <a  href=" {{route('handleSocialRedirect',[ 'lang' =>$lang,  'provider'=> 'facebook'])}} "  class="submit-btn btn btn-bordered" style="text-decoration: none; color:white; width:100%; background-color:royalblue;border:none;" > {{ucfirst(trans('lang.facebook'))}} </a> <br>
             
                    </div>
                    <div class="form-group wow fadeInUp login-options">
                        <a  href=" {{route('handleSocialRedirect',[ 'lang' =>$lang,  'provider'=> 'facebook'])}} "  class="submit-btn btn btn-bordered" style="text-decoration: none; color:white; width:100%; background-color:brown;border:none;" > {{ucfirst(trans('lang.google'))}} </a> 
                  
                    </div>

                <div class="login-form">
                    <input type="hidden" name="redirect" value="{{$redirect}}">
                    <div class="form-group wow fadeInUp">
                        <label>
                              {{ucfirst(trans('lang.email'))}} &nbsp
                            <span  class="color-red" style="color:Red;display:none;"></span>
                        </label>
                        <input type="text"  name="email" class="input-item" placeholder="{{ trans('lang.email')}}" />
                    </div>
                    <div class="form-group wow fadeInUp">
                        <label>
                        {{ucfirst(trans('lang.password'))}} &nbsp
                            <span class="color-red" style="color:Red;display:none;"></span>
                        </label>
                        <input name="password" type="password" name="password"  class="input-item" placeholder="{{ trans('lang.password')}}" />
                    </div>
                 
                  <div class="form-group wow fadeInUp login-options">
                         <a href="{{ route('showRegistration',['lang' => $lang]) }}" class="register auto-margin"> {{ucfirst(trans('lang.register'))}} </a>
                  
                      <!--    <a href="#" class="forgit-pass">نسيت كلمة المرور ؟</a>
                        <a href="{{ route('showRegistration',['lang' => $lang]) }}" class="register auto-margin">مستخدم جديد </a>-->
                    </div> 
                    <div class="form-group wow fadeInUp login-options">
                        <input type="submit"  value="{{trans('lang.login')}}"  class="submit-btn btn btn-bordered" />
                      <!--  <a href="#" class="btn btn-bordered help auto-margin">مساعدة</a> -->
                    </div>


              
                </div>
            </div>
        </div>
        </form>
    </div>
</section>


@endsection