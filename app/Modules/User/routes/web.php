<?php


Route::group(['module' => 'User', 'middleware' => ['web'], 'prefix' => '{lang}'], function () {

Route::get('/login/{redirect?}', 'WebController@showLogin')->name('showLogin');
Route::get('/registration', 'WebController@showRegistration')->name('showRegistration');

Route::post('/user/register', 'WebController@handleUserRegister')->name('handleUserRegister');
Route::post('/user/login', 'WebController@handleUserLogin')->name('handleUserLogin');

Route::get('/user/social/{provider}', 'WebController@handleSocialRedirect')->name('handleSocialRedirect')->middleware('guest');
Route::get('/user/social/{provider}/callback', 'WebController@handleSocialCallback')->name('handleSocialCallback')->middleware('guest');
Route::get('/user/social/{provider}/disconnect', 'WebController@handleUserSocialDisconnect')->name('handleUserSocialDisconnect')->middleware('auth');



Route::get('/manager/user/ban/{id}', 'WebController@handleManagerBanUser')->name('handleManagerBanUser')->middleware('auth', 'manager');
Route::get('/manager/user/allow/{id}', 'WebController@handleManagerAllowUser')->name('handleManagerAllowUser')->middleware('auth', 'manager');
Route::get('/manager/user/authorize/{id}', 'WebController@handleManagerAuthorizeManager')->name('handleManagerAuthorizeManager')->middleware('auth', 'manager');
Route::get('/manager/user/revoke/{id}', 'WebController@handleManagerRevokeManager')->name('handleManagerRevokeManager')->middleware('auth', 'manager');

Route::get('/manager/user/add', 'WebController@showManagerAddUser')->name('showManagerAddUser')->middleware('auth', 'manager'); 
Route::get('/manager/user/edit/{id}', 'WebController@showManagerEditUser')->name('showManagerEditUser')->middleware('auth', 'manager');
Route::post('/manager/user/update', 'WebController@handleManagerEditUser')->name('handleManagerEditUser')->middleware('auth', 'manager');


Route::get('/manager/officer/authorize/{id}', 'WebController@handleManagerAuthorizeOfficer')->name('handleManagerAuthorizeOfficer'); 
Route::get('/manager/officer/revoke/{id}', 'WebController@handleManagerRevokeOfficer')->name('handleManagerRevokeOfficer'); 


Route::get('/manager/adviser/authorize/{id}', 'WebController@handleManagerAuthorizeAdviser')->name('handleManagerAuthorizeAdviser'); 
Route::get('/manager/adviser/revoke/{id}', 'WebController@handleManagerRevokeAdviser')->name('handleManagerRevokeAdviser'); 

Route::get('/manager/users', 'WebController@showManagerUsers')->name('showManagerUsers')->middleware('auth', 'manager'); 

Route::get('/members/logout', 'WebController@handleLogout')->name('handleLogout')->middleware('auth');


Route::get('/manager/officers', 'WebController@showManagerOfficers')->name('showManagerOfficers')->middleware('auth', 'manager');
Route::get('/manager/advisers', 'WebController@showManagerAdvisers')->name('showManagerAdvisers')->middleware('auth', 'manager'); 
});