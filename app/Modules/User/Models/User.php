<?php

namespace App\Modules\User\Models;

use App\Modules\Consult\Models\Consult;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

 

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'status',
        'photo',
        'gender',
        'validation',
        'language',
        'city', 
        'country_id',
        'short_dscription',
        'title',
        'short_dscription_ar',
        'title_ar',
        'name_ar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(
            'App\Modules\User\Models\Role',
            'user_roles',
            'user_id',
            'role_id'
        );
    }

 
    public function consultations()
    {
        return $this->hasMany('App\Modules\Consult\Models\Consult', 'user_id', 'id');
    }



}