<?php

namespace App\Modules\Payment\Models;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Consult\Models\Consult;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use Notifiable;
    use SoftDeletes;

 

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'payments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'consult_id',
        'getway',
        'status',
        'photo',
        'token',
        'price',
        'description', 
        'created_at', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(
            'App\Modules\User\Models\Role',
            'user_roles',
            'user_id',
            'role_id'
        );
    }

 
    public function consultations()
    {
        return $this->hasMany('App\Modules\Consult\Models\Consult', 'user_id', 'id');
    }
}
