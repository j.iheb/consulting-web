<?php

namespace App\Modules\Payment\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Payment\Models\Payment;
use Facade\FlareClient\View;
use Illuminate\Http\Request;

class WebController extends Controller
{

  public function showManagerPayments($lang)
  {
      return View('Payment::backOffice.payments',[
          'payments' => Payment::all(), 
          'lang' => $lang
      ]); 
  }
}
