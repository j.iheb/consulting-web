<?php

namespace App\Modules\Blog\Http\Controllers;

use App\Modules\Blog\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\General\Models\Service;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Brian2694\Toastr\Facades\Toastr;
use Facade\FlareClient\View;
use Illuminate\Support\Facades\File;

class WebController extends Controller
{

    public function showManagerArticlesList($lang){

        return view('Blog::backOffice.list', [
            'articles' => Article::all(), 
            'lang' => $lang
        ]);
    }

    public function showManagerAddArticle($lang){

        return view('Blog::backOffice.article', [
            'lang' => $lang,
            'services' => Service::where('lang', $lang)->get(),
        ]);
    }

    
    public function handleManagerAddArticle($lang,Request $request){
        $data = $request->all();

        $rules = [
            'title' => 'required',
            'short_description' => 'required',
            'content' => 'required',
        ];

        $messages = [
            'title.required' => trans('lang.required', ['string' => ucfirst(trans('lang.title'))]),
            'short_description.required' => trans('lang.required', ['string' => ucfirst(trans('lang.short_description'))]),
            'content.required' => trans('lang.required', ['string' => ucfirst(trans('lang.short_description'))]),
        ];

        if (isset($data['cover'])) {
            $rules['cover'] = 'image|mimes:jpeg,png,jpg|max:2000';
            $messages['cover.image'] = trans('lang.file_format', ['file' => trans('lang.cover'), 'format' => 'image']);
            $messages['cover.mimes'] = trans('lang.file_mimes', ['file' => ucfirst(trans('lang.cover')), 'mimes' => 'JPEG, JPG & PNG']);
            $messages['cover.max'] = trans('lang.file_size', ['file' => ucfirst(trans('lang.cover')), 'size' => 2]);
        }

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors());
        }

        if (isset($data['cover'])) {

            $fileName = time().'_'.$request->cover->getClientOriginalName();
            $filePath = $request->file('cover')->storeAs('/uploads/blog/covers/', $fileName, 'public');
            $photoPath = '/storage/' . $filePath;
        } else {
            $photoPath = null;
        }

        Article::create([
            'category' => $data['category'],
            'title' => $data['title'],
            'short_description' => $data['short_description'],
            'content' => $data['content'],
            'cover' => $photoPath,
            'status' => $data['status'],
            'author_id' => Auth::id(),
            'lang' => $lang
        ]);

        Toastr::success(ucfirst(trans('lang.operation_success')));
        return redirect(route('showManagerArticlesList', ['lang' => $lang]));
    }
    
    public function showManagerEditArticle($lang, $articleId){
        $article = Article::find($articleId);

        return view('Blog::backOffice.article', [
            'lang' => $lang,
            'article' => $article,
            'services' => Service::all(),
        ]);
    }

    public function handleManagerEditArticle($lang,$articleId, Request $request){
        $data = $request->all();

        $rules = [
            'title' => 'required',
            'short_description' => 'required',
            'content' => 'required',
        ];

        $messages = [
            'title.required' => trans('lang.required', ['string' => ucfirst(trans('shared.title'))]),
            'short_description.required' => trans('lang.required', ['string' => ucfirst(trans('Blog::lang.short_description'))]),
            'content.required' => trans('lang.required', ['string' => ucfirst(trans('Blog::lang.short_description'))]),
       ];

        if (isset($data['cover'])) {
            $rules['cover'] = 'image|mimes:jpeg,png,jpg|max:2000';
            $messages['cover.image'] = trans('lang.file_format', ['file' => trans('Blog::lang.cover'), 'format' => 'image']);
            $messages['cover.mimes'] = trans('lang.file_mimes', ['file' => ucfirst(trans('Blog::lang.cover')), 'mimes' => 'JPEG, JPG & PNG']);
            $messages['cover.max'] = trans('lang.file_size', ['file' => ucfirst(trans('Blog::lang.cover')), 'size' => 2]);
        }

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors());
        }

        $article = Article::find($articleId);

        if (isset($data['cover'])) {
            if ($article->cover !== null) {
                File::delete(asset($article->cover));
            }

            $fileName = time().'_'.$request->cover->getClientOriginalName();
            $filePath = $request->file('cover')->storeAs('/uploads/blog/covers/', $fileName, 'public');
            $article->cover = '/storage/' . $filePath;


        }

        $article->title = $data['title'];
        $article->category = $data['category'];
        $article->short_description = $data['short_description'];
        $article->content = $data['content'];
        $article->status = $data['status'];

        $article->save();

        Toastr::success(ucfirst(trans('lang.operation_success')));
        return redirect(route('showManagerArticlesList', ['lang' => $lang]));
    }
    

    public function handleManagerDeleteArticle($lang, $articleId){
        $article = Article::find($articleId);

        $article->delete();

        Toastr::success(ucfirst(trans('lang.opration_success')));
        return redirect(route('showManagerArticlesList', ['lang' => $lang]));
    }

    

    public function handleManagerAddArticleMedia($lang, Request $request){

        $rules = [
            'upload' => 'mimes:jpeg,png,jpg|max:1024'
        ];

        $messages = [
            'upload.mimes' => trans('file_mimes', ['mimes' => 'jpeg,png,jpg']),
            'upload.max' => trans('file_size', ['size' => '1']),
        ];

        $validation = Validator::make($request->all(), $rules, $messages);

        if ($validation->fails()) {
            foreach ($validation->errors() as $error) {
                return '<script>toastr.error(' . $error . ') </script>';
            }
        }

        if ($request->hasFile('upload')) {

            $fileDown = $request['upload'];
            $file = time() . '-article-' . $fileDown->getClientOriginalName();
            $fullFilePath = public_path('storage/uploads/blog/' . $file);
            move_uploaded_file($fileDown->getRealPath(), $fullFilePath);

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('storage/uploads/blog/' . $file);
            $msg = ucfirst(trans('Blog::lang.medial_upload_success')); // todo
            $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            // Render HTML output
            @header('Content-type: text/html; charset=utf-8');
            echo $re;
        }
    }


    public function showSingleArticle($lang, $id)
    {
        $article = Article::find($id);
        if ($article){
            return View('Blog::frontOffice.single', ['lang' => $lang, 'article' => $article]); 
        }else{
            Toastr::error(trans('lang.operation_failed')); 
            return back(); 
        }
    }



    
}
