<?php

Route::group(['module' => 'Blog', 'middleware' => ['web'], 'prefix' => '{lang}'], function () {


    Route::get('/manager/blog', 'WebController@showManagerArticlesList')->name('showManagerArticlesList')->middleware('auth', 'manager');
    Route::get('/manager/blog/new', 'WebController@showManagerAddArticle')->name('showManagerAddArticle')->middleware('auth', 'manager');
    Route::post('/manager/blog/media/upload', 'WebController@handleManagerAddArticleMedia')->name('handleManagerAddArticleMedia')->middleware('auth', 'manager');
    Route::post('/manager/blog/new', 'WebController@handleManagerAddArticle')->name('handleManagerAddArticle')->middleware('auth', 'manager');
    Route::get('/manager/blog/delete/{articleId}', 'WebController@handleManagerDeleteArticle')->name('handleManagerDeleteArticle')->middleware('auth', 'manager');
    Route::get('/manager/blog/edit/{articleId}', 'WebController@showManagerEditArticle')->name('showManagerEditArticle')->middleware('auth', 'manager');
    Route::post('/manager/blog/edit/{articleId}', 'WebController@handleManagerEditArticle')->name('handleManagerEditArticle')->middleware('auth', 'manager');
   
    Route::get('/article/{id}', 'WebController@showSingleArticle')->name('showSingleArticle'); 
 
  /*  Route::get('/manager/blog/categories', 'WebController@showManagerArticlesCategoriesList')->name('showManagerArticlesCategoriesList')->middleware('auth', 'manager');

    Route::post('/manager/blog/categories/new', 'WebController@handleManagerAddArticleCategory')->name('handleManagerAddArticleCategory')->middleware('auth', 'manager');
    Route::post('/manager/blog/categories/edit/{categoryId}', 'WebController@handleManagerEditArticleCategory')->name('handleManagerEditArticleCategory')->middleware('auth', 'manager');
    Route::get('/manager/blog/categories/delete/{categoryId}', 'WebController@handleManagerDeleteArticleCategory')->name('handleManagerDeleteArticleCategory')->middleware('auth', 'manager');
    Route::post('/manager/blog/categories/move/{categoryId}', 'WebController@handleManagerMoveArticleCategory')->name('handleManagerMoveArticleCategory')->middleware('auth', 'manager');

    Route::post('/manager/blog/new', 'WebController@handleManagerAddArticle')->name('handleManagerAddArticle')->middleware('auth', 'manager');
    Route::post('/manager/blog/edit/{articleId}', 'WebController@handleManagerEditArticle')->name('handleManagerEditArticle')->middleware('auth', 'manager');
   
    Route::post('/manager/blog/media/upload', 'WebController@handleManagerAddArticleMedia')->name('handleManagerAddArticleMedia')->middleware('auth', 'manager');

*/

});