@extends('backOffice.layout')

@section('head')
    @include('backOffice.inc.head',
        ['title' => config('app.name') . ' - ' . ucfirst(trans('shared.administration')),
    'description' => config('app.name')
    ])
@endsection

@section('header')
    @include('backOffice.inc.header')
@endsection

@section('sidebar')
    @include('backOffice.inc.sidebar', [
        'current' => 'blog'
    ])
@endsection

@section('content')
 
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
        
<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>

<script>
    $(function () {
        $("#article").validate({
            ignore: "input:hidden:not(input:hidden.required)",
            rules: {
                "title": {
                    required: true,
                    minlength: 2,
                    maxlength: 150
                },
                "short_description": {
                    required: true,
                    minlength: 10,
                    maxlength: 300
                },
                "content": {
                    required: true,
                    minlength: 100,
                },
                "cover": {
                    extension: "jpg|jpeg|png",
                    fileSizeMax: 2048000
                },
            },
            messages: {
                "title": {
                    required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.title'))]) }}",
                    minlength: "{{ trans('lang.min_string', ['string' =>   ucfirst(trans('shared.title')), 'min' => 2]) }}",
                    maxlength: "{{ trans('lang.max_string', ['string' =>   ucfirst(trans('shared.title')), 'min' => 150]) }}"
                },
                "short_description": {
                    required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('Blog::lang.short_description'))]) }}",
                    minlength: "{{ trans('lang.min_string', ['string' =>   ucfirst(trans('Blog::lang.short_description')), 'min' => 10]) }}",
                    maxlength: "{{ trans('lang.max_string', ['string' =>   ucfirst(trans('Blog::lang.short_description')), 'min' => 300]) }}"
                },
                "content": {
                    required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('Blog::lang.content'))]) }}",
                    minlength: "{{ trans('lang.min_string', ['string' =>   ucfirst(trans('Blog::lang.content')), 'min' => 100]) }}"
                },
                "cover": {
                    extension: "{{ trans('lang.file_mimes', ['file' =>  ucfirst(trans('Blog::lang.cover')), 'mimes' => 'JPEG, JPG & PNG']) }}",
                    fileSizeMax: "{{ trans('lang.file_size', ['file' => ucfirst(trans('Blog::lang.cover')), 'size' => 2]) }}"
                },
            }
        });

        CKEDITOR.replace('article-ckeditor', {
            height: 300,
            embed_provider: '//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}',
            filebrowserUploadUrl: "{{route('handleManagerAddArticleMedia', ['lang' => $lang, '_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
        });

        CKEDITOR.replace('description-ckeditor', {
            height: 100,
            embed_provider: '//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}',
            filebrowserUploadUrl: "{{route('handleManagerAddArticleMedia', ['lang' => $lang, '_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
        });
    });
</script>


 
<div class="breadcrumb">
        <h1>@isset($article){{ ucwords(trans('lang.edit')) }}@else{{ ucwords(trans('lang.new')) }}@endisset</h1>
        <ul>
            <li><a href="{{ route('showManagerHome', ['lang' => $lang]) }}">{{ ucwords(trans('lang.dashboard')) }}</a>
            </li>
       
        </ul>
    </div>

    <div class="separator-breadcrumb border-top"></div>
    <form id="article" @isset($article) action="{{ route('handleManagerEditArticle', ['lang'=> $lang, 'articleId' =>$article->id]) }}"
          @else action="{{ route('handleManagerAddArticle',['lang'=> $lang]) }}" @endisset method="post"
          enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-md-12">
                <div class="card mb-5">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">{{ ucfirst(trans('lang.title')) }}</label>
                            <div class="col-sm-10">
                                <input class="form-control" @isset($article) value="{{ $article->title }}"
                                       @endisset name="title" type="text"
                                       placeholder="{{ ucfirst(trans('lang.title')) }}">
                            </div>
                        </div>
                       <div class="form-group row">
                            <label class="col-sm-2 col-form-label"
                            >{{ ucfirst(trans('lang.category')) }}</label>
                            <div class="col-sm-10">
                            <input class="form-control" @isset($article) value="{{ $article->category }}"
                                       @endisset name="category" type="text"
                                       placeholder="{{ ucfirst(trans('lang.category')) }}">
                             
                            </div>
                        </div> 
                   
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label"
                                   >{{ ucfirst(trans('lang.cover')) }} @if(isset($article) and $article->cover)
                                    <sup><a href="{{ asset($article->cover) }}">({{ ucfirst(trans('lang.show')) }})</a></sup>@endif</label>
                            <div class="col-sm-10">
                                <input class="form-control" id="inputEmail3" name="cover" type="file"
                                       placeholder="{{ ucfirst(trans('lang.cover')) }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label"
                                   >{{ ucfirst(trans('lang.short_description')) }}</label>
                            <div class="col-sm-10">
                                    <textarea   id="format-hmtl5" name="short_description" class="form-control"
                                              rows="4">@isset($article) {!! $article->short_description !!} @endisset</textarea>
                            </div>
                        </div>
                        <fieldset class="form-group">
                            <div class="row">
                                <div class="col-form-label col-sm-2 pt-0">{{ ucfirst(trans('lang.status')) }}</div>
                                <div class="col-sm-10">
                                    <div class="form-check">
                                        <input class="form-check-input" id="status1" type="radio" name="status"
                                               @if(isset($article) and $article->status == 0) checked @endif
                                               value="0">
                                        <label class="form-check-label ml-3" for="gridRadios1">
                                            {{ ucfirst(trans('lang.draft')) }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" id="status2" type="radio" name="status"
                                               value="1" @if(isset($article) and $article->status == 1) checked
                                               @elseif(!isset($article)) checked @endif>
                                        <label class="form-check-label ml-3" for="gridRadios2">
                                            {{ ucfirst(trans('lang.online')) }}
                                        </label>
                                    </div>

                                </div>
                            </div>
                        </fieldset>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label"
                                   >{{ ucfirst(trans('lang.content')) }}</label>
                            <div class="col-sm-10">
                            <textarea
                                    id="format-hmtl5"
                                    required
                                    name="content"
                                    class="form-control"
                                    rows="6"
                            >@isset($article){!! $article->content!!}@endif</textarea>
                            </div>
                        </div>
                        <div class="col-md-12 pr-0">
                            <button class="btn btn-info m-1 float-right" type="submit">{{ ucfirst(trans('lang.save')) }}</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>



    <script src="{{asset('js/backOffice/tiny.js')}}"></script>

<script>


tinymce.init({
  selector: 'textarea',
  height: 500,
  plugins: 'visualblocks',
  style_formats: [
    { title: 'Headers', items: [
      { title: 'h1', block: 'h1' },
      { title: 'h2', block: 'h2' },
      { title: 'h3', block: 'h3' },
      { title: 'h4', block: 'h4' },
      { title: 'h5', block: 'h5' },
      { title: 'h6', block: 'h6' }
    ] },

    { title: 'Blocks', items: [
      { title: 'p', block: 'p' },
      { title: 'div', block: 'div' },
      { title: 'pre', block: 'pre' }
    ] },

    { title: 'Containers', items: [
      { title: 'section', block: 'section', wrapper: true, merge_siblings: false },
      { title: 'article', block: 'article', wrapper: true, merge_siblings: false },
      { title: 'blockquote', block: 'blockquote', wrapper: true },
      { title: 'hgroup', block: 'hgroup', wrapper: true },
      { title: 'aside', block: 'aside', wrapper: true },
      { title: 'figure', block: 'figure', wrapper: true }
    ] }
  ],
  visualblocks_default_state: true,
  end_container_on_empty_block: true,
  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
});


</script>



@endsection
