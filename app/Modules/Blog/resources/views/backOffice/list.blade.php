@extends('backOffice.layout')

@section('head')
    @include('backOffice.inc.head',
        ['title' => config('app.name') . ' - ' . ucfirst(trans('shared.administration')),
    'description' => config('app.name')
    ])
@endsection

@section('header')
    @include('backOffice.inc.header')
@endsection

@section('sidebar')
    @include('backOffice.inc.sidebar', [
        'current' => 'blog'
    ])
@endsection

@section('content')

    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/datatable.css') }}">
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatable.js') }}"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/'.$lang.'.json') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "{{ ucfirst(trans('shared.search')) }}...");
        });

    </script>

    <div class="row">
        <div class="col-md-9">
            <div class="breadcrumb">
                <h1>{{ ucwords(trans('lang.blog')) }}</h1>
                <ul>
                    <li><a href="#">{{ ucwords(trans('lang.dashboard')) }}</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-3 pull-right">
            <div class="float-right">
                <a href="{{route('showManagerAddArticle',['lang' => $lang])}}" class="btn btn-primary">{{ ucwords(trans('lang.new')) }}</a>
            </div>
        </div>
    </div>

    <div class="separator-breadcrumb border-top"></div>


    <div class="row mb-4">

        <div class="col-md-12 mb-4">
            <div class="card text-left">

                <div class="card-body">
                    <table id="datatable-responsive"
                           class="display table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>{{ ucfirst(trans('lang.title')) }}</th>
                            <th>{{ ucfirst(trans('lang.author')) }}</th>
                            <th>{{ ucfirst(trans('lang.category')) }}</th>
                            <th>{{ ucfirst(trans('lang.status')) }}</th>
                            <th>{{ ucfirst(trans('lang.date')) }}</th>
                            <th>{{ ucfirst(trans('lang.action')) }}</th>
                        </tr>
                        </thead>

                        <tfoot>
                        <tr>
                            <th>{{ ucfirst(trans('lang.title')) }}</th>
                            <th>{{ ucfirst(trans('lang.author')) }}</th>
                            <th>{{ ucfirst(trans('lang.category')) }}</th>
                            <th>{{ ucfirst(trans('lang.status')) }}</th>
                            <th>{{ ucfirst(trans('lang.date')) }}</th>
                            <th>{{ ucfirst(trans('lang.action')) }}</th>
                        </tr>
                        </tfoot>

                        <tbody>
                        @foreach($articles as $article)
                            <tr>
                                <td>{{ $article->title }}</td>
                                <td>{{ $article->author->name }}</td>
                                <td>{{ $article->category }}</td>
                                <td>{{ $article->status == 1 ? ucfirst(trans('lang.online')) : ucfirst(trans('lang.draft')) }}</td>
                                <td>{{ $article->created_at->format('d-m-Y') }}</td>
                                <td>
                                    
                                    <a title="{{ ucfirst(trans('lang.edit')) }}" href="{{ route('showManagerEditArticle', ['lang'=> $lang,'articleId' => $article->id]) }}">
                                    
                                        <img src="  {{asset('img/pencil.png')}}" alt="" style="width:25px">
                                    
                                    </a>
                                    <a title="{{ ucfirst(trans('lang.delete')) }}" href="{{ route('handleManagerDeleteArticle', ['lang'=> $lang,'articleId' => $article->id]) }}">
                              
                                        <img src="  {{asset('img/remove.png')}}" alt="" style="width:25px">
                                   
                                    </a> 

                                    
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>




@endsection
