@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head')
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection



@section('content')


<section class="breadcrumb-section wow fadeIn">
    <div class="container">
        <div class="breadcrumb-wrap">
            <a href="#"> {{ucfirst(trans('lang.home'))}} </a>
            <i>/</i>
            <span> {{ucfirst(trans('lang.blog'))}} </span>
        </div>
    </div>
</section>

<section class="inner-page-section wow fadeIn">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                  <div class="col-sm-6">
                        <div class="blog-item wow fadeInUp">
                            <div class="blog-img">
                                <a href="/post/6674/أساسيات-الكتابة-الأكاديمية">
                                    <img src="https://www.manaraa.com/Images/Posts/8413a2f8-71fe-4d9d-8b6d-b793a06356b1.png?w=360&h=270" alt="أساسيات الكتابة الأكاديمية" title="أساسيات الكتابة الأكاديمية" />
                                </a>
                            </div>
                            <div class="blog-content">
                                <h1><a href="/post/6674/أساسيات-الكتابة-الأكاديمية">أساسيات الكتابة الأكاديمية</a></h1>
                                <div class="blog-meta">
                                    <div class="date"><i class="fa fa-calendar-o"></i> 13-3-2021</div>
                                    <div class="views"><i class="fa fa-eye"></i>166</div>
                                </div>
                                <div class="blog-tags">
                                    <a class='mytag' href='/tag/أساسيات-الكتابة-الأكاديمية' />أساسيات الكتابة الأكاديمية</a><a class='mytag' href='/tag/صياغة-البحث-وكتابته' />صياغة البحث وكتابته</a><a class='mytag' href='/tag/الاقتباس-في-البحث-العلمي' />الاقتباس في البحث العلمي</a>
                                </div>
                                <div class="blog-summary">
                                    تُعرَّف الكتابة الأكاديمية بأنها أسلوب ونسق لغوي، له أدواته وألفاظه وتراكيبه وبناؤه، ودلالاته ومعانيه وصياغته وخصائصه، تكتب به البحوث، والدراسات، والرسائل، ...
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="blog-item wow fadeInUp" data-wow-delay='0.2s'>
                            <div class="blog-img">
                                <a href="/post/6673/خطوات-كتابة-بحوث-في-تكنولوجيا-التعليم">
                                    <img src="https://www.manaraa.com/Images/Posts/fab558b2-197a-4563-85e3-51eed4adaf06.png?w=360&h=270" alt="بحوث مقترحة في تكنولوجيا التعليم" title="بحوث مقترحة في تكنولوجيا التعليم" />
                                </a>
                            </div>
                            <div class="blog-content">
                                <h1><a href="/post/6673/خطوات-كتابة-بحوث-في-تكنولوجيا-التعليم">بحوث مقترحة في تكنولوجيا التعليم</a></h1>
                                <div class="blog-meta">
                                    <div class="date"><i class="fa fa-calendar-o"></i> 10-3-2021</div>
                                    <div class="views"><i class="fa fa-eye"></i>278</div>
                                </div>
                                <div class="blog-tags">
                                    <a class='mytag' href='/tag/بحوث-مقترحة-في-تكنولوجيا-التعليم' />بحوث مقترحة في تكنولوجيا التعليم</a><a class='mytag' href='/tag/خطوات-كتابة-بحوث-تكنولوجيا-التعليم' />خطوات كتابة بحوث تكنولوجيا التعليم</a><a class='mytag' href='/tag/ما-هي-تكنولوجيا-التعليم' />ما هي تكنولوجيا التعليم</a>
                                </div>
                                <div class="blog-summary">
                                    تكنولوجيا التعليم هي تطبيق منهجي للعمليات والموارد التكنولوجية ذات الصلة في التدريس، بهدف تحسين أداء الطلاب. إنه ينطوي على نهج منضبط لتحديد احتياجات الطلاب، ...
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='clearfix'></div>
                    <div class="col-sm-6">
                        <div class="blog-item wow fadeInUp">
                            <div class="blog-img">
                                <a href="/post/6672/12-رسالة-ماجستير-حول-التعلم-عن-بعد">
                                    <img src="https://www.manaraa.com/Images/Posts/a8c1a2d1-5a24-46d8-98d2-1acf7022b386.png?w=360&h=270" alt="رسائل ماجستير عن التعلم عن بعد" title="رسائل ماجستير عن التعلم عن بعد" />
                                </a>
                            </div>
                            <div class="blog-content">
                                <h1><a href="/post/6672/12-رسالة-ماجستير-حول-التعلم-عن-بعد">رسائل ماجستير عن التعلم عن بعد</a></h1>
                                <div class="blog-meta">
                                    <div class="date"><i class="fa fa-calendar-o"></i> 06-3-2021</div>
                                    <div class="views"><i class="fa fa-eye"></i>399</div>
                                </div>
                                <div class="blog-tags">
                                    <a class='mytag' href='/tag/رسائل-ماجستير-عن-التعلم-عن-بعد' />رسائل ماجستير عن التعلم عن بعد</a><a class='mytag' href='/tag/رسالة-ماجستير-حول-التعلم-عن-بعد' />رسالة ماجستير حول التعلم عن بعد</a><a class='mytag' href='/tag/تعلم،-عن-بعد' />تعلم، عن بعد</a>
                                </div>
                                <div class="blog-summary">
                                    التعلم عن بعد صعب بعض الشيء أيضاً. يتطلب الوقت والالتزام المجنون، دعنا نواجه الأمر: عندما تقوم بالتسجيل في دورة، فإن فرص ترك الدراسة أعلى بكثير من احتمالات ...
                                </div>
                            </div>
                        </div>
                    </div>
              
            
                 
              
                 
                    <div class='clearfix'></div>
                    <nav aria-label="Page navigation" style="text-align: center;">
                        <ul id="ctl00_ContentPlaceHolder1_PagesDiv" class="pagination">
                            <li class="active" style="">
                                <a href="/blog/1">1</a>
                            </li>
                            <li style="display:none;">
                                <span>1</span>
                            </li>
                            <li class="" style="">
                                <a href="/blog/2">2</a>
                            </li>
                            <li style="display:none;">
                                <span>2</span>
                            </li>
                            <li class="" style="">
                                <a href="/blog/3">3</a>
                            </li>
                       
                            <li class="" style="">
                                <a href="/blog/319">319</a>
                            </li>
                            <li style="display:none;">
                                <span>319</span>
                            </li>
                            <li id="ctl00_ContentPlaceHolder1_LiNext">
                                <a id="ctl00_ContentPlaceHolder1_hlNext" aria-label="Next" href="/blog/2"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sidebar-wrapper">
                    <div class="sidebar-item">
                        <div class="single-input-form wow fadeInUp">
                            <span id="ctl00_ContentPlaceHolder1_PostsLeft_RequiredFieldValidator1" style="color:Red;display:none;"></span>
                            <input name="ctl00$ContentPlaceHolder1$PostsLeft$txtSearch" type="text" id="ctl00_ContentPlaceHolder1_PostsLeft_txtSearch" placeholder="أدخل كلمة البحث" />
                            <input type="submit" name="ctl00$ContentPlaceHolder1$PostsLeft$btnSearch" value="بحث" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$PostsLeft$btnSearch&quot;, &quot;&quot;, true, &quot;Search&quot;, &quot;&quot;, false, false))" id="ctl00_ContentPlaceHolder1_PostsLeft_btnSearch" style="position: absolute; left: 0; height: 40px; padding: 0 10px; font-size: 14px; font-weight: bold; color: #ffffff; background: #5893dd; border: none; outline: none; width: 85px; transition: all 0.3s;" />
                        </div>
                    </div>
                    <div class="sidebar-item">
                        <div class="sidebar-title wow fadeInUp">الأقسام</div>
                        <ul class="sidebar-cats-ul">
                            <li class="wow fadeInUp"><a href="/category/1/دراسة-الماجستير-والدكتوراة">دراسة الماجستير والدكتوراة</a></li>
                            <li class="wow fadeInUp"><a href="/category/3/الكتب-والدراسات-السابقة">الكتب والدراسات السابقة</a></li>
                            <li class="wow fadeInUp"><a href="/category/4/ترجمة">ترجمة</a></li>
                            <li class="wow fadeInUp"><a href="/category/6/التحليل-الإحصائي">التحليل الإحصائي</a></li>
                            <li class="wow fadeInUp"><a href="/category/7/قبول-الجامعات-ومراكز-اللغة">قبول الجامعات ومراكز اللغة</a></li>
                            <li class="wow fadeInUp"><a href="/category/11/البحث-العلمي">البحث العلمي</a></li>
                            <li class="wow fadeInUp"><a href="/category/12/رسائل-ماجستير-ودكتوراة">رسائل ماجستير ودكتوراة</a></li>
              
                        </ul>
                    </div>
                    <div class="sidebar-item">
                        <div class="sidebar-box">
                            <div class="sidebar-title wow fadeInUp">آخر العروض </div>
                            <div class="small-news wow fadeInUp">
                                <a href="#">
                                    <img src="https://www.manaraa.com/Images/Posts/ea30510e-eb49-4a6d-a8d6-b2dc323540d0.jpg?mode=crop&w=60&h=45" alt="احصل على الإطار النظري مع ضمانة أن تكون نسبة التماثل أقل من 9%" title="احصل على الإطار النظري مع ضمانة أن تكون نسبة التماثل أقل من 9%" />
                                    <span>احصل على الإطار النظري مع ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="#">
                                    <img src="https://www.manaraa.com/Images/Posts/ed1feb03-43bb-4158-bb65-30f81bdd7385.jpg?mode=crop&w=60&h=45" alt="احصل على خصم 100 ريال عند طلب توفير وتلخيص أربع دراسات أو أكثر" title="احصل على خصم 100 ريال عند طلب توفير وتلخيص أربع دراسات أو أكثر" />
                                    <span>احصل على خصم 100 ريال عند طلب ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="#">
                                    <img src="https://www.manaraa.com/Images/Posts/67054ec0-59bf-4251-8abe-4de8e2878559.jpg?mode=crop&w=60&h=45" alt="احصل على ترجمة للسيرة الذاتية ب 120 دولار خلال يومين من العمل" title="احصل على ترجمة للسيرة الذاتية ب 120 دولار خلال يومين من العمل" />
                                    <span>احصل على ترجمة للسيرة الذاتية ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="#">
                                    <img src="https://www.manaraa.com/Images/Posts/d6469bfa-0ed1-47f9-84ce-337da62ebd4c.jpg?mode=crop&w=60&h=45" alt="احصل على خصم بقيمة 600 ريال سعودي، وكتابة التوصيات مجاناً عند طلب خدمة التحليل الإحصائي" title="احصل على خصم بقيمة 600 ريال سعودي، وكتابة التوصيات مجاناً عند طلب خدمة التحليل الإحصائي" />
                                    <span>احصل على خصم بقيمة 600 ريال ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="#">
                                    <img src="https://www.manaraa.com/Images/Posts/05200ac1-47cf-4916-8afa-b09ffbff45a6.jpg?mode=crop&w=60&h=45" alt="احصل على فحص السرقة الأدبية مجاناً عند طلب المساعدة في كتابة خطة البحث" title="احصل على فحص السرقة الأدبية مجاناً عند طلب المساعدة في كتابة خطة البحث" />
                                    <span>احصل على فحص السرقة الأدبية ...</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar-item">
                        <div class="sidebar-box">
                            <div class="sidebar-title wow fadeInUp">الزوار شاهدوا أيضاً</div>
                            <div class="small-news wow fadeInUp">
                                <a href="#">
                                    <img src="https://www.manaraa.com/Images/Posts/0b80ff5e-1443-45b3-b953-5f3a79ecaffb.jpg?mode=crop&w=60&h=45" alt="رسائل ماجستير ودكتوراه PDF جاهزة للتحميل المباشر [محدث]" title="رسائل ماجستير ودكتوراه PDF جاهزة للتحميل المباشر [محدث]" />
                                    <span>رسائل ماجستير ودكتوراه PDF ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="#">
                                    <img src="https://www.manaraa.com/Images/Posts/fb7e8f83-57cd-444f-9f33-c97128415b96.jpg?mode=crop&w=60&h=45" alt="أحدث 50 رسالة ماجستير ودكتوراه كاملة للتحميل" title="أحدث 50 رسالة ماجستير ودكتوراه كاملة للتحميل" />
                                    <span>أحدث 50 رسالة ماجستير ودكتوراه ...</span>
                                </a>
                            </div>
                       
                        </div>
                    </div>
                 
             
                </div>
            </div>
        </div>
    </div>
</section>

@endsection