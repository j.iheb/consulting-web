@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head',
['title' => ' - Administration',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection






@section('content')

<div 
 @if ($lang == "ar")
  dir="rtl"
 @else 
   dir="ltr"
 @endif   

>

<section class="breadcrumb-section wow fadeIn">
    <div class="container">
        <div class="breadcrumb-wrap">
            <a href="/">
                {{ucfirst(trans('lang.home'))}}
            </a>
            <i>/</i>
            <a href="/Blog">
                {{ucfirst(trans('lang.blog'))}}
            </a>
            <i>/</i>
            <span>
                <span id="ctl00_ContentPlaceHolder1_lblTitle"> {{$article->title}} </span>
            </span>
        </div>
    </div>
</section>
<section class="inner-page-section wow fadeIn">
    <div class="container  ">
        <div class="row ">
            <div class="col-md-8">
                <div class="single-blog-content">
                    <h1 class="title"> {{$article->title}} </h1>
                    <div class="main-img text-center">
                        <img src="{{asset($article->cover)}}" alt="كيفية تجنب الاقتباس الحرفي" title="كيفية تجنب الاقتباس الحرفي" />
                    </div>
                    <div class="meta">
                        <div class="date-time">
                            <a href="#" class="item"><i class="fa fa-tags"></i><span> {{ucfirst(trans('lang.category'))}} </span></a>

                        </div>
                        <div class="blog-tags">
                            <a class='mytag' href="#"> {{ $article->category}} </a>
                        </div>

                    </div>
                    <div class="blog-text blog-class">
                     
                        <div>
                           
                           {!! $article->short_description !!}
                            <hr />
                           
                            {!! $article->content !!}
                         </div>
                       
                     
                   </div>
                </div>
            
            </div>
            <div class="col-md-4">
                <div class="sidebar-wrapper">
                <!--
                    <div class="sidebar-item">
                        <div class="single-input-form wow fadeInUp">
                            <span id="ctl00_ContentPlaceHolder1_PostsLeft2_RequiredFieldValidator1" style="color:Red;display:none;"></span>
                            <input name="ctl00$ContentPlaceHolder1$PostsLeft2$txtSearch" type="text" id="ctl00_ContentPlaceHolder1_PostsLeft2_txtSearch" placeholder="أدخل كلمة البحث" />
                            <input type="submit" name="ctl00$ContentPlaceHolder1$PostsLeft2$btnSearch" value="بحث" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$PostsLeft2$btnSearch&quot;, &quot;&quot;, true, &quot;Search&quot;, &quot;&quot;, false, false))" id="ctl00_ContentPlaceHolder1_PostsLeft2_btnSearch" style="position: absolute; left: 0; height: 40px; padding: 0 10px; font-size: 14px; font-weight: bold; color: #ffffff; background: #5893dd; border: none; outline: none; width: 85px; transition: all 0.3s;" />
                        </div>
                    </div>
                    
                    <div class="sidebar-item">
                        <div class="sidebar-title wow fadeInUp">الأقسام</div>
                        <ul class="sidebar-cats-ul">
                            <li class="wow fadeInUp"><a href="/category/1/دراسة-الماجستير-والدكتوراة">دراسة الماجستير والدكتوراة</a></li>
                            <li class="wow fadeInUp"><a href="/category/3/الكتب-والدراسات-السابقة">الكتب والدراسات السابقة</a></li>
                            <li class="wow fadeInUp"><a href="/category/4/ترجمة">ترجمة</a></li>
                            <li class="wow fadeInUp"><a href="/category/6/التحليل-الإحصائي">التحليل الإحصائي</a></li>
                            <li class="wow fadeInUp"><a href="/category/7/قبول-الجامعات-ومراكز-اللغة">قبول الجامعات ومراكز اللغة</a></li>
                            <li class="wow fadeInUp"><a href="/category/11/البحث-العلمي">البحث العلمي</a></li>
                            <li class="wow fadeInUp"><a href="/category/12/رسائل-ماجستير-ودكتوراة">رسائل ماجستير ودكتوراة</a></li>
                            <li class="wow fadeInUp"><a href="/category/14/عناوين-رسائل-ماجستير-والدكتوراة">عناوين رسائل ماجستير والدكتوراة</a></li>
                            <li class="wow fadeInUp"><a href="/category/15/خطة-البحث-العلمي">خطة البحث العلمي</a></li>
                            <li class="wow fadeInUp"><a href="/category/17/الإطار-النظري">الإطار النظري</a></li>
                            <li class="wow fadeInUp"><a href="/category/18/انفوجرافيك-البحث-العلمي">انفوجرافيك البحث العلمي</a></li>
                            <li class="wow fadeInUp"><a href="/category/20/وظائف-شاغرة">وظائف شاغرة</a></li>
                            <li class="wow fadeInUp"><a href="/category/21/نماذج-أعمالنا-وتجربتي-مع-المنارة">نماذج أعمالنا وتجربتي مع المنارة</a></li>
                            <li class="wow fadeInUp"><a href="/category/25/نشر-الأبحاث-العلمية">نشر الأبحاث العلمية</a></li>
                            <li class="wow fadeInUp"><a href="/category/28/مقالات-علمية">مقالات علمية</a></li>
                            <li class="wow fadeInUp"><a href="/category/29/التعليم-عن-بعد">التعليم عن بعد</a></li>
                            <li class="wow fadeInUp"><a href="/category/30/خدمات-المنارة-للاستشارات">خدمات المنارة للاستشارات</a></li>
                        </ul>
                    </div>
                    <div class="sidebar-item">
                        <div class="sidebar-box">
                            <div class="sidebar-title wow fadeInUp">آخر العروض </div>
                            <div class="small-news wow fadeInUp">
                                <a href="/offer/6670/احصل-على-الإطار-النظري-مع-نسبة-تماثل-أقل-من-9">
                                    <img src="/Images/Posts/ea30510e-eb49-4a6d-a8d6-b2dc323540d0.jpg?mode=crop&w=60&h=45" alt="احصل على الإطار النظري مع ضمانة أن تكون نسبة التماثل أقل من 9%" title="احصل على الإطار النظري مع ضمانة أن تكون نسبة التماثل أقل من 9%" />
                                    <span>احصل على الإطار النظري مع ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="/offer/6669/احصل-على-خصم-100-ر.س-عند-طلب-توفير-وتلخيص">
                                    <img src="/Images/Posts/ed1feb03-43bb-4158-bb65-30f81bdd7385.jpg?mode=crop&w=60&h=45" alt="احصل على خصم 100 ريال عند طلب توفير وتلخيص أربع دراسات أو أكثر" title="احصل على خصم 100 ريال عند طلب توفير وتلخيص أربع دراسات أو أكثر" />
                                    <span>احصل على خصم 100 ريال عند طلب ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="/offer/6668/احصل-على-ترجمة-للسيرة-الذاتية-ب-120$-خلال-يومين-من-العمل">
                                    <img src="/Images/Posts/67054ec0-59bf-4251-8abe-4de8e2878559.jpg?mode=crop&w=60&h=45" alt="احصل على ترجمة للسيرة الذاتية ب 120 دولار خلال يومين من العمل" title="احصل على ترجمة للسيرة الذاتية ب 120 دولار خلال يومين من العمل" />
                                    <span>احصل على ترجمة للسيرة الذاتية ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="/offer/6667/خصم-على-خدمة-التحليل-الإحصائي-ومناقشة-النتائج">
                                    <img src="/Images/Posts/d6469bfa-0ed1-47f9-84ce-337da62ebd4c.jpg?mode=crop&w=60&h=45" alt="احصل على خصم بقيمة 600 ريال سعودي، وكتابة التوصيات مجاناً عند طلب خدمة التحليل الإحصائي" title="احصل على خصم بقيمة 600 ريال سعودي، وكتابة التوصيات مجاناً عند طلب خدمة التحليل الإحصائي" />
                                    <span>احصل على خصم بقيمة 600 ريال ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="/offer/6666/احصل-على-فحص-السرقة-الأدبية-مجاناً">
                                    <img src="/Images/Posts/05200ac1-47cf-4916-8afa-b09ffbff45a6.jpg?mode=crop&w=60&h=45" alt="احصل على فحص السرقة الأدبية مجاناً عند طلب المساعدة في كتابة خطة البحث" title="احصل على فحص السرقة الأدبية مجاناً عند طلب المساعدة في كتابة خطة البحث" />
                                    <span>احصل على فحص السرقة الأدبية ...</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar-item">
                        <div class="sidebar-box">
                            <div class="sidebar-title wow fadeInUp">الزوار شاهدوا أيضاً</div>
                            <div class="small-news wow fadeInUp">
                                <a href="/Post/2079/رسائل-ماجستير-ودكتوراه-PDF-جاهزة-للتحميل-المباشر-[محدث]">
                                    <img src="/Images/Posts/0b80ff5e-1443-45b3-b953-5f3a79ecaffb.jpg?mode=crop&w=60&h=45" alt="رسائل ماجستير ودكتوراه PDF جاهزة للتحميل المباشر [محدث]" title="رسائل ماجستير ودكتوراه PDF جاهزة للتحميل المباشر [محدث]" />
                                    <span>رسائل ماجستير ودكتوراه PDF ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="/Post/2077/أحدث-50-رسالة-ماجستير-ودكتوراه-كاملة-للتحميل">
                                    <img src="/Images/Posts/fb7e8f83-57cd-444f-9f33-c97128415b96.jpg?mode=crop&w=60&h=45" alt="أحدث 50 رسالة ماجستير ودكتوراه كاملة للتحميل" title="أحدث 50 رسالة ماجستير ودكتوراه كاملة للتحميل" />
                                    <span>أحدث 50 رسالة ماجستير ودكتوراه ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="/Post/3547/نماذج-اختبار-كفايات-اللغة-الانجليزية-STEP">
                                    <img src="/Images/Posts/000374c7-e0f9-4e64-a7e3-58ae5b30e996.webp?mode=crop&w=60&h=45" alt="نماذج اختبار كفايات اللغة الانجليزية STEP" title="نماذج اختبار كفايات اللغة الانجليزية STEP" />
                                    <span>نماذج اختبار كفايات اللغة ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="/Post/2207/رسائل-ماجستير-ودكتوراه-تربوية-وحديثة-(تحميل-مباشر-وسريع)">
                                    <img src="/Images/Posts/522ce425-90ea-46a4-a1fc-10818f8596d8.jpg?mode=crop&w=60&h=45" alt="رسائل ماجستير ودكتوراه تربوية وحديثة (تحميل مباشر وسريع)" title="رسائل ماجستير ودكتوراه تربوية وحديثة (تحميل مباشر وسريع)" />
                                    <span>رسائل ماجستير ودكتوراه تربوية ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="/Post/2340/خطوات-عمل-رسالة-الماجستير-شرح-وصورة-انفوجرافيك">
                                    <img src="/Images/Posts/3ddb74fe-e961-4ca2-84ac-e5dc4e942f1e.jpg?mode=crop&w=60&h=45" alt="خطوات عمل رسالة الماجستير شرح وصورة انفوجرافيك" title="خطوات عمل رسالة الماجستير شرح وصورة انفوجرافيك" />
                                    <span>خطوات عمل رسالة الماجستير شرح ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="/Post/2182/مجاناً-تحميل-رسائل-ماجستير-ودكتوراه-PDF-">
                                    <img src="/Images/Posts/0984af00-fe6b-4838-9e7a-d1884a36aa06.jpg?mode=crop&w=60&h=45" alt="مجاناً تحميل رسائل ماجستير ودكتوراه PDF " title="مجاناً تحميل رسائل ماجستير ودكتوراه PDF " />
                                    <span>مجاناً تحميل رسائل ماجستير ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="/Post/3101/رسائل-ماجستير-ودكتوراه-في-علم-النفس">
                                    <img src="/Images/Posts/96c2ac61-42b1-42c0-8ea9-9a353182b07f.jpg?mode=crop&w=60&h=45" alt="رسائل ماجستير ودكتوراه في علم النفس" title="رسائل ماجستير ودكتوراه في علم النفس" />
                                    <span>رسائل ماجستير ودكتوراه في علم ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="/Post/2444/مناهج-البحث-العلمي-وأنواعها-وكيفية-استخدامها">
                                    <img src="/Images/Posts/2177d094-8272-4763-91c6-35f0a339f7ec.jpg?mode=crop&w=60&h=45" alt="مناهج البحث العلمي وأنواعها وكيفية استخدامها" title="مناهج البحث العلمي وأنواعها وكيفية استخدامها" />
                                    <span>مناهج البحث العلمي وأنواعها ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="/Post/2026/كيفية-كتابة-مشكلة-الدراسة-وما-هي-ضوابطها؟">
                                    <img src="/Images/Posts/79eaa43c-10a3-497e-860c-7bc4b40ac37f.png?mode=crop&w=60&h=45" alt="كيفية كتابة مشكلة الدراسة وما هي ضوابطها؟" title="كيفية كتابة مشكلة الدراسة وما هي ضوابطها؟" />
                                    <span>كيفية كتابة مشكلة الدراسة وما ...</span>
                                </a>
                            </div>
                            <div class="small-news wow fadeInUp">
                                <a href="/Post/2196/رسائل-ماجستير-ودكتوراه-جديدة-[جاهزة-للتحميل]">
                                    <img src="/Images/Posts/b5bcbc4d-1e94-4010-807e-78210fccb9a6.jpeg?mode=crop&w=60&h=45" alt="رسائل ماجستير ودكتوراه جديدة [جاهزة للتحميل]" title="رسائل ماجستير ودكتوراه جديدة [جاهزة للتحميل]" />
                                    <span>رسائل ماجستير ودكتوراه جديدة ...</span>
                                </a>
                            </div>
                        </div>
                    </div> 
                    <div class="sidebar-item">
                        <div class="sidebar-title wow fadeInUp">تابعنا على الفيسبوك </div>
                        <div class="fb-page" data-href="https://www.facebook.com/manaraao/" data-tabs="" data-width="250" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                            <blockquote cite="https://www.facebook.com/manaraao/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/manaraao/">‎مؤسسة المنارة للاستشارات‎</a></blockquote>
                        </div>
                    </div>
                    <div class="sidebar-item">
                        <div class="sidebar-title wow fadeInUp">الوسوم</div>
                        <div class="blog-tags sidebar-tags">
                            <a href="/tag/البحث-العلمي">البحث العلمي</a>
                            <a href="/tag/بحث">بحث</a>
                            <a href="/tag/البحث">البحث</a>
                            <a href="/tag/الباحث-">الباحث </a>
                            <a href="/tag/الباحث-العلمي">الباحث العلمي</a>
                            <a href="/tag/ماجستير">ماجستير</a>
                            <a href="/tag/باحث">باحث</a>
                            <a href="/tag/العلمي">العلمي</a>
                            <a href="/tag/كتابة">كتابة</a>
                            <a href="/tag/خطة-البحث-">خطة البحث </a>
                            <a href="/tag/دكتوراه-">دكتوراه </a>
                            <a href="/tag/رسائل">رسائل</a>
                            <a href="/tag/عناوين">عناوين</a>
                            <a href="/tag/علمي-">علمي </a>
                            <a href="/tag/دراسة">دراسة</a>
                            <a href="/tag/خطة-بحث-">خطة بحث </a>
                            <a href="/tag/رسائل-ماجستير">رسائل ماجستير</a>
                            <a href="/tag/خطة">خطة</a>
                            <a href="/tag/-البحث-العلمي"> البحث العلمي</a>
                            <a href="/tag/الدراسات-السابقة">الدراسات السابقة</a>
                            <a href="/tag/عناوين-رسائل-ماجستير">عناوين رسائل ماجستير</a>
                            <a href="/tag/رسائل-ماجستير-ودكتوراه">رسائل ماجستير ودكتوراه</a>
                            <a href="/tag/ترجمة">ترجمة</a>
                            <a href="/tag/عناوين-رسائل">عناوين رسائل</a>
                            <a href="/tag/دراسة-الماجستير">دراسة الماجستير</a>
                            <a href="/tag/الدراسة">الدراسة</a>
                            <a href="/tag/بحث-علمي-">بحث علمي </a>
                            <a href="/tag/عملية">عملية</a>
                            <a href="/tag/بيانات">بيانات</a>
                            <a href="/tag/الماجستير">الماجستير</a>
                        </div>
                    </div>
                    -->
                </div>
            </div>
        </div>
    </div>
</section>
</div>
@endsection