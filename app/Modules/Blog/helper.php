<?php


if (!function_exists('transformBlogStatus')) {
    /**
     * @param integer $status
     * @return string
     */
    function transformBlogStatus($status)
    {
        switch ($status){
            case 0 :
                return ucfirst(trans('shared.draft'));
            case 1:
                return ucfirst(trans('shared.online'));
            default:
                return '';
        }
    }
}

if (!function_exists('transformArticleCategoryTitle')) {
    /**
     * @param \App\Modules\Blog\Models\ArticleCategory $category
     * @return string
     */
    function transformArticleCategoryTitle($category)
    {
        $id = \Vinkla\Hashids\Facades\Hashids::encodeHex($category->id);
        $title = urlencode($category->title);
        return $id . '-' . $title;
    }
}


if (!function_exists('transformArticleTitle')) {
    /**
     * @param \App\Modules\Blog\Models\Article $article
     * @return string
     */
    function transformArticleTitle($article)
    {
        $id = \Vinkla\Hashids\Facades\Hashids::encodeHex($article->id);
        $title = urlencode($article->title);

        return $id . '-' . $title;
    }
}
