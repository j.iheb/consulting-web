<?php

namespace App\Modules\Blog\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model {

    use SoftDeletes;

    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'blog_articles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'category',
        'author_id',
        'title',
        'short_description',  
        'content',
        'cover',
        'status',
        'views',
        'lang'
    ];

  
    public function author()
    {
        return $this->hasOne('App\Modules\User\Models\User', 'id', 'author_id');
    }


    public function service()
    {
        return $this->hasOne('App\Modules\General\Models\Service', 'id', 'service_id'); 
    }

}
