@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head',
['title' => ' - Administration',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection






@section('content')


<section class="main-slider" style="height: 600px;">
    <div id="main-slider" class="carousel slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators">
            <li data-target="#main-slider" data-slide-to="0" class="active"></li>
            <li data-target="#main-slider" data-slide-to="1" class=""></li>
        
        </ol>
  

        <div class="carousel-inner" role="listbox">

           <!-- <div class="item active">
                <img src="https://www.manaraa.com/Images/Slider/2de7a8cd-e835-4414-86d2-3dbb3ebc30d0.jpg" />
                <div class="slide-caption">
                    {!! ucfirst(trans('lang.slider-1')) !!}
                    <a href="#" class="animated fadeInDown" style="display: ">
                      {{ ucfirst(trans('lang.free_consultations')) }}
                    </a>
                </div>
            </div>
           -->
            @foreach($sliders as $slider)
           
            <div class="item  @if ($loop->first)  active   @endif">
                <img src="{{asset($slider->photo)}}" />
                <div class="slide-caption">
                    <h3>  {{$slider->title}} </h3>
                    <p> {{$slider->description}}</p>
                    @if ($slider->link)
                    <a href="{{$slider->link}}"  target="_blank" class="animated fadeInDown" style="display: ">
                            {{ ucfirst(trans('lang.details')) }}
                    </a>
                    @endif
                </div>
            </div>

            @endforeach

            <!--
            <div class="item ">
                <img src="https://www.manaraa.com/Images/Slider/37f013bf-5bfb-4659-b45d-90f57d02c7c9.jpg" />
                <div class="slide-caption">
                {!! ucfirst(trans('lang.slider-1')) !!}
                    <a href="#" class="animated fadeInDown" style="display: ">
                    {{ ucfirst(trans('lang.details')) }}
                    </a>
                </div>
            </div>
            <div class="item ">
                <img src="https://www.manaraa.com/slider2.jpg" />
                <div class="slide-caption">
                {!! ucfirst(trans('lang.slider-1')) !!}
                    <a href="#" class="animated fadeInDown" style="display: ">
                    {{ ucfirst(trans('lang.details')) }}
                    </a>
                </div>
            </div>
            <div class="item ">
                <img src="https://www.manaraa.com/slider3.jpg" />
                <div class="slide-caption">
                {!! ucfirst(trans('lang.slider-1')) !!}
                    <a href="#" class="animated fadeInDown" style="display: ">
                    {{ ucfirst(trans('lang.details')) }}
                    </a>
                </div>
            </div> -->
        </div>

        <a class="left slider-control" href="#main-slider" data-slide="next"><span class="fa fa-chevron-left"></span></a>
        <a class="right slider-control" href="#main-slider" data-slide="prev"> <span class="fa fa-chevron-right" ></span></a>
    </div>
</section>
<section class="services wow fadeIn">
    <div class="container">
        <div class="section-title text-center wow fadeInUp">
            <h1> {{ ucfirst(trans('lang.our_services')) }} </h1>
        </div>
        <div class="row">

          @foreach($services as $service)

            <div class="col-xs-6 col-md-3">
                <div class="service-item wow fadeInUp" data-wow-delay="0.0s">
                    <img src="{{asset($service->photo)}}" style="height: 390px;" />
                    <div class="service-caption">
                        <div class="service-caption-wrap">
                            <h3> {{$service->title}} </h3>
                            <p> {{$service->short_description}}</p>
                            <a href="{{route('showAddFreeConsultation',['lang' => $lang])}}" class="btn btn-bordered service-details"> {{ucfirst(trans('lang.free_consultations'))}} </a>
                            <a href="{{route('showAddConsultation',['lang' => $lang])}}" class="btn btn-bordered service-details"> {{ucfirst(trans('lang.service_request'))}} </a>
                           <a href="{{route('showSingleService',['lang'=>$lang, 'id' => $service->id])}}" class="btn btn-bordered service-details"> {{ucfirst(trans('lang.details'))}} </a> 
                        </div>
                    </div>
                </div>
            </div>

        

            @endforeach

        </div>
    </div>
</section>

<section class="team-section wow fadeIn">
    <div class="container">
        <div class="section-title text-center wow fadeInUp">
            <h2> {{ucfirst(trans('lang.advisory_body'))}} </h2>
        </div>
        <div class="owl-theme owl-carousel three-slide-carousel team-carousel">
            @foreach($advisers as $adviser)
            <div class="team-item">
                <div class="team-img">
                    @if($adviser->photo)
                    <img src="{{asset($adviser->photo)}}" style="height:300px; width:250px;" />
                    @else 
                    <img src="{{asset('img/unknow.jpeg')}}" style="height:300px; width:250px;"/>
                    @endif
                </div>
                <div class="team-content">
                @if($lang == "ar")
                    <h3> {{$adviser->name_ar}}</h3>
                    <span> {{$adviser->title_ar}} </span>
                    <p>
                       {{$adviser->short_description_ar}}
                    </p>
                @else 
                <h3> {{$adviser->name}}</h3>
                    <span> {{$adviser->title}} </span>
                    <p>
                       {{$adviser->short_description}}
                    </p>
                @endif
                </div>
            </div>
            @endforeach
      
            

    
        </div>
    </div>
</section>





<section class="team-section wow fadeIn">
    <div class="container">
        <div class="section-title text-center wow fadeInUp">
            <h2> {{ ucfirst(trans('lang.our_partners')) }} </h2>
        </div>
        <div class="owl-theme owl-carousel three-slide-carousel team-carousel">

        @foreach($partners as $partner)
            <div class="team-item">
                <div class="team-img">
                    <img src="{{asset($partner->photo)}}" />
                </div>
                <div class="team-content">
                    @if($lang == 'ar')
                    <h3>  {{$partner->name_ar}} </h3>
                   <span>  {{$partner->description_ar}} </span>
                    @else 
                    <h3>  {{$partner->name}} </h3>
                   <span>  {{$partner->description}} </span>
                    @endif
                    <p>
                    </p>
                </div>
            </div>
        @endforeach    
         
        </div>
    </div>
</section>




<section class="service-order-section wow fadeIn">
    <div class="container">
        <div class="service-order-wrap">
            <div class="text">
                <h3 class="wow fadeInDown">{{ucfirst(trans('lang.service_request'))}}  </h3>
                <p class="wow fadeInDown">
                    {{ucfirst(trans('lang.mughtarib_welcome'))}}
            </p>
            </div>
            <a href="{{route('showAddConsultation',['lang' => $lang])}}" class="btn btn-bordered link wow fadeInDown " style="margin-right: 10px;;">{{ucfirst(trans('lang.service_request'))}} </a>
            <a href="{{route('showAddFreeConsultation',['lang' => $lang])}}" class="btn btn-bordered link wow fadeInDown">  {{ucfirst(trans('lang.free_consultations'))}} </a>
        </div>
    </div>
</section>
<section class="steps-section wow fadeIn" style="background-image: url(https://www.manaraa.com/Images/Pages/a7e5de08-4bb5-43c5-b684-2c09a53b24b0.jpg)">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="row order-steps">

                    @foreach($ourServices as $service)
                    <div class="col-xs-6">
                        <div class="step-item wow fadeInUp">
                            <h3> {{$service->title}} </h3>
                            <p><a href="#" style="color:dark_gray"> {{$service->description}} </a></p>
                        </div>
                    </div>
               
                    @endforeach
                </div>
            </div>
            <div class="col-md-6 video-script">
                <img src="{{asset('img/home-2.jpg')}}" alt="home deux jpg" style="width: 100%; height:100%;">
           </div>
        </div>
    </div>
</section>
<section class="counters wow fadeIn">
    <div class="container">
        <div class="section-title text-center wow fadeInUp">
            <h2> {{ucfirst(trans('lang.growth'))}} </h2>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="counter-item text-center wow fadeInUp">
                <div id="ctl00_ContentPlaceHolder1_lblPostsCountDiv" class="counter-number" data-count="{{$articleNumber}}">{{$articleNumber}}</div>
                <h3> {{ucfirst(trans('lang.blog_articles'))}} </h3>
            
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="counter-item text-center wow fadeInUp" data-wow-delay="0.2s">
                <div id="ctl00_ContentPlaceHolder1_lblExpDaysDiv" class="counter-number" data-count="{{$freeConsult}}">{{$freeConsult}}</div>
                <h3> {{ucfirst(trans('lang.free_consultations'))}} </h3>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="counter-item text-center wow fadeInUp" data-wow-delay="0.2s">
                <div id="ctl00_ContentPlaceHolder1_lblExpDaysDiv" class="counter-number" data-count="{{$paidConsult}}">{{$paidConsult}}</div>
                <h3> {{ucfirst(trans('lang.service_request'))}} </h3>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="counter-item text-center wow fadeInUp" data-wow-delay="0.4s">
                <div id="ctl00_ContentPlaceHolder1_lblVisitsCountDiv" class="counter-number" data-count="23451575">23,451,575</div>
                <h3> {{ucfirst(trans('lang.visit_number'))}}  </h3>
            </div>
        </div>
    </div>
</section>





<section class="offers-section wow fadeIn" style="background-image: url(https://www.manaraa.com/assets_home/backgrounds/bg-82.jpg)">
    <div class="container">
        <div class="section-title text-center wow fadeInUp">
            <h2 style="color:#fff;"> {{ucfirst(trans('lang.programs_and_offers'))}} </h2>
        </div>
        <div class="row">
          
            <div class="col-xs-6 col-sm-6 col-md-3">
                <div class="offer-item wow fadeInUp" data-wow-delay="0.2s">
                    <a href="#">
                        <img src="https://www.manaraa.com/Images/Posts/ed1feb03-43bb-4158-bb65-30f81bdd7385.jpg" />
                        <div class="offer-caption">
                            <h3>احصل على خصم 100 ريال عند طلب توفير وتلخيص أربع دراسات أو أكثر </h3>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <div class="offer-item wow fadeInUp" data-wow-delay="0.4s">
                    <a href="#">
                        <img src="https://www.manaraa.com/Images/Posts/67054ec0-59bf-4251-8abe-4de8e2878559.jpg" />
                        <div class="offer-caption">
                            <h3>احصل على ترجمة للسيرة الذاتية ب 120 دولار خلال يومين من العمل </h3>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <div class="offer-item wow fadeInUp" data-wow-delay="0.6s">
                    <a href="#">
                        <img src="https://www.manaraa.com/Images/Posts/d6469bfa-0ed1-47f9-84ce-337da62ebd4c.jpg" />
                        <div class="offer-caption">
                            <h3>احصل على خصم بقيمة 600 ريال سعودي، وكتابة التوصيات مجاناً عند طلب خدمة التحليل الإحصائي </h3>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <div class="offer-item wow fadeInUp" data-wow-delay="0.8s">
                    <a href="#">
                        <img src="https://www.manaraa.com/Images/Posts/05200ac1-47cf-4916-8afa-b09ffbff45a6.jpg" />
                        <div class="offer-caption">
                            <h3>احصل على فحص السرقة الأدبية مجاناً عند طلب المساعدة في كتابة خطة البحث </h3>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-xs-6 col-sm-6 col-md-3">
                <div class="offer-item wow fadeInUp" data-wow-delay="0.2s">
                    <a href="#">
                        <img src="https://www.manaraa.com/Images/Posts/ed1feb03-43bb-4158-bb65-30f81bdd7385.jpg" />
                        <div class="offer-caption">
                            <h3>احصل على خصم 100 ريال عند طلب توفير وتلخيص أربع دراسات أو أكثر </h3>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <div class="offer-item wow fadeInUp" data-wow-delay="0.4s">
                    <a href="#">
                        <img src="https://www.manaraa.com/Images/Posts/67054ec0-59bf-4251-8abe-4de8e2878559.jpg" />
                        <div class="offer-caption">
                            <h3>احصل على ترجمة للسيرة الذاتية ب 120 دولار خلال يومين من العمل </h3>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <div class="offer-item wow fadeInUp" data-wow-delay="0.6s">
                    <a href="#">
                        <img src="https://www.manaraa.com/Images/Posts/d6469bfa-0ed1-47f9-84ce-337da62ebd4c.jpg" />
                        <div class="offer-caption">
                            <h3>احصل على خصم بقيمة 600 ريال سعودي، وكتابة التوصيات مجاناً عند طلب خدمة التحليل الإحصائي </h3>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3">
                <div class="offer-item wow fadeInUp" data-wow-delay="0.8s">
                    <a href="#">
                        <img src="https://www.manaraa.com/Images/Posts/05200ac1-47cf-4916-8afa-b09ffbff45a6.jpg" />
                        <div class="offer-caption">
                            <h3>احصل على فحص السرقة الأدبية مجاناً عند طلب المساعدة في كتابة خطة البحث </h3>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="team-section wow fadeIn">
    <div class="container">
        <div class="section-title text-center wow fadeInUp">
            <h2> {{ ucfirst(trans('lang.customers_service')) }} </h2>
        </div>
        <div class="owl-theme owl-carousel three-slide-carousel team-carousel">

        @foreach($officers as $adviser)
            <div class="team-item">
                <div class="team-img">
                    @if($adviser->photo)
                    <img src="{{asset($adviser->photo)}}" style="height:300px; width:250px;" />
                    @else 
                    <img src="{{asset('img/unknow.jpeg')}}" style="height:300px; width:250px;"/>
                    @endif
                </div>
                <div class="team-content">
                @if($lang == "ar")
                    <h3> {{$adviser->name_ar}}</h3>
                    <span> {{$adviser->title_ar}} </span>
                    <p>
                       {{$adviser->short_description_ar}}
                    </p>
                @else 
                <h3> {{$adviser->name}}</h3>
                    <span> {{$adviser->title}} </span>
                    <p>
                       {{$adviser->short_description}}
                    </p>
                @endif
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<section class="blog-section wow fadeIn">
    <div class="container">
        <div class="section-title text-center wow fadeInUp">
            <h2> {{ ucfirst(trans('lang.mughrabi_consulting_blog')) }} </h2>
        </div>
        <div class="owl-theme owl-carousel three-slide-carousel blog-carousel">

            @foreach($articles as $article)
            <div class="blog-item">
                <div class="blog-img">
                    <a href="{{route('showSingleArticle',['lang' => $lang , 'id' => $article->id])}}">
                        <img src="{{asset($article->cover)}}" style="width: 400px; height:300px;" alt="{{$article->title}}" title="{{$article->title}}" />
                    </a>
                </div>
                <div class="blog-content">
                    <h1><a href="#"></a></h1>
                    <div class="blog-meta">
                        <div class="date"><i class="fa fa-calendar-o"></i> {{$article->created_at->format('Y-m-d')}} </div>
                       <!-- <div class="views"><i class="fa fa-eye"></i>53</div> -->
                    </div>
                    <div class="blog-tags">
                        <a href="{{route('showSingleArticle',['lang' => $lang , 'id' => $article->id])}}"> {{$article->category}} </a>
                    </div>
                    <div class="blog-summary">
                    <a href="{{route('showSingleArticle',['lang' => $lang , 'id' => $article->id])}}"> 
                        {!! $article->title !!}
                    </a>    
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

<script type="text/javascript" src="{{ asset('js/frontOffice/jquery-1.9.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/bootstrap.min.js') }}"></script>
     <script type="text/javascript" src="{{ asset('js/frontOffice/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/script.js?c=1') }}"></script>

@endsection