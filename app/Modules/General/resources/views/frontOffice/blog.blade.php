@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head')
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection



@section('content')

<section class="blog-section wow fadeIn">
    <div class="container">
        <div class="section-title text-center wow fadeInUp">
            <h2> {{ ucfirst(trans('lang.mughrabi_consulting_blog')) }} </h2>
        </div>
        <div class="owl-theme owl-carousel three-slide-carousel blog-carousel">

            @foreach($articles as $article)
            <div class="blog-item">
                <div class="blog-img">
                    <a href="{{route('showSingleArticle',['lang' => $lang , 'id' => $article->id])}}">
                        <img src="{{asset($article->cover)}}" style="width: 400px; height:300px;" alt="{{$article->title}}" title="{{$article->title}}" />
                    </a>
                </div>
                <div class="blog-content">
                    <h1><a href="#"></a></h1>
                    <div class="blog-meta">
                        <div class="date"><i class="fa fa-calendar-o"></i> {{$article->created_at->format('Y-m-d')}} </div>
                       <!-- <div class="views"><i class="fa fa-eye"></i>53</div> -->
                    </div>
                    <div class="blog-tags">
                        <a href="{{route('showSingleArticle',['lang' => $lang , 'id' => $article->id])}}"> {{$article->service->title}} </a>
                    </div>
                    <div class="blog-summary">
                    <a href="{{route('showSingleArticle',['lang' => $lang , 'id' => $article->id])}}"> 
                        {!! $article->title !!}
                    </a>    
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>


<script type="text/javascript" src="{{ asset('js/frontOffice/jquery-1.9.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/bootstrap.min.js') }}"></script>
     <script type="text/javascript" src="{{ asset('js/frontOffice/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/script.js?c=1') }}"></script>

@endsection