@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head')
@endsection

@section('header')
    @include('frontOffice.inc.header')
@endsection



@section('content')

<section class="breadcrumb-section wow fadeIn">
            <div class="container">
                <div class="breadcrumb-wrap">
                    <a href="{{route('showHome',['lang'=>$lang])}}">  {{ucfirst(trans('lang.home'))}} </a>
                    <i>/</i>
                    <span> {{ucfirst(trans('lang.contact_us'))}}  </span>
                </div>
            </div>
        </section>
        <section class="inner-page-section wow fadeIn">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="row contact-info">
                         
                            <div class="col-xs-12 col-md-4">
                                <a href="https://wa.me/00923009367790" class="info-item wow fadeInUp" data-wow-delay="0.2s">
                                    <i class="fa fa-whatsapp"></i>
                                    <span>  00923009367790 </span>
                                </a>
                            </div>

                            <div class="col-xs-12 col-md-4">
                                <a href="mailto:info@almughtrib.com" class="info-item wow fadeInUp" data-wow-delay="0.4s">
                                    <i class="fa fa-envelope-o"></i>
                                    <span> info@almughtrib.com </span>
                                </a>
                            </div>

                            <div class="col-xs-12 col-md-4">
                                <a href="https://t.me/00923009367790" class="info-item wow fadeInUp" data-wow-delay="0.2s">
                                    <i class="fa fa-telegram"></i>
                                    <span>  00923009367790 </span>
                                </a>
                            </div>

                          
                        </div>
                        <form id="form"  method="POST" action="{{route('handleUserAddContact',['lang' => $lang])}}" enctype="multipart/form-data">
                    {{ csrf_field() }} 
                        <div class="contact-form row">
                            <div class="col-xs-12 form-text wow fadeInUp">
                                <div class="MyFont">
                                    <div style="text-align: center;">
                                        <div style="line-height: 20.8px; text-align: center;"><span style="font-size:14px"><strong> {{ucfirst(trans('lang.contact_sentence'))}} </strong></span></div>
                                     <!--   <div style="line-height: 20.8px; text-align: center;"><span style="font-size:14px"><strong>أما الطلبات تتم من خلال صفحة &quot;<a href="#"><span style="color:#008000">طلب خدمة</span></a>&quot; فقط.</strong></span></div>
                                        <div style="line-height: 20.8px; text-align: center;"><span style="font-size:14px"><strong>هل ترغب في العمل معنا؟ <a href="#"><span style="color:#A52A2A">انضم إلينا</span>.</a></strong></span></div> -->
                                    </div>
                                </div>
                            </div>
                            <div id="ctl00_ContentPlaceHolder1_UpdatePanel1">
                                <div class="col-md-6 form-group wow fadeInUp">
                                    <label for="username">
                                         {{ ucfirst(trans('lang.full_name')) }}
                                        <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator4" class="color-red" style="color:Red;display:none;">*</span></label>
                                    <input name="name" type="text" id="ctl00_ContentPlaceHolder1_TxtName" class="input-item" />
                                </div>
                                <div class="col-md-6 form-group wow fadeInUp" data-wow-delay="0.2s">
                                    <label for="email">
                                    {{ ucfirst(trans('lang.email')) }}
                                        <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator5" class="color-red" style="color:Red;display:none;">*</span>
                                        <span id="ctl00_ContentPlaceHolder1_RegularExpressionValidator2" class="color-red MyFont" style="color:Red;display:none;">الرجاء إدخال بريد إلكتروني صحيح</span></label>
                                    <input name="email" type="text" id="ctl00_ContentPlaceHolder1_TxtEmail" class="input-item" />
                                </div>
                                <div class="col-md-12 form-group wow fadeInUp" data-wow-delay="0.4s">
                                    <label for="subject">
                                    {{ ucfirst(trans('lang.title')) }}
                                        <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator1" class="color-red" style="color:Red;display:none;">*</span></label>
                                    <input name="title" type="text" id="ctl00_ContentPlaceHolder1_TxtSubject" class="input-item" />
                                </div>
                                <div class="col-md-12 form-group wow fadeInUp">
                                    <label for="msg">
                                    {{ ucfirst(trans('lang.content')) }}
                                        <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator2" class="color-red" style="color:Red;display:none;">*</span></label>
                                    <textarea name="content" rows="5" cols="20" id="ctl00_ContentPlaceHolder1_TxtDetails" class="input-item"></textarea>
                                </div>
                          
                                <div class="col-md-12 form-group wow fadeInUp">
                                    <input type="submit" name="ctl00$ContentPlaceHolder1$btnSend" value="{{ucfirst(trans('lang.send'))}}" id="ctl00_ContentPlaceHolder1_btnSend" class="contact-btn btn btn-bordered" />
                                    <span id="ctl00_ContentPlaceHolder1_lblMsg" class="MyFont" style="font-weight: bold; margin-right: 50px; color: Green;"></span>
                                    <div id="ctl00_ContentPlaceHolder1_UpdateProgress1" style="display:none;">
                                        <img src="#" alt="loading" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>



@endsection