
@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head')
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection



@section('content')

        <section class="breadcrumb-section wow fadeIn">
            <div class="container">
                <div class="breadcrumb-wrap">
                    <a href="{{route('showManagerHome',['lang' => $lang])}}"> {{ucfirst(trans('lang.home'))}} </a>
                    <i>/</i>
                    <span> {{ucfirst(trans('lang.faq'))}}  </span>
                </div>
            </div>
        </section>
        <section class="about-section wow fadeIn">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="about-text" style="margin:0 !important;">
                            <h3 class="wow fadeInDown"> {{ucfirst(trans('lang.faq'))}}</h3>
                            <p class="wow fadeInDown">
                                {{ucfirst(trans('lang.faq-sentence'))}}
                            </p>
                        </div>
                    </div>
                </div>
        </section>
        <section class="inner-page-section wow fadeIn">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                       <!-- <div class="faq-ul">
                            <a href="#" data-to="all" class="active">الكل</a>
                            <a href="#" data-to="cat_1">خدمات الموقع </a>
                            <a href="#" data-to="cat_2">رسوم العمل </a>
                            <a href="#" data-to="cat_3">الضمانات </a>
                            <a href="#" data-to="cat_4">أسئلة عامة </a>
                        </div> -->
                        <div class="faq-items">

                        @foreach($faqs as $faq)
                            <div class="faq-item" data-cat="cat_1">
                                <div class="title">
                                    <a href="#">
                                        <i class="fa fa-plus"></i>
                                        <span> {{$faq->title}} </span>
                                    </a>
                                </div>
                                <div class="content">
                                    {{$faq->content}}
                                    <br />
                                </div>
                            </div>
                        @endforeach   
                  
               
                   
                          
                        
                      
                   
                        </div>
                    </div>
                </div>
            </div>
        </section>
    





        @endsection