@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => 'countries'
])
@endsection

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins') }}/datatable/datatable.css">
<script type="text/javascript" src="{{ asset('plugins') }}/datatable/datatable.js"></script>

<script type="text/javascript">
    /* Datatables responsive */

    $(document).ready(function() {
        $('#datatable-responsive').DataTable({
            responsive: true,
            language: {
                url: "{{ asset('plugins/datatable/lang/'.\Illuminate\Support\Facades\Session::get('youfors_applocale').'.js') }}"
            }
        });
        $('.dataTables_filter input').attr("placeholder", "{{ trans('lang.search') }}...");
    });
</script>


<div class="row">
        <div class="col-md-8">
            <div class="breadcrumb">
                <h1>{{ucfirst( trans('lang.countries')) }}</h1>
                <ul>
                    <li><a href="{{route('showManagerHome',['lang' => $lang])}}">{{ucfirst(trans('lang.dashboard')) }} </a></li>
                  
                </ul> 
            </div>
        </div>

     
    
    </div>



<div class="card-body">
    <div class="card-title mb-3">{{ ucfirst( trans('lang.new'))}}</div>
    <form id="form"  method="POST" 

    @isset($country)
       action="{{route('handleManagerUpdateCountry',['lang' => $lang])}}"
    @else 
       action="{{route('handleManagerAddCountry',['lang' => $lang])}}"
    @endisset
    
    
    
    enctype="multipart/form-data">
        {{ csrf_field() }} 
        <div class="row" >

                <input type="hidden" name="country" @isset($country) value="{{$country->id}}" @endisset>
            
            <div class="col-md-4 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.title'))}}</label>
                <input class="form-control" type="text"  name="title" id="title"  placeholder="{{trans('lang.title')}}" @isset($country) value="{{$country->title}}" @endisset required>
              
            </div>

            <div class="col-md-4 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.title_ar'))}}</label>
                <input class="form-control" type="text"  name="title_ar" id="title"  placeholder="{{trans('lang.title_ar')}}" @isset($country) value="{{$country->title_ar}}" @endisset required>
              
            </div>

         

        
           

          

 
          
            <div class="col-md-4 form-group mt-4">
                <input type="submit" class="btn btn-primary" value="{{ucfirst(trans('lang.send'))}}">
            </div>
        </div>
    </form>
</div>    

<div class="separator-breadcrumb border-top"></div>

<div class="row mb-4">

    <div class="col-md-12 mb-4">
        <div class="card text-left">

            <div class="card-body">
                <table id="datatable-responsive" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                       
                            <th>{{ trans('lang.title') }}</th>
                            <th>{{ trans('lang.title_ar') }}</th>
                            <th>{{ trans('lang.action') }}</th>
                         
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                           <th>{{ trans('lang.title') }}</th>
                            <th>{{ trans('lang.title_ar') }}</th>
                            <th>{{ trans('lang.action') }}</th>
                        </tr>
                    </tfoot>

                    <tbody>
                        @foreach($countries as $country)
                         <tr>
                            <td> {{ $country->title }} </td>
                            <td> {{ $country->title_ar }}  </td>
                          
               
                           <td>
                               <span class="badge badge-pill  p-2 m-1" title="{{ trans('lang.edit') }}" onclick="location='{{route('showManagerEditCountry',['lang' => $lang, 'id' => $country->id])}}';">    <img src="  {{asset('img/pencil.png')}}" alt="" style="width:25px"> </span>
                               <span class="badge badge-pill  p-2 m-1" title="{{ trans('lang.delete') }}" onclick="location='{{route('handleManagerDeleteCountry',['lang' => $lang, 'id' => $country->id])}}';">    <img src="  {{asset('img/remove.png')}}" alt="" style="width:25px"> </span>
                          
                            </td>

                            </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection