@extends('backOffice.layout')

@section('head')
    @include('backOffice.inc.head',
    ['title' => ' - Administration',
    'description' => 'Espace Administratif - '
    ])
@endsection

@section('header')
    @include('backOffice.inc.header')
@endsection

@section('sidebar')
    @include('backOffice.inc.sidebar', [
        'current' => 'dashboard'
    ])
@endsection

@section('content')

    <div class="breadcrumb">
        <h1>{{ trans('lang.home') }}</h1>
        <ul>
            <li><a href="#">{{ trans('lang.dashboard') }}</a></li>
        </ul>
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row">         <!-- ICON BG-->
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                    <div class="card-body text-center"><i class="i-Add-User"></i>
                        <div class="content">
                            <p class="text-muted mt-2 mb-0">{{ trans('lang.users') }}</p>
                            <p class="text-primary text-24 line-height-1 mb-2"> {{$users}} </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                    <div class="card-body text-center"><i class="i-Book"></i>
                        <div class="content">
                            <p class="text-muted mt-2 mb-0"> {{ ucfirst(trans('lang.consultations')) }} </p>
                            <p class="text-primary text-24 line-height-1 mb-2"> {{ $consults }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                    <div class="card-body text-center"><i class="i-Love-User"></i>
                        <div class="content">
                            <p class="text-muted mt-2 mb-0">  {{ucfirst(trans('lang.blog'))}} </p>
                            <p class="text-primary text-24 line-height-1 mb-2"> {{ $blog }} </p>
                        </div>
                    </div>
                </div>
            </div>
         
       
    </div>

  <br> <br> <br>

    <div class="row">
        <div class="col-md-8">
            <div class="breadcrumb">
                <h1>{{ucfirst( trans('lang.social_media')) }}</h1>
            </div>
        </div>
    </div>

<div class="card-body">
    <form id="form"  method="POST" action="{{route('handleMnagerSocialMedia',['lang' => $lang])}}" enctype="multipart/form-data">
        {{ csrf_field() }} 
        <div class="row" >
            
            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.facebook'))}}</label>
                <input class="form-control" type="url"  name="facebook" id="title" @isset($media) value="{{$media->facebook}}" @endisset placeholder="https://facebook.com" required>
            </div>

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.twitter'))}}</label>
                <input class="form-control" type="url"  name="twitter" id="title" @isset($media) value="{{$media->twitter}}" @endisset placeholder="https://twitter.com" required>
            </div>

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.instagram'))}}</label>
                <input class="form-control" type="url"  name="instagram" id="title" @isset($media) value="{{$media->instagram}}" @endisset placeholder="https://instagram.com" required>
            </div>

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.youtube'))}}</label>
                <input class="form-control" type="url"  name="youtube" id="title" @isset($media) value="{{$media->youtube}}" @endisset placeholder="https://youtube.com" required>
            </div>

            <div class="col-md-12">
                <input type="submit" class="btn btn-primary" value="{{ucfirst(trans('lang.send'))}}">
            </div>
        </div>
    </form>
</div>





@endsection