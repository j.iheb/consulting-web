@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => 'partner'
])
@endsection

@section('content')

<div class="row">
        <div class="col-md-8">
            <div class="breadcrumb">
                <h1>{{ucfirst( trans('lang.our_partners')) }}</h1>
                <ul>
                    <li><a href="#">{{ucfirst(trans('lang.dashboard')) }} </a></li>
                    <li>{{ucfirst( trans('lang.our_partners')) }}</li>
                </ul>
            </div>
        </div>
    
    </div>

<div class="card-body">
    <div class="card-title mb-3">{{ ucfirst( trans('lang.faq'))}}</div>
    <form id="form"  method="POST" 

    @isset($faq)
    action="{{route('handleManagerUpdatePartner',['lang' => $lang, 'id' => $partner->id])}}"
    @else 
    action="{{route('handleManagerAddPartner',['lang' => $lang])}}"
    @endisset
    
    
    
    enctype="multipart/form-data">
        {{ csrf_field() }} 
        <div class="row" >


            
            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.full_name'))}}</label>
                <input class="form-control" type="text"  name="name" id="title"  placeholder="{{trans('lang.full_name')}}" @isset($partner) value="{{$partner->name}}" @endisset required>
              
            </div>

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.full_name_ar'))}}</label>
                <input class="form-control" type="text"  name="name_ar" id="title"  placeholder="{{trans('lang.full_name')}}" @isset($partner) value="{{$partner->name_ar}}" @endisset required>
              
            </div>

         

        
            <div class="col-md-6 form-group mb-3">
                <label for="description">{{ucfirst( trans ('lang.description'))}}</label>
                <textarea class="form-control" name="description" id="description"  rows="10"  placeholder="{{trans('lang.description')}}" required>@isset($partner) {{$partner->description}} @endisset</textarea>
            </div>

            <div class="col-md-6 form-group mb-3">
                <label for="description">{{ucfirst( trans ('lang.description_ar'))}}</label>
                <textarea class="form-control" name="description_ar" id="description"  rows="10"  placeholder="{{trans('lang.description')}}" required>@isset($partner) {{$partner->description_ar}} @endisset</textarea>
            </div>

            <div class="col-md-6 form-group mb-3">
                <label for="cover">{{ucfirst( trans ('lang.cover'))}}</label>
                <input class="form-control-file" type="file" name="photo" id="cover" accept=".jpg,.jpeg,.png" >
            </div>

 
          
            <div class="col-md-12">
                <input type="submit" class="btn btn-primary" value="{{ucfirst(trans('lang.send'))}}">
            </div>
        </div>
    </form>
</div>

<script>
$("#form").validate({
    ignore: ":hidden",
    errorClass: "danger is-invalid",
    validClass: "success is-valid",
    highlight: function (element, errorClass, validClass) {
        $(element).addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass(errorClass).addClass(validClass);
    },
    errorPlacement: function (i, e) {
        i.insertAfter(e)
    },
    rules: {
        "title": {
            required: true,
            minlength: 3,
           
        },
        "description": {
            required: true,
            minlength: 10,
        },
        "players_per_match": {
            required: true,
        },
        "tree": {
            required: true,
        },
        "price": {
            required: true,         
        },
        "start": {
            required: true,
        },
        "end": {
            required: true,
        },
   
        "featured_picture": {
            extension: "jpg|jpeg|png",
            fileSizeMax: 2048000
        }
    },
    messages: {
        "title": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.title'))]) }}",
            minlength: "{{ trans('lang.min_string', ['string' =>   ucfirst(trans('shared.title')), 'min' => 3]) }}",
        },
        "description": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.description'))]) }}",
            minlength: "{{ trans('lang.min_string', ['string' =>   ucfirst(trans('shared.description')), 'min' => 10]) }}",
        },
        "players_per_match": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('Tournament::lang.players_per_match'))]) }}",
        
        },
        "tree": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.tree'))]) }}",
        },
      
        "price": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.price'))]) }}",
           
        },
        "start": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.start_date'))]) }}",
          
        },
        "end": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.end_date'))]) }}",
            
        },
        "start": {
            extension: "{{ trans('lang.mimes', ['file' =>  ucfirst(trans('shared.featured_picture')), 'mimes' => 'JPEG, JPG & PNG']) }}",
            fileSizeMax: "{{ trans('lang.max', ['file' => ucfirst(trans('shared.featured_picture')), 'size' => 2]) }}"
        }
    }
});
</script>
@endsection
