@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => config('app.name') . ' - ' . ucfirst(trans('shared.administration')),
'description' => config('app.name')
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => 'testimonial'
])
@endsection

@section('content')




<div class="breadcrumb">
    <h1> {{ucfirst(trans('lang.said_about_us'))}} </h1>
    <ul>
        <li><a href="{{ route('showManagerHome', ['lang' => $lang]) }}">{{ ucwords(trans('lang.dashboard')) }}</a>
        </li>

    </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
<form id="article" action="{{ route('handleManagerUpdateTestimotinial', ['lang'=> $lang]) }}" method="post" enctype="multipart/form-data">
    @csrf

    <div class="row">
        <div class="col-md-12">
            <div class="card mb-5">
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{ ucfirst(trans('lang.link')) }}</label>
                        <div class="col-sm-10">
                            <input class="form-control" value="{{ $testi->link }}" name="link" type="text" placeholder="{{ ucfirst(trans('lang.link')) }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{ucfirst( trans ('lang.cover'))}}</label>
                        <div class="col-sm-10">
                        <input class="form-control-file" type="file" name="file" id="cover" accept=".jpg,.jpeg,.png">
                     </div>
                    </div>


                    <input type="hidden" name="testi" value="{{$testi->id}}">




                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{ ucfirst(trans('lang.content')) }}</label>
                        <div class="col-sm-10">
                            <textarea id="article-ckeditor" required name="content" class="form-control" rows="6"> {!! $testi->content !!} </textarea>
                        </div>
                    </div>
                    <div class="col-md-12 pr-0">
                        <button class="btn btn-info m-1 float-right" type="submit">{{ ucfirst(trans('lang.save')) }}</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>



<script src="{{asset('js/backOffice/tiny.js')}}"></script>

<script>


tinymce.init({
  selector: 'textarea',
  height: 500,
  plugins: 'visualblocks',
  style_formats: [
    { title: 'Headers', items: [
      { title: 'h1', block: 'h1' },
      { title: 'h2', block: 'h2' },
      { title: 'h3', block: 'h3' },
      { title: 'h4', block: 'h4' },
      { title: 'h5', block: 'h5' },
      { title: 'h6', block: 'h6' }
    ] },

    { title: 'Blocks', items: [
      { title: 'p', block: 'p' },
      { title: 'div', block: 'div' },
      { title: 'pre', block: 'pre' }
    ] },

    { title: 'Containers', items: [
      { title: 'section', block: 'section', wrapper: true, merge_siblings: false },
      { title: 'article', block: 'article', wrapper: true, merge_siblings: false },
      { title: 'blockquote', block: 'blockquote', wrapper: true },
      { title: 'hgroup', block: 'hgroup', wrapper: true },
      { title: 'aside', block: 'aside', wrapper: true },
      { title: 'figure', block: 'figure', wrapper: true }
    ] }
  ],
  visualblocks_default_state: true,
  end_container_on_empty_block: true,
  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
});


</script>








@endsection