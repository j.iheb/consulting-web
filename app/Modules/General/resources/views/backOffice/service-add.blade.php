@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => 'services'
])
@endsection

@section('content')

<style>
    .input {
  text-align:right;
  unicode-bidi:bidi-override;
  direction:rtl;
}
</style>


<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
        
<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>

<script>
    $(function () {
        $("#article").validate({
            ignore: "input:hidden:not(input:hidden.required)",
            rules: {
                "title": {
                    required: true,
                    minlength: 2,
                    maxlength: 150
                },
                "short_description": {
                    required: true,
                    minlength: 10,
                    maxlength: 300
                },
                "content": {
                    required: true,
                    minlength: 100,
                },
                "cover": {
                    extension: "jpg|jpeg|png",
                    fileSizeMax: 2048000
                },
            },
            messages: {
                "title": {
                    required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.title'))]) }}",
                    minlength: "{{ trans('lang.min_string', ['string' =>   ucfirst(trans('shared.title')), 'min' => 2]) }}",
                    maxlength: "{{ trans('lang.max_string', ['string' =>   ucfirst(trans('shared.title')), 'min' => 150]) }}"
                },
                "short_description": {
                    required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('Blog::lang.short_description'))]) }}",
                    minlength: "{{ trans('lang.min_string', ['string' =>   ucfirst(trans('Blog::lang.short_description')), 'min' => 10]) }}",
                    maxlength: "{{ trans('lang.max_string', ['string' =>   ucfirst(trans('Blog::lang.short_description')), 'min' => 300]) }}"
                },
                "content": {
                    required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('Blog::lang.content'))]) }}",
                    minlength: "{{ trans('lang.min_string', ['string' =>   ucfirst(trans('Blog::lang.content')), 'min' => 100]) }}"
                },
                "cover": {
                    extension: "{{ trans('lang.file_mimes', ['file' =>  ucfirst(trans('Blog::lang.cover')), 'mimes' => 'JPEG, JPG & PNG']) }}",
                    fileSizeMax: "{{ trans('lang.file_size', ['file' => ucfirst(trans('Blog::lang.cover')), 'size' => 2]) }}"
                },
            }
        });

        CKEDITOR.replace('article-ckeditor', {
            height: 300,
            embed_provider: '//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}',
            filebrowserUploadUrl: "{{route('handleManagerAddArticleMedia', ['lang' => $lang, '_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
            @if ($lang =='ar')
            contentsLangDirection: 'rtl'
            @endif
        });

        CKEDITOR.replace('description-ckeditor', {
            height: 100,
            embed_provider: '//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}',
            filebrowserUploadUrl: "{{route('handleManagerAddArticleMedia', ['lang' => $lang, '_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
        });
    });
</script>


<div class="row">
        <div class="col-md-8">
            <div class="breadcrumb">
                <h1>{{ucfirst( trans('lang.services')) }}</h1>
                <ul>
                    <li><a href="#">{{ucfirst(trans('lang.dashboard')) }} </a></li>
                    <li>{{ucfirst( trans('lang.services')) }}</li>
                </ul>
            </div>
        </div>
        <div class="col-md-4">
            <div style="" class="btn btn-primary float-right">
                <a style="color: white"  href="{{route('showManagerServices',['lang' => $lang])}}">{{ucfirst(trans('lang.services')) }} </a>

            </div>
        </div>
    </div>

<div class="card-body">
    <div class="card-title mb-3">{{ ucfirst( trans('lang.add_service'))}}</div>
    <form id="form"  method="POST" action="{{route('handleManagerAddServices',['lang' => $lang])}}" enctype="multipart/form-data">
        {{ csrf_field() }} 
        <div class="row" >
            
            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.title'))}}</label>
                <input class="form-control @if ($lang == 'ar') input @endif" type="text"  name="title" id="title"  placeholder="{{trans('lang.title')}}" >
            </div>


            <div class="col-md-6 form-group mb-3">
                <label for="description">{{ucfirst( trans ('lang.short_description'))}}</label>
                <textarea class="form-control @if ($lang == 'ar') input @endif" name="short_description" id="description"  rows="5"  placeholder="{{ucfirst( trans ('lang.short_description'))}}" ></textarea>
            </div>


            <div class="col-md-6 form-group mb-3">
                <label for="description">{{ucfirst( trans ('lang.description'))}}</label>
                <textarea class="form-control @if ($lang == 'ar') input @endif"  id="format-hmtl5" name="description" id="description"  rows="5"  placeholder="{{trans('lang.description')}}" ></textarea>
            </div>



        
            <div class="col-md-6 form-group mb-3">
                <label for="price">{{ucfirst( trans ('lang.price'))}}</label>
                <input class="form-control @if ($lang == 'ar') input @endif"  type="number" name="price" id="price" placeholder="{{trans('lang.price')}}" >
            </div>
         
         
            <div class="col-md-6 form-group mb-3">
                <label for="cover">{{ucfirst( trans ('lang.featured_picture'))}}</label>
                <input class="form-control-file" type="file" name="cover" id="cover" accept=".jpg,.jpeg,.png" >
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary">{{ucfirst(trans('lang.submit'))}}</button>
            </div>
        </div>
    </form>
</div>

<script src="{{asset('js/backOffice/tiny.js')}}"></script>

<script>


tinymce.init({
  selector: 'textarea#format-hmtl5',
  height: 500,
  plugins: 'visualblocks',
  style_formats: [
    { title: 'Headers', items: [
      { title: 'h1', block: 'h1' },
      { title: 'h2', block: 'h2' },
      { title: 'h3', block: 'h3' },
      { title: 'h4', block: 'h4' },
      { title: 'h5', block: 'h5' },
      { title: 'h6', block: 'h6' }
    ] },

    { title: 'Blocks', items: [
      { title: 'p', block: 'p' },
      { title: 'div', block: 'div' },
      { title: 'pre', block: 'pre' }
    ] },

    { title: 'Containers', items: [
      { title: 'section', block: 'section', wrapper: true, merge_siblings: false },
      { title: 'article', block: 'article', wrapper: true, merge_siblings: false },
      { title: 'blockquote', block: 'blockquote', wrapper: true },
      { title: 'hgroup', block: 'hgroup', wrapper: true },
      { title: 'aside', block: 'aside', wrapper: true },
      { title: 'figure', block: 'figure', wrapper: true }
    ] }
  ],
  visualblocks_default_state: true,
  end_container_on_empty_block: true,
  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
});


</script>

@endsection
