@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => 'slider'
])
@endsection

@section('content')

<div class="row">
    <div class="col-md-8">
        <div class="breadcrumb">
            <h1>{{ucfirst( trans('lang.testimotinial')) }}</h1>
            <ul>
                <li><a href="#">{{ucfirst(trans('lang.dashboard')) }} </a></li>
           
            </ul>
        </div>
    </div>

</div>

<div class="card-body">
    <div class="card-title mb-3">{{ ucfirst( trans('lang.testimotinial'))}}</div>
    <form id="form" method="POST"  action="{{route('handleManagerAddTestimotinial',['lang' => $lang])}}"  enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
  <div class="col-md-6 form-group mb-3">
                <label for="description">{{ucfirst( trans ('lang.content'))}}</label>
                <textarea class="form-control" name="content" id="description" rows="10" placeholder="{{trans('lang.content')}}"></textarea>
            </div>

            <div class="col-md-6 form-group mb-3">
                <label for="description">{{ucfirst( trans ('lang.link'))}}</label>
                <input class="form-control" name="link" id="description" rows="10" placeholder="{{trans('lang.link')}}">
            </div>





            <div class="col-md-6 form-group mb-3">
                <label for="cover">{{ucfirst( trans ('lang.cover'))}}</label>
                <input class="form-control-file" type="file" name="file" id="cover" accept=".jpg,.jpeg,.png">
            </div>
            <div class="col-md-12">
                <input type="submit" class="btn btn-primary" value="{{ucfirst(trans('lang.send'))}}">
            </div>
        </div>
    </form>
</div>

<script src="{{asset('js/backOffice/tiny.js')}}"></script>

<script>


tinymce.init({
  selector: 'textarea',
  height: 500,
  plugins: 'visualblocks',
  style_formats: [
    { title: 'Headers', items: [
      { title: 'h1', block: 'h1' },
      { title: 'h2', block: 'h2' },
      { title: 'h3', block: 'h3' },
      { title: 'h4', block: 'h4' },
      { title: 'h5', block: 'h5' },
      { title: 'h6', block: 'h6' }
    ] },

    { title: 'Blocks', items: [
      { title: 'p', block: 'p' },
      { title: 'div', block: 'div' },
      { title: 'pre', block: 'pre' }
    ] },

    { title: 'Containers', items: [
      { title: 'section', block: 'section', wrapper: true, merge_siblings: false },
      { title: 'article', block: 'article', wrapper: true, merge_siblings: false },
      { title: 'blockquote', block: 'blockquote', wrapper: true },
      { title: 'hgroup', block: 'hgroup', wrapper: true },
      { title: 'aside', block: 'aside', wrapper: true },
      { title: 'figure', block: 'figure', wrapper: true }
    ] }
  ],
  visualblocks_default_state: true,
  end_container_on_empty_block: true,
  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
});


</script>


@endsection