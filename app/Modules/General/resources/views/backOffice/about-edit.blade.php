@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => 'consultations'
])
@endsection

@section('content')

<div class="row">
        <div class="col-md-8">
            <div class="breadcrumb">
                <h1>{{ucfirst( trans('lang.about_us')) }}</h1>
                <ul>
                    <li><a href="#">{{ucfirst(trans('lang.dashboard')) }} </a></li>
                    <li>{{ucfirst( trans('lang.about_us')) }}</li>
                </ul>
            </div>
        </div>
        <div class="col-md-4">
            <div style="" class="btn btn-primary float-right">
                <a style="color: white"  href="{{route('showManagerServices', ['lang' => $lang])}}">{{ucfirst(trans('lang.about_us')) }} </a>

            </div>
        </div>
    </div>

<div class="card-body">
    <div class="card-title mb-3">{{ ucfirst( trans('lang.add_service'))}}</div>
    <form id="form"  method="POST" action="{{route('handleManagerUpdateAbout',['lang' => $lang])}}" enctype="multipart/form-data">
        {{ csrf_field() }} 
        <div class="row" >

            <input type="hidden" name="id" value="{{$about->id}}">
            
            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.title'))}}</label>
                <input class="form-control" type="text"  name="title" id="title"  placeholder="{{trans('lang.tile_placeholder')}}" value="{{$about->title}}" required>
              
            </div>

            <div class="col-md-6 form-group mb-3">
                <label for="description">{{ucfirst( trans ('lang.short_description'))}}</label>
                <textarea class="form-control" name="short_description" id="format-hmtl5"   rows="5"  placeholder="{{trans('lang.description_placeholder')}}" required> {{$about->short_description}}</textarea>
            </div>

            <div class="col-md-6 form-group mb-3">
                <label for="description">{{ucfirst( trans ('lang.description'))}}</label>
                <textarea class="form-control" name="description" id="format-hmtl5"  rows="10"  placeholder="{{trans('lang.description_placeholder')}}" required> {{$about->description}}</textarea>
            </div>


         
         
            <div class="col-md-6 form-group mb-3">
                <label for="cover">{{ucfirst( trans ('lang.featured_picture'))}}</label>
                <input class="form-control-file" type="file" name="photo" id="cover" accept=".jpg,.jpeg,.png" >
            </div>
            <div class="col-md-12">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
        </div>
    </form>
</div>


<script src="{{asset('js/backOffice/tiny.js')}}"></script>

<script>


tinymce.init({
  selector: 'textarea',
  height: 500,
  plugins: 'visualblocks',
  style_formats: [
    { title: 'Headers', items: [
      { title: 'h1', block: 'h1' },
      { title: 'h2', block: 'h2' },
      { title: 'h3', block: 'h3' },
      { title: 'h4', block: 'h4' },
      { title: 'h5', block: 'h5' },
      { title: 'h6', block: 'h6' }
    ] },

    { title: 'Blocks', items: [
      { title: 'p', block: 'p' },
      { title: 'div', block: 'div' },
      { title: 'pre', block: 'pre' }
    ] },

    { title: 'Containers', items: [
      { title: 'section', block: 'section', wrapper: true, merge_siblings: false },
      { title: 'article', block: 'article', wrapper: true, merge_siblings: false },
      { title: 'blockquote', block: 'blockquote', wrapper: true },
      { title: 'hgroup', block: 'hgroup', wrapper: true },
      { title: 'aside', block: 'aside', wrapper: true },
      { title: 'figure', block: 'figure', wrapper: true }
    ] }
  ],
  visualblocks_default_state: true,
  end_container_on_empty_block: true,
  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
});


</script>

<script>
$("#form").validate({
    ignore: ":hidden",
    errorClass: "danger is-invalid",
    validClass: "success is-valid",
    highlight: function (element, errorClass, validClass) {
        $(element).addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass(errorClass).addClass(validClass);
    },
    errorPlacement: function (i, e) {
        i.insertAfter(e)
    },
    rules: {
        "title": {
            required: true,
            minlength: 3,
           
        },
        "description": {
            required: true,
            minlength: 10,
        },
        "players_per_match": {
            required: true,
        },
        "tree": {
            required: true,
        },
        "price": {
            required: true,         
        },
        "start": {
            required: true,
        },
        "end": {
            required: true,
        },
   
        "featured_picture": {
            extension: "jpg|jpeg|png",
            fileSizeMax: 2048000
        }
    },
    messages: {
        "title": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.title'))]) }}",
            minlength: "{{ trans('lang.min_string', ['string' =>   ucfirst(trans('shared.title')), 'min' => 3]) }}",
        },
        "description": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.description'))]) }}",
            minlength: "{{ trans('lang.min_string', ['string' =>   ucfirst(trans('shared.description')), 'min' => 10]) }}",
        },
        "players_per_match": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('Tournament::lang.players_per_match'))]) }}",
        
        },
        "tree": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.tree'))]) }}",
        },
      
        "price": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.price'))]) }}",
           
        },
        "start": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.start_date'))]) }}",
          
        },
        "end": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.end_date'))]) }}",
            
        },
        "start": {
            extension: "{{ trans('lang.mimes', ['file' =>  ucfirst(trans('shared.featured_picture')), 'mimes' => 'JPEG, JPG & PNG']) }}",
            fileSizeMax: "{{ trans('lang.max', ['file' => ucfirst(trans('shared.featured_picture')), 'size' => 2]) }}"
        }
    }
});
</script>
@endsection
