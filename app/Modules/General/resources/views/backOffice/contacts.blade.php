@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => 'contact'
])
@endsection

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins') }}/datatable/datatable.css">
<script type="text/javascript" src="{{ asset('plugins') }}/datatable/datatable.js"></script>

<script type="text/javascript">
    /* Datatables responsive */

    $(document).ready(function() {
        $('#datatable-responsive').DataTable({
            responsive: true,
            language: {
                url: "{{ asset('plugins/datatable/lang/'.\Illuminate\Support\Facades\Session::get('youfors_applocale').'.js') }}"
            }
        });
        $('.dataTables_filter input').attr("placeholder", "{{ trans('lang.search') }}...");
    });
</script>


<div class="row">
        <div class="col-md-8">
            <div class="breadcrumb">
                <h1>{{ucfirst( trans('lang.contact_us')) }}</h1>
                <ul>
                    <li><a href="{{route('showManagerHome',['lang' => $lang])}}">{{ucfirst(trans('lang.dashboard')) }} </a></li>
                  
                </ul> 
            </div>
        </div>

     
    </div>

<div class="separator-breadcrumb border-top"></div>

<div class="row mb-4">

    <div class="col-md-12 mb-4">
        <div class="card text-left">

            <div class="card-body">
                <table id="datatable-responsive" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                       
                            <th>{{ trans('lang.full_name') }}</th>
                            <th>{{ trans('lang.email') }}</th>
                            <th>{{ trans('lang.title') }}</th>
                            <th>{{ trans('lang.content') }}</th>
                            <th>{{ trans('lang.file') }}</th>
                            <th>{{ trans('lang.action') }}</th>
                         
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                           <th>{{ trans('lang.full_name') }}</th>
                            <th>{{ trans('lang.email') }}</th>
                            <th>{{ trans('lang.title') }}</th>
                            <th>{{ trans('lang.content') }}</th>
                            <th>{{ trans('lang.file') }}</th>
                            <th>{{ trans('lang.action') }}</th>
                        </tr>
                    </tfoot>

                    <tbody>
                        @foreach($contacts as $contact)
                         <tr>
                            <td> {{ $contact->name }} </td>
                            <td> <a href="mailto:{{$contact->email}}"> {{$contact->email}} </a>  </td>
                            <td> {{ $contact->title }}  </td>
                            <td> {{ $contact->content }}  </td>
                            <td> {{ $contact->file}} </td> 
                  
                           <td>
                              <span class="badge badge-pill  p-2 m-1" title="{{ trans('lang.delete') }}" onclick="location='{{route('handleManagerDeleteContact',['lang'=>$lang, 'id' => $contact->id])}}';">    <img src="  {{asset('img/remove.png')}}" alt="" style="width:25px"> </span>
                          
                            </td>

                            </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection