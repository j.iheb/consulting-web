<?php

namespace App\Modules\General\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Consult\Models\Consult;
use App\Modules\General\Models\Service;
use App\Modules\General\Models\About;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Brian2694\Toastr\Facades\Toastr;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use App\Modules\User\Models\User;
Use App\Modules\Blog\Models\Article;
use App\Modules\General\Models\Contact;
use App\Modules\General\Models\Country;
use App\Modules\General\Models\Faq;
use App\Modules\General\Models\JoinUs;
use App\Modules\General\Models\OurService;
use App\Modules\General\Models\Page;
use App\Modules\General\Models\Partner;
use App\Modules\General\Models\Slider;
use App\Modules\General\Models\SocialMedia;
use App\Modules\General\Models\Testimotinial;
use Facade\FlareClient\View;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Mail;

class WebController extends Controller
{



    public function showHome($lang)
    {
      
        return view('General::frontOffice.home', [
            'lang' => $lang,
            'services' => Service::where('lang',$lang)->where('status',1)->get(),
            'advisers' => User::whereHas('roles' , function ($q) { $q->where('title', 'adviser'); })->get(), 
            'officers' => User::whereHas('roles' , function ($q) { $q->where('title', 'officer'); })->get(), 
            'articles' => Article::where('lang', $lang)->where('status',1)->take(5)->get(), 
            'freeConsult' => Consult::where('type',0)->count(), 
            'paidConsult' => Consult::where('type',1)->count(), 
            'articleNumber' => Article::count(),
            'sliders' => Slider::where('lang', $lang)->where('model', 'home')->get(), 
            'partners' => Partner::all(),
            'ourServices' => OurService::where('lang', $lang)->get()

            
        ]);
    }


    public function showManagerHome($lang)
    {

        return view('General::backOffice.home', [
            'lang' => $lang,
            'users' => User::count(),
            'freeConsults' => Consult::where('type', 0)->count(),
            'consults' => Consult::where('type', 1)->count(),
            'blog' => Article::count(),
            'media' => SocialMedia::first(),
        ]);
    }

    public function showManagerServices($lang)
    {
        //  app()->setLocale('ar');
        return view('General::backOffice.services', [
            'services' => Service::all(),
            'lang' => $lang
        ]);
    }

    public function handleManagerAddServices($lang, Request $request)
    {


        $data = $request->all();


        $rules = [
            'title' => 'required|min:3',
            'description' => 'required|min:10',
            'short_description' => 'required|min:10',
            'price' => 'required|numeric',
      
        ];

        $messages = [
            'title.required' => trans('lang.required', ['string' => ucfirst(trans('lang.title'))]),
            'title.min' => trans('lang.min', ['string' => ucfirst(trans('lang.title'))]),
            'description.required' => trans('lang.required', ['string' => ucfirst(trans('lang.description'))]),
            'description.min' => trans('lang.min', ['string' => ucfirst(trans('lang.description'))]),

           'short_description.required' => trans('lang.required', ['string' => ucfirst(trans('lang.short_description'))]),
            'short_description.min' => trans('lang.min', ['string' => ucfirst(trans('lang.short_description'))]),


            'price.required' => trans('lang.required', ['string' => ucfirst(trans('lang.price'))]),
            'price.numeric' => trans('lang.numeric', ['string' => ucfirst(trans('lang.price'))]),
        ];

        $validation = Validator::make($data, $rules, $messages);


        if ($validation->fails())
        {
            return back()->withErrors($validation->errors()); 
        }


        if (isset($data['cover'])) {

           

            $fileName = time().'_'.$request->cover->getClientOriginalName();
            $filePath = $request->file('cover')->storeAs('/uploads/services/', $fileName, 'public');
            $path = '/storage/' . $filePath;

     

        }else{
            $path = null ; 
        }



        Service::create([
            'title' => $data['title'],
            'description' => $data['description'],
            'short_description' => $data['short_description'],
            'status' => 0,
            'photo' => $path,
            'price' => $data['price'],
            'lang' => $lang

        ]);

        Toastr::success(ucfirst(trans('shared.operation_success')));
        return redirect(route('showManagerServices', ['lang' => $lang]));
    }

    public function showManagerAddAbout($lang)
    {
        return View('General::backOffice.about-add', ['lang' => $lang]);
    }

    public function handleManagerAddAbout($lang, Request $request)
    {
        $data = $request->all();

        $rules = [
            'title' => 'required|min:3',
            'description' => 'required|min:10',


        ];

        $messages = [
            'title.required' => trans('lang.required', ['string' => ucfirst(trans('lang.title'))]),
            'title.min' => trans('lang.min', ['string' => ucfirst(trans('lang.title'))]),
            'description.required' => trans('lang.required', ['string' => ucfirst(trans('lang.description'))]),
            'description.min' => trans('lang.min', ['string' => ucfirst(trans('lang.description'))]),
        ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors());
        }

        $abouts = About::where('lang', $lang)->count();
        if (isset($data['photo'])) {



            $fileName = time().'_'.$request->photo->getClientOriginalName();
            $filePath = $request->file('photo')->storeAs('/uploads/about/', $fileName, 'public');
            $photoPath = '/storage/' . $filePath;
        } else {
            $photoPath = null;
        }

        if ($abouts > 0) {

            if (isset($data['photo'])) {

                $fileName = time().'_'.$request->photo->getClientOriginalName();
                $filePath = $request->file('photo')->storeAs('/uploads/about/', $fileName, 'public');
                $photoPath = '/storage/' . $filePath;
            }

            $about = About::where('lang', $lang)->first();
            $about->title = $data['title'];
            $about->description = $data['description'];

            $about->photo = $photoPath;
            $about->update();
            Toastr::success(trans('lang.operation_success'));
            return redirect(route('showManagerAboutList', ['lang' => $lang]));
        } else {

            $about = About::create([
                'title' => $data['title'],
                'description' => $data['description'],
                'photo' => $photoPath,
                'lang' => $lang
            ]);

            Toastr::success(trans('lang.operation_success'));
            return redirect(route('showManagerAboutList', ['lang' => $lang]));
        }
    }

    public function showManagerEditAbout($lang, $id)
    {
    
        $about = About::find($id);
        if ($about) {
            return View('General::backOffice.about-edit',['lang' => $lang, 'about' => $about]);
        } else {
            Toastr::error(trans('lang.operation_failed'));
            return back();
        }
    }

    public function handleManagerUpdateAbout($lang, Request $request)
    {
        $data = $request->all(); 
        
        $rules = [
            'title' => 'required', 
            'short_description' => 'required', 
            'description' => 'required'
        ]; 


        $messages = [
            'title.required' => trans('lang.required', ['string' => ucfirst(trans('lang.title'))]),
            'title.min' => trans('lang.min', ['string' => ucfirst(trans('lang.title'))]),
            'short_description.required' => trans('lang.required', ['string' => ucfirst(trans('lang.short_description'))]),
            'short_description.min' => trans('lang.min', ['string' => ucfirst(trans('lang.short_description'))]),
     
            'description.required' => trans('lang.required', ['string' => ucfirst(trans('lang.description'))]),
            'description.min' => trans('lang.min', ['string' => ucfirst(trans('lang.description'))]),
        ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails())
        {
            return redirect()->back()->withErrors($validation->errors());
        }

        $about = About::find($data['id']); 
        if ($about)
        {
            $about->title = $data['title']; 
            $about->short_description = $data['short_description'] ; 
            $about->description = $data['description']; 
            $about->update(); 
            Toastr::success(trans('operation_success')); 
            return redirect(route('showManagerAboutList',['lang' => $lang])); 
        }else {

            Toastr::error(trans('lang.operation_failed')); 
            return back(); 

        }



    }
    public function showManagerEditService($lang, $id)
    {
        return View('General::backOffice.service-edit', ['service' => Service::find($id), 'lang' => $lang]);
    }
    public function showManagerAddServices($lang)
    {
        return View('General::backOffice.service-add', [
            'lang' => $lang
        ]);
    }

    public function handleManagerSwitchServiceStatus($lang, $id)
    {
        $service = Service::find($id);
        if ($service->status == 1) {
            $service->status = 0;
            $service->update();
        } else {
            $service->status = 1;
            $service->update();
        }

        Toastr::success(trans('lang.status_switched'));
        return back();
    }

    public function handleManagerDeleteService($lang, $id)
    {
        $service = Service::find($id);
        if ($service) {
            $service->delete();
            Toastr::success(ucfirst(trans('lang.operation_success')));
            return redirect(route('showManagerServices',['lang' => $lang]));
        } else {
            Toastr::error('No Service Found');
            return redirect(route('showManagerServices',['lang' => $lang]));
        }
    }


    public function handleManagerUpdateService($lang, Request $request)
    {
        $data = $request->all();

        $rules = [
            'title' => 'required|min:3',
            'description' => 'required|min:10',
            'short_description' => 'required|min:10',
            'price' => 'required|numeric',

        ];

        $messages = [
            'title.required' => trans('lang.required', ['string' => ucfirst(trans('lang.title'))]),
            'title.min' => trans('lang.min', ['string' => ucfirst(trans('lang.title'))]),
            'short_description.required' => trans('lang.required', ['string' => ucfirst(trans('lang.short_description'))]),
            'short_description.min' => trans('validation.min', ['string' => ucfirst(trans('lang.short_description'))]),
            'description.required' => trans('lang.required', ['string' => ucfirst(trans('lang.description'))]),
            'description.min' => trans('lang.min', ['string' => ucfirst(trans('lang.description'))]),
            'price.required' => trans('lang.required', ['string' => ucfirst(trans('lang.price'))]),
            'price.numeric' => trans('lang.numeric', ['string' => ucfirst(trans('lang.price'))]),
        ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors());
        }

        if (isset($data['cover'])) {

           

            $fileName = time().'_'.$request->cover->getClientOriginalName();
            $filePath = $request->file('cover')->storeAs('/uploads/about/', $fileName, 'public');
            $path = '/storage/' . $filePath;

        }



        $service = Service::find($data['id']);

        if ($service){

            $service->title = $data['title'] ? $data['title'] : $service->title ;
            $service->description = $data['description'] ? $data['description'] : $service->description;
            $service->short_description = $data['short_description'] ? $data['short_description'] : $service->short_description;
            $service->price = $data['price'] ? $data['price'] : $service->price;
            $service->photo = isset($path) ? $path : $service->photo ; 
    
            $service->update();
            Toastr::success('Updated with success');
            return redirect(route('showManagerServices', ['lang' => $lang]));


        }else{
            Toastr::error(trans('lang.operation_failed')); 
            return back(); 
        }


   
    }

    public function showContact($lang)
    {
        return view('General::frontOffice.contact', ['lang' => $lang]);
    }

    public function showAbout($lang)
    {
        return view('General::frontOffice.about', [
            'lang' => $lang, 'about' => About::where('lang', $lang)->first(),
            'slides' => Slider::where('model', 'about_us')->where('lang', $lang)->get()
            ]);
    }

    public function showFaq($lang)
    {
        return view('General::frontOffice.faq', [
            'lang' => $lang, 
            'faqs' => Faq::where('lang',$lang)->get()
            ]);
    }

    public function showServices($lang)
    {
        return view('General::frontOffice.services', ['lang' => $lang, 'services' => Service::where('lang',$lang)->where('status',1)->get()]);
    }

    public function showTeam($lang)
    {
        return view('General::frontOffice.team', [
            'lang' => $lang, 
            'users' => User::whereHas('roles' , function ($q) {   $q->where('title', 'adviser');   })->get(), 
            'nb' =>  User::whereHas('roles' , function ($q) {   $q->where('title', 'adviser');   })->count(),  
            ]);
    }


    public function showTestimonial($lang)
    {
        return view('General::frontOffice.testimonial', [
            'lang' => $lang ,
            'sayes' => Testimotinial::where('lang', $lang)->get()]);
    }


    public function showJoinUs($lang)
    {

        if (Auth::check()) {
            return view('General::frontOffice.join-us', ['lang' => $lang]);
        }else{
           return redirect(route('showLogin',['lang'=>$lang, 'redirect' => 'showJoinUs'])); 
        }
        
      
    }

    public function showManagerCountries($lang)
    {
        return View('General::backOffice.countries',[
            'lang' => $lang, 
            'countries' => Country::all(), 
        ]); 
    }


    public function showManagerAddJoinUs($lang)
    {
        return View('General::backOffice.joinus-add',[
            'lang' => $lang, 
            'users' => User::all(), 
        ]); 
    }

 
    public function showManagerAboutList($lang)
    {
        return View('General::backOffice.about-list', ['lang' => $lang, 'abouts' => About::all()]);
    }


    public function showBlog($lang)
    {
        return view('General::frontOffice.blog', ['lang' => $lang, 'articles' => Article::where('lang', $lang)->where('status',1)->get()]);
    }

    public function showManagerTestimotinialList($lang)
    {
        return View('General::backOffice.saye-list',[
            'lang' => $lang, 
            'sayes' => Testimotinial::all(),
        ]) ; 
    }

    public function showManagerAddTestiomtinial($lang)
    {
        return View('General::backOffice.saye-add',[
            'lang' => $lang, 
        ]); 
    }
   
    public function handleManagerAddTestimotinial($lang, Request $request)
    {
        $data = $request->all(); 

        $rules = [
            'content' => 'required', 
        ];

        $messages = [
            'content.required' => trans('lang.required', ['string' => ucfirst(trans('lang.content'))]),
        ];

        $validation = Validator::make($data, $rules, $messages); 
        if ($validation->fails())
        {
            return redirect()->back()->withErrors($validation->errors()); 
        }

   

        if ($request->hasFile('file')) {

            $fileDown = $request['file'];
            $file = time() . '-article-' . $fileDown->getClientOriginalName();
            $fullFilePath = public_path('storage/uploads/joinus/' . $file);
            move_uploaded_file($fileDown->getRealPath(), $fullFilePath);
            $path = 'storage/uploads/joinus/' . $file ;
        
        }

        $testi = Testimotinial::create([
            'content' => $data['content'], 
            'photo' => isset($path) ? $path : null , 
            'link' => $data['link'], 
            'lang' => $lang, 
        ]); 


      
            Toastr::success(trans('lang.operation_success')); 
            return redirect(route('showManagerTestimotinialList',['lang' => $lang])); 
      
    }


    public function handleManagerDeleteTesti($lang, $id)
    {
        $test = Testimotinial::find($id)->delete(); 
        Toastr::success(trans('lang.operation_success')); 
        return redirect(route('showManagerTestimotinialList',['lang' => $lang])); 
    }
    public function showManagerEditTestimotinal($lang, $id)
    {
        return View('General::backOffice.saye-edit',[
            'lang' => $lang , 
            'testi' => Testimotinial::find($id),
        ]);
    }


    public function handleManagerUpdateTestimotinial($lang, Request  $request)
    {
        $data = $request->all(); 

        $rules = [
            'content' => 'required', 
        ];

        $messages = [
            'content.required' => trans('lang.required', ['string' => ucfirst(trans('lang.content'))]),
        ];

        $validation = Validator::make($data, $rules, $messages); 
        if ($validation->fails())
        {
            return redirect()->back()->withErrors($validation->errors()); 
        }

        $testi = Testimotinial::find($data['testi']); 


        if ($request->hasFile('file')) {

            $fileDown = $request['file'];
            $file = time() . '-article-' . $fileDown->getClientOriginalName();
            $fullFilePath = public_path('storage/uploads/joinus/' . $file);
            move_uploaded_file($fileDown->getRealPath(), $fullFilePath);
            $path = 'storage/uploads/joinus/' . $file ;
        
        }



        if ($testi)
        {
            $testi->content = $data['content']; 
            $testi->photo = isset($path) ? $path : $testi->photo ; 
            $testi->link = $data['link'] ? $data['link'] : null; 
            $testi->update(); 
            Toastr::success(trans('lang.operation_success')); 
            return redirect(route('showManagerTestimotinialList',['lang' => $lang])); 
        }
    }


    public function showSingleService($lang, $id)
    {
        $service = Service::find($id); 
        return View('General::frontOffice.service-single', [
            'lang' => $lang, 
            'service' => $service,
            'slides' => Slider::where('model', 'services')->where('model_id', $service->id)->get(), 
            ]); 
    }
    public function showManagerJoinUs($lang)
    {
        return View('General::backOffice.joinus-list', [
            'lang' => $lang, 
            'joinus' => JoinUs::all(), 
        ]);
    }

    public function handleManagerAcceptJoinUs($lang, $id)
    {
        $joinUs = JoinUs::find($id); 

       
        if ($joinUs->status == 0)
        {
            $joinUs->status = 1 ; 
            User::find($joinUs->user_id)->roles()->attach(3);
            $joinUs->update(); 
        }else {
            $joinUs->status = 0 ; 
            User::find($joinUs->user_id)->roles()->detach(3);;
            $joinUs->update(); 
        }
        Toastr::success(trans('lang.operation_success')); 
        return redirect(route('showManagerJoinUs',['lang' => $lang])); 
    }


    public function handleUserAddJoinUs($lang, Request $request)
    {
        $data = $request->all(); 

        $rules = [
            'content' => 'required', 
            
        ];
       
        $messages = [
            'content.required' => trans('lang.required', ['string' => ucfirst(trans('lang.content'))]),
        ];

        $validation = Validator::make($data, $rules, $messages); 
        if ($validation->fails())
        {
            return redirect()->back()->withErrors($validation->errors()); 
        }

        if ($request->hasFile('file')) {

            $fileDown = $request['file'];
            $file = time() . '-article-' . $fileDown->getClientOriginalName();
            $fullFilePath = public_path('storage/uploads/joinus/' . $file);
            move_uploaded_file($fileDown->getRealPath(), $fullFilePath);
            $path = 'storage/uploads/joinus/' . $file ;
        }else{

            $path = null ; 
        }


        $joinUs = JoinUs::create([
            'content' => $data['content'], 
            'file' => $path,
            'user_id' => Auth::id(),
            'status' => 0,
        ]); 

        Toastr::success(trans('lang.operation_success'));
        return back(); 
    }


    public function handleManagerAddJoinUs($lang, Request $request)
    {
        $data = $request->all(); 

        $rules = [
            'description' => 'required', 
            
        ];
       
        $messages = [
            'description.required' => trans('lang.required', ['string' => ucfirst(trans('lang.description'))]),
        ];

        $validation = Validator::make($data, $rules, $messages); 
        if ($validation->fails())
        {
            return redirect()->back()->withErrors($validation->errors()); 
        }

        if ($request->hasFile('file')) {

            $fileDown = $request['file'];
            $file = time() . '-article-' . $fileDown->getClientOriginalName();
            $fullFilePath = public_path('storage/uploads/joinus/' . $file);
            move_uploaded_file($fileDown->getRealPath(), $fullFilePath);
            $path = 'storage/uploads/joinus/' . $file ;
        }else{

            $path = null ; 
        }


        $joinUs = JoinUs::create([
            'content' => $data['description'], 
            'file' => $path,
            'user_id' => $data['user_id'],
            'status' => 0,
        ]); 

        Toastr::success(trans('lang.operation_success'));
        return  redirect(route('showManagerJoinUs',['lang' => $lang])) ; 
    }




    public function showManagerSliders($lang)
    {
        return View('General::backOffice.slider-list',[
            'sliders' => Slider::all(), 
            'lang' => $lang
        ]); 
    }

    public function showManagerAddSlider($lang)
    {
        return View('General::backOffice.slider',[
            'lang' => $lang,
            'services' => Service::where('lang', $lang)->get()
        ]);
    }

    public function  HandleManagerAddSlider($lang, Request $request)
    {
        $data = $request->all(); 

        $rules = [
            'title' => 'required', 
            'description' => 'required', 
            'photo' => 'required' , 
            'model' => 'required', 
        ] ; 


        $messages = [
            'title.required' => ucfirst(trans('lang.required', ['string' => trans('lang.title')])), 
            'description.required' => ucfirst(trans('lang.required', ['string' => trans('lang.description')])), 
            'photo.required' => ucfirst(trans('lang.required', ['string' => trans('lang.photo')])), 
            
        ] ; 


        $validation = Validator::make($data, $rules, $messages); 
        if ($validation->fails()){
            return back()->withErrors($validation->errors()); 
        }


        if ($request->hasFile('photo')) {

            $fileDown = $request['photo'];
            $file = time() . '-article-' . $fileDown->getClientOriginalName();
            $fullFilePath = public_path('storage/uploads/joinus/' . $file);
            move_uploaded_file($fileDown->getRealPath(), $fullFilePath);
            $path = 'storage/uploads/joinus/' . $file ;
        }


        $slider = Slider::create([
            'title' => $data['title'], 
            'description' => $data['description'], 
            'lang' => $lang, 
            'link' => isset($data['link']) ? $data['link'] : null , 
            'photo' => $path,
            'model' =>  $data['model'], 
            'model_id' => isset($data['model_id']) ? $data['model_id'] : null,
        ]);


        Toastr::success(trans('lang.operation_success')); 
        return redirect(route('showManagerSliders',['lang' => $lang])) ; 


        
    }


    public function showManagerSingleSlide($lang, $id)
    {
        $slider = Slider::find($id); 
        if ($slider){
            return View('General::backOffice.slider', [
                'slider' => $slider , 
                'lang' => $lang,
                'services' => Service::all(), 
                ]); 
        }
    }

    public function handleManagerUpdateSlider($lang, $id, Request $request)
    {
        $data = $request->all(); 

        $rules = [
            'title' => 'required', 
            'description' => 'required', 
       
        ] ; 


        $messages = [
            'title.required' => ucfirst(trans('lang.required', ['string' => trans('lang.title')])), 
            'description.required' => ucfirst(trans('lang.required', ['string' => trans('lang.description')])), 
            
        ] ; 


        $validation = Validator::make($data, $rules, $messages); 
        if ($validation->fails()){
            return back()->withErrors($validation->errors()); 
        }

        $slider = Slider::find($id); 


        if ($slider)
        {
            if ($request->hasFile('photo')) {

                $fileDown = $request['photo'];
                $file = time() . '-article-' . $fileDown->getClientOriginalName();
                $fullFilePath = public_path('storage/uploads/joinus/' . $file);
                move_uploaded_file($fileDown->getRealPath(), $fullFilePath);
                $path = 'storage/uploads/joinus/' . $file ;
            }


            $slider->title = $data['title']; 
            $slider->description = $data['description']; 
            $slider->link = isset($data['link']) ? $data['link'] : $slider->link ; 
            $slider->photo = isset($path) ? $path : $slider->photo ; 
            $slider->update(); 
            Toastr::success(ucfirst(trans('lang.operation_success'))); 
            return redirect(route('showManagerSliders',['lang' => $lang])); 

        }else{
            Toastr::error(ucfirst(trans('lang.operation_faild'))); 
            return back(); 
        }
    }


    public function handleManagerDeleteSlider($lang, $id){
        $slider = Slider::find($id); 
        if ($slider) 
        {
            $slider->delete(); 
            Toastr::success(ucfirst(trans('lang.operation_success'))); 
            return redirect(route('showManagerSliders',['lang' => $lang])); 
        }else{
            Toastr::error(ucfirst(trans('lang.operation_failed')));  
            return back(); 
        }
    }

    public function handleUserAddContact($lang, Request $request)
    {
        $data = $request->all(); 

        $rules = [
            'name' => 'required', 
            'email' => 'required', 
            'content' => 'required', 

        ] ; 

        $messages = [
            'name.required' => ucfirst(trans('lang.required',['string' => trans('lang.full_name')])), 
            'email.required' => ucfirst(trans('lang.required',['string' => trans('lang.email')])), 
            'content.required' => ucfirst(trans('lang.required',['string' => trans('lang.content')])), 
        ]; 

        $validation = Validator::make($data, $rules, $messages); 

        if ($validation->fails())
        {
            return back()->withErrors($validation->errors()); 
        }

        if ($request->hasFile('file')) {

            $fileDown = $request['file'];
            $file = time() . '-article-' . $fileDown->getClientOriginalName();
            $fullFilePath = public_path('storage/uploads/joinus/' . $file);
            move_uploaded_file($fileDown->getRealPath(), $fullFilePath);
            $path = 'storage/uploads/joinus/' . $file ;
        }

        $contact = Contact::create([
            'name' => $data['name'], 
            'email' => $data['email'], 
            'title' => $data['title'], 
            'content' => $data['content'],
            'file' => isset($path) ? $path : null,
        ]) ; 

        Toastr::success(trans('lang.operation_success')); 
        return redirect(route('showHome',['lang' => $lang])); 
    }


    public function showManagerContacts($lang)
    {
        return View('General::backOffice.contacts',[
            'lang' => $lang, 
            'contacts' => Contact::all(),
        ]); 
    }

    public function handleManagerDeleteContact($lang, $id)
    {
        $contact = Contact::find($id); 
        if ($contact)
        {
            $contact->delete(); 
            Toastr::success(trans('lang.operation_success')); 
            return back(); 
        }
    }


    public function showManagerFaqs($lang)
    {
        return View('General::backOffice.faqs',[
            'faqs' => Faq::all(), 
            'lang' => $lang
        ]); 
    }

    public function handleManagerAddFaq($lang, Request $request)
    {
        $data = $request->all();

        $rules = [
            'title' => 'required' , 
            'content' => 'required'
        ]; 

        $messages = [
            'title.required' => ucfirst(trans('lang.required',['string' => trans('lang.title')])), 
            'content.required' => ucfirst(trans('lang.required',['string'=> trans('lang.content')])),
        ]; 

        $validation = Validator::make($data, $rules, $messages); 

        if ($validation->fails())
        {
            return back()->withErrors($validation->errors()); 
        }

        $faq = Faq::create([
            'title' => $data['title'],
            'content' => $data['content'], 
            'lang' => $lang
        ]) ; 


        Toastr::success(ucfirst(trans('lang.operation_success'))); 
        return redirect(route('showManagerFaqs',['lang' => $lang])); 

    }

    public function showManagerAddFaq($lang)
    {
        return View('General::backOffice.faq-add',[
            'lang' => $lang,
        ]); 
    }


    public function showManagerEditFaq($lang, $id)
    {
        $faq = Faq::find($id); 
        if ($faq)
        {
            return View('General::backOffice.faq-add',[
                'lang' => $lang, 
                'faq' => $faq
            ]); 
        }
    }

    public function handleManagerUpdateFaq($lang, $id, Request $request)
    {
        $data = $request->all();

        $rules = [
            'title' => 'required' , 
            'content' => 'required'
        ]; 

        $messages = [
            'title.required' => ucfirst(trans('lang.required',['string' => trans('lang.title')])), 
            'content.required' => ucfirst(trans('lang.required',['string'=> trans('lang.content')])),
        ]; 

        $validation = Validator::make($data, $rules, $messages); 

        if ($validation->fails())
        {
            return back()->withErrors($validation->errors()); 
        }


        $faq = Faq::find($id); 

        if ($faq) {
            $faq->title = $data['title']; 
            $faq->content = $data['content'] ; 
            $faq->update(); 
           
        Toastr::success(ucfirst(trans('lang.operation_success'))); 
        return redirect(route('showManagerFaqs',['lang' => $lang])); 
        }else{
            Toastr::error(ucfirst(trans('lang.operation_failed'))); 
            return back(); 
        }
    }


    public function handleManagerDeleteFaq($lang, $id)
    {
        $faq = Faq::find($id); 
        if ($faq)
        {
            $faq->delete(); 
            Toastr::success(ucfirst(trans('lang.operation_success'))); 
            return redirect(route('showManagerFaqs',['lang' => $lang])); 
        }else{
            Toastr::error(ucfirst(trans('lang.operation_failed'))); 
            return back(); 
        }
    }


    public function showManagerPartners($lang)
    {
        return View('General::backOffice.partners',[
            'lang' => $lang,
            'partners' => Partner::all(),
        ]); 
    }

    public function showManagerAddPartner($lang)
    {
        return View('General::backOffice.partner',[
            'lang' => $lang
        ]); 
    }

    public function handleManagerAddPartner($lang, Request $request)
    {
        $data = $request->all(); 

        $rules = [
            'name' => 'required', 
            'description' => 'required', 
            'name_ar' => 'required', 
            'description_ar' => 'required', 
            'photo' => 'required'
              ] ; 

           $messages = [
                'name.required' => ucfirst(trans('lang.required',['string' => trans('lang.name')])), 
                'description.required' => ucfirst(trans('lang.required',['string'=> trans('lang.description')])),
                'name_ar.required' => ucfirst(trans('lang.required',['string' => trans('lang.name_ar')])), 
                'description_ar.required' => ucfirst(trans('lang.required',['string'=> trans('lang.description_ar')])),
                'photo.required' => ucfirst(trans('lang.required',['string' => trans('lang.photo')])), 
            ]; 
    
            $validation = Validator::make($data, $rules, $messages); 
    
            if ($validation->fails())
            {
                return back()->withErrors($validation->errors()); 
            }
            if (isset($data['photo'])) {
         

                    $fileName = time().'_'.$request->photo->getClientOriginalName();
                    $filePath = $request->file('photo')->storeAs('/uploads/about/', $fileName, 'public');
                    $path = '/storage/' . $filePath;
            }

            $partner = Partner::create([
                'name' => $data['name'], 
                'description' => $data['description'], 
                'name_ar' => $data['name_ar'], 
                'description_ar' => $data['description_ar'], 
                'photo' => $path
                
            ]); 



            Toastr::success(ucfirst(trans('lang.operation_success'))); 
            return redirect(route('showManagerPartners', ['lang' => $lang])); 
        
    }


    public function showManagerEditPartner($lang, $id)
    {   
        $partner = Partner::find($id); 
        if ($partner)
        {
            return View('General::backOffice.partner', [
                'lang' => $lang, 
                'partner' => $partner
            ]) ; 
        }else{
            Toastr::error(trans('lang.operation_failed'));
            return back(); 

        }
       
    }

    public function handleManagerEditPartner($lang, $id, $request)
    {

        $data = $request->all(); 

        $rules = [
            'name' => 'required', 
            'description' => 'required', 
            'name_ar' => 'required', 
            'description_ar' => 'required', 
            'photo' => 'required'
              ] ; 

           $messages = [
                'name.required' => ucfirst(trans('lang.required',['string' => trans('lang.name')])), 
                'description.required' => ucfirst(trans('lang.required',['string'=> trans('lang.description')])),
                'name_ar.required' => ucfirst(trans('lang.required',['string' => trans('lang.name_ar')])), 
                'description_ar.required' => ucfirst(trans('lang.required',['string'=> trans('lang.description_ar')])),
                'photo.required' => ucfirst(trans('lang.required',['string' => trans('lang.photo')])), 
            ]; 
    
            $validation = Validator::make($data, $rules, $messages); 
    
            if ($validation->fails())
            {
                return back()->withErrors($validation->errors()); 
            }
            if (isset($data['photo'])) {
            

                $fileName = time().'_'.$request->photo->getClientOriginalName();
                $filePath = $request->file('photo')->storeAs('/uploads/about/', $fileName, 'public');
                $path = '/storage/' . $filePath;
            }

            $partner = Partner::find($id); 
            if ($partner)
            {
                $partner->name = $data['name']; 
                $partner->description = $data['description'];
                $partner->name_ar = $data['name_ar']; 
                $partner->description_ar = $data['description_ar']; 
                $partner->photo =  isset($path) ? $path : $partner->photo ; 
                $partner->update(); 

                Toastr::success(ucfirst(trans('lang.operation_success'))); 
                return redirect(route('showManagerPartners', ['lang' => $lang])); 

            }
     

    }

    public function handleManagerDeletePartner($lang, $id)
    {
        $partner = Partner::find($id); 
        if ($partner)
        {
             $partner->delete(); 
            Toastr::success(ucfirst(trans('lang.operation_success'))); 
            return redirect(route('showManagerPartners', ['lang' => $lang])); 

        }else{
            Toastr::error(trans('lang.operation_failed'));
            return back(); 

        }
       

    }


    public function handleMnagerSocialMedia($lang, Request $request)
    {
        $data = $request->all(); 
        $social = SocialMedia::count(); 
        if ($social > 0)
        {
            $social = SocialMedia::first(); 
            $social->facebook = isset($data['facebook']) ? $data['facebook'] : $social->facebook ; 
            $social->twitter = isset($data['twitter']) ? $data['twitter'] : $social->twitter ; 
            $social->youtube = isset($data['youtube']) ? $data['youtube'] : $social->youtube ; 
            $social->instagram = isset($data['instagram']) ? $data['instagram'] : $social->instagram ; 
            $social->update(); 
            Toastr::success(ucfirst(trans('lang.operation_success'))); 
            return back(); 

         }else{

            $social = SocialMedia::create([
                'facebook' => isset($data['facebook']) ? $data['facebook'] : null,
                'twitter' => isset($data['twitter']) ? $data['twitter'] : null , 
                'instagram' =>isset($data['instagram']) ? $data['instagram'] : null , 
                'youtube' => isset($data['youtube']) ? $data['youtube'] : null ,  
            ]); 

            Toastr::success(trans('lang.operation_success')); 
            return back(); 

        }
    }


    public function handleManagerUpdatePage($lang, $id, Request $request)
    {
        $data = $request->all(); 
        $page = Page::find($id); 
        if ($page)
        {
            $page->name = $data['name']; 
            $page->name_ar = $data['name_ar']; 
            $page->update(); 
            Toastr::success(ucfirst(trans('lang.operation_success'))); 
            return redirect(route('showManagerPages', ['lang' => $lang])); 
        }
    }

    public function showManagerPages($lang)
    {
        return View('General::backOffice.pages',[
            'pages' => Page::all(), 
            'lang' => $lang
        ]); 
    }

    public function showManagerEditPage($lang, $id)
    {
        $page = Page::find($id);
        if ($page)
        {
            return View('General::backOffice.page',[
                'page' => $page, 
                'lang' => $lang
            ]); 
        }
       
    }


    public function showManagerOurServices($lang)
    {
        return View('General::backOffice.our-services',[
            'services' => OurService::all(), 
            'lang' => $lang
        ]);
    }




    public function showManagerEditOurService($lang, $id)
    {
        return View('General::backOffice.our-service-add', ['service' => OurService::find($id), 'lang' => $lang]);
    }
    public function showManagerAddOurServices($lang)
    {
        return View('General::backOffice.our-service-add', [
            'lang' => $lang
        ]);
    }

 
    public function handleManagerAddOurService($lang, Request $request)
    {
        $data = $request->all();

        $rules = [
            'title' => 'required',
            'description' => 'required',
   

        ];

        $messages = [
            'title.required' => trans('lang.required', ['string' => ucfirst(trans('lang.title'))]),
            'description.required' => trans('lang.required', ['string' => ucfirst(trans('lang.description'))]),
       ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors());
        }

    


        $service = OurService::create([
            'title' => $data['title'], 
            'description' => $data['description'], 
            'lang' => $lang

        ]); 


        Toastr::success(ucfirst(trans('lang.operation_success'))); 
        return redirect(route('showManagerOurServices',['lang' => $lang])); 
    }

 

    public function handleManagerDeleteOurService($lang,$id)
    {
        $service = OurService::find($id);
        if ($service) {
            $service->delete();
            Toastr::success('Deleted with success');
            return redirect(route('showManagerOurServices',['lang' => $lang]));
        } else {
            Toastr::error('No Service Found');
            return redirect(route('showManagerServices', ['lang' => $lang]));
        }
    }


    public function handleManagerUpdateOurService($lang, $id,  Request $request)
    {
        $data = $request->all();

        $rules = [
            'title' => 'required',
            'description' => 'required',

        ];

        $messages = [
            'title.required' => trans('lang.required', ['string' => ucfirst(trans('lang.title'))]),
           'description.required' => trans('lang.required', ['string' => ucfirst(trans('lang.description'))]),
       ];

        $validation = Validator::make($data, $rules, $messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors());
        }



        $service = OurService::find($id);

        if ($service){

            $service->title = $data['title'] ? $data['title'] : $service->title ;
            $service->description = $data['description'] ? $data['description'] : $service->description;
        
            $service->update();
            Toastr::success('Updated with success');
            return redirect(route('showManagerOurServices', ['lang' => $lang]));


        }else{
            Toastr::error(trans('lang.operation_failed')); 
            return back(); 
        }


   
    }

    public function showManagerEditCountry($lang, $id)
    {
        $country = Country::find($id); 
        if ($country)
        {
            return View('General::backOffice.countries', [
                'country' => $country, 
                'lang' => $lang, 
                'countries' => Country::all()
            ]); 
        }
    }

    


    public function handleManagerUpdateCountry($lang, Request $request)
    {
        $data = $request->all(); 

        $rules = [
            'title' => 'required', 
            'title_ar' => 'required'
        ]; 

        $messages =  [
            'title.required' => ucfirst(trans('lang.required',['string' => trans('lang.title')])), 
            'title_ar.required' => ucfirst(trans('lang.required',['string' => trans('lang.title_ar')]))
        ]; 

        $validation = Validator::make($data, $rules, $messages); 
        if ($validation->fails())
        {
            return back()->withErrors($validation->errors()); 
        }

        $country = Country::find($data['country']); 

        if ($country)
        {
            $country->title = $data['title']; 
            $country->title_ar = $data['title_ar']; 
            $country->update(); 
            Toastr::success(ucfirst(trans('lang.operation_success'))); 
            return redirect(route('showManagerCountries',['lang' => $lang])); 
        }

    }


    public function handleManagerAddCountry($lang, Request $request)
    {
        $data = $request->all(); 

        $rules = [
            'title' => 'required', 
            'title_ar' => 'required'
        ]; 

        $messages =  [
            'title.required' => ucfirst(trans('lang.required',['string' => trans('lang.title')])), 
            'title_ar.required' => ucfirst(trans('lang.required',['string' => trans('lang.title_ar')]))
        ]; 

        $validation = Validator::make($data, $rules, $messages); 
        if ($validation->fails())
        {
            return back()->withErrors($validation->errors()); 
        }

        $country = Country::create([
            'title' => $data['title'], 
            'title_ar' => $data['title_ar'],
        ]); 

        Toastr::success(ucfirst(trans('lang.operation_success'))); 
        return redirect(route('showManagerCountries', ['lang' => $lang])); 

        
    }


    public function handleManagerDeleteCountry($lang, $id)
    {
       $country = Country::find($id)->delete(); 
       Toastr::success(ucfirst(trans('lang.operation_success'))); 
       return redirect(route('showManagerCountries', ['lang' => $lang])); 
    }


}
