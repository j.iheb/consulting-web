<?php

use Illuminate\Support\Facades\Route; 

Route::group(['module' => 'General', 'middleware' => ['web'], 'prefix' => '{lang}'], function () {


Route::get('/', 'WebController@showHome')->name('showHome');
Route::get('/contact', 'WebController@showContact')->name('showContact'); 
Route::get('/about', 'WebController@showAbout')->name('showAbout'); 
Route::get('/faq', 'WebController@showFaq')->name('showFaq'); 
Route::get('/services', 'WebController@showServices')->name('showServices'); 
Route::get('/team', 'WebController@showTeam')->name('showTeam'); 
Route::get('/testimonial', 'WebController@showTestimonial')->name('showTestimonial'); 
Route::get('/join-us', 'WebController@showJoinUs')->name('showJoinUs'); 


Route::get('/blog', 'WebController@showBlog')->name('showBlog'); 

Route::get('/service/{id}', 'WebController@showSingleService')->name('showSingleService'); 


// manager 

Route::get('/manager/testimotinial/list', 'WebController@showManagerTestimotinialList')->name('showManagerTestimotinialList'); 
Route::post('/manager/testimotinial/update', 'WebController@handleManagerUpdateTestimotinial')->name('handleManagerUpdateTestimotinial'); 
Route::get('/manager/testimotinial/edit/{id}', 'WebController@showManagerEditTestimotinal')->name('showManagerEditTestimotinal'); 
Route::get('/manager/testimotinial/show/add', 'WebController@showManagerAddTestiomtinial')->name('showManagerAddTestiomtinial'); 
Route::post('/manager/testimotinial/add', 'WebController@handleManagerAddTestimotinial')->name('handleManagerAddTestimotinial'); 
Route::get('/manager/testimotinial/delete/{id}', 'WebController@handleManagerDeleteTesti')->name('handleManagerDeleteTesti'); 




Route::get('/manager/join-us', 'WebController@showManagerJoinUs')->name('showManagerJoinUs'); 
Route::post('/manager/join-us/add', 'WebController@handleUserAddJoinUs')->name('handleUserAddJoinUs'); 

Route::get('/manager/join-us/accept/{id}', 'WebController@handleManagerAcceptJoinUs')->name('handleManagerAcceptJoinUs'); 

// service 

Route::get('/manager', 'WebController@showManagerHome')->name('showManagerHome')->middleware('auth', 'manager');
Route::get('/manager/services', 'WebController@showManagerServices')->name('showManagerServices')->middleware('auth', 'manager');
Route::get('/manager/services/show/add', 'WebController@showManagerAddServices')->name('showManagerAddServices')->middleware('auth', 'manager');
Route::post('/manager/services/add', 'WebController@handleManagerAddServices')->name('handleManagerAddServices')->middleware('auth', 'manager');
Route::get('/manager/services/switch/status/{id}', 'WebController@handleManagerSwitchServiceStatus')->name('handleManagerSwitchServiceStatus')->middleware('auth', 'manager');
Route::get('/manager/services/edit/{id}', 'WebController@showManagerEditService')->name('showManagerEditService')->middleware('auth', 'manager');
Route::get('/manager/services/delete/{id}', 'WebController@handleManagerDeleteService')->name('handleManagerDeleteService')->middleware('auth', 'manager');
Route::post('/manager/services/update', 'WebController@handleManagerUpdateService')->name('handleManagerUpdateService')->middleware('auth', 'manager');

// about 

Route::get('/manager/about/us', 'WebController@showManagerAboutList')->name('showManagerAboutList')->middleware('auth', 'manager'); 
Route::get('/manager/about/show/add', 'WebController@showManagerAddAbout')->name('showManagerAddAbout')->middleware('auth','manager');
Route::post('/manager/about/add', 'WebController@handleManagerAddAbout')->name('handleManagerAddAbout')->middleware('auth', 'manager'); 

Route::get('/manager/about/edit/{id}', 'WebController@showManagerEditAbout')->name('showManagerEditAbout')->middleware('auth', 'manager'); 
Route::post('/manager/about/update', 'WebController@handleManagerUpdateAbout')->name('handleManagerUpdateAbout')->middleware('auth', 'manager');


//silder 

Route::get('/manager/sliders', 'WebController@showManagerSliders')->name('showManagerSliders')->middleware('auth', 'manager'); 
Route::get('/manager/slider/edit/{id}', 'WebController@showManagerSingleSlide')->name('showManagerSingleSlide')->middleware('auth', 'manager');
Route::get('/manager/slider/show/add', 'WebController@showManagerAddSlider')->name('showManagerAddSlider')->middleware('auth', 'manager');
Route::post('/manager/slider/add', 'WebController@HandleManagerAddSlider')->name('HandleManagerAddSlider')->middleware('auth', 'manager');
Route::post('/manager/slider/update/{id}', 'WebController@handleManagerUpdateSlider')->name('handleManagerUpdateSlider')->middleware('auth', 'manager');
Route::get('/manager/slider/delete/{id}', 'WebController@handleManagerDeleteSlider')->name('handleManagerDeleteSlider')->middleware('auth', 'manager');



//contact 

Route::get('/manager/contacts', 'WebController@showManagerContacts')->name('showManagerContacts')->middleware('auth', 'manager');
Route::get('/manager/contacts/delete/{id}', 'WebController@handleManagerDeleteContact')->name('handleManagerDeleteContact')->middleware('auth', 'manager');
Route::post('/manager/contact/add', 'WebController@handleUserAddContact')->name('handleUserAddContact')->middleware('auth', 'manager');


//faq 

Route::get('/manager/faqs', 'WebController@showManagerFaqs')->name('showManagerFaqs')->middleware('auth', 'manager');
Route::get('/manager/faq/delete/{id}', 'WebController@handleManagerDeleteFaq')->name('handleManagerDeleteFaq')->middleware('auth', 'manager');
Route::post('/manager/faq/add', 'WebController@handleManagerAddFaq')->name('handleManagerAddFaq')->middleware('auth', 'manager');

Route::get('/manager/faqs/show/add', 'WebController@showManagerAddFaq')->name('showManagerAddFaq')->middleware('auth', 'manager');
Route::get('/manager/faq/edit/{id}', 'WebController@showManagerEditFaq')->name('showManagerEditFaq')->middleware('auth', 'manager');
Route::post('/manager/faq/update', 'WebController@handleManagerUpdateFaq')->name('handleManagerUpdateFaq')->middleware('auth', 'manager');


//partner

Route::get('/manager/partners', 'WebController@showManagerPartners')->name('showManagerPartners')->middleware('auth', 'manager');
Route::get('/manager/partner/delete/{id}', 'WebController@handleManagerDeletePartner')->name('handleManagerDeletePartner')->middleware('auth', 'manager');
Route::post('/manager/partner/add', 'WebController@handleManagerAddPartner')->name('handleManagerAddPartner')->middleware('auth', 'manager');

Route::get('/manager/partner/show/add', 'WebController@showManagerAddPartner')->name('showManagerAddPartner')->middleware('auth', 'manager');
Route::get('/manager/partner/edit/{id}', 'WebController@showManagerEditPartner')->name('showManagerEditPartner')->middleware('auth', 'manager');
Route::post('/manager/partner/update', 'WebController@handleManagerEditPartner')->name('handleManagerEditPartner')->middleware('auth', 'manager');

Route::post('/manager/social/media', 'WebController@handleMnagerSocialMedia')->name('handleMnagerSocialMedia')->middleware('auth', 'manager');


//pages 


Route::get('/manager/pages', 'WebController@showManagerPages')->name('showManagerPages')->middleware('auth', 'manager');
Route::get('/manager/page/{id}', 'WebController@showManagerEditPage')->name('showManagerEditPage')->middleware('auth', 'manager');
Route::post('/manager/page/update/{id}', 'WebController@handleManagerUpdatePage')->name('handleManagerUpdatePage')->middleware('auth', 'manager');


//our service  



Route::get('/manager/our/services', 'WebController@showManagerOurServices')->name('showManagerOurServices')->middleware('auth', 'manager');
Route::get('/manager/our/services/show/add', 'WebController@showManagerAddOurServices')->name('showManagerAddOurServices')->middleware('auth', 'manager');
Route::post('/manager/our/services/add', 'WebController@handleManagerAddOurService')->name('handleManagerAddOurService')->middleware('auth', 'manager');
Route::get('/manager/our/services/edit/{id}', 'WebController@showManagerEditOurService')->name('showManagerEditOurService')->middleware('auth', 'manager');
Route::get('/manager/our/services/delete/{id}', 'WebController@handleManagerDeleteOurService')->name('handleManagerDeleteOurService')->middleware('auth', 'manager');
Route::post('/manager/our/services/update/{id}', 'WebController@handleManagerUpdateOurService')->name('handleManagerUpdateOurService')->middleware('auth', 'manager');



//join us 

Route::get('/manager/show/add/join/us', 'WebController@showManagerAddJoinUs')->name('showManagerAddJoinUs')->middleware('auth', 'manager');
Route::post('/manager/add/join/', 'WebController@handleManagerAddJoinUs')->name('handleManagerAddJoinUs')->middleware('auth', 'manager');

//countries 
Route::get('/manager/countries', 'WebController@showManagerCountries')->name('showManagerCountries')->middleware('auth', 'manager');
Route::get('/manager/country/delete/{id}', 'WebController@handleManagerDeleteCountry')->name('handleManagerDeleteCountry')->middleware('auth', 'manager');
Route::get('/manager/country/edit/{id}', 'WebController@showManagerEditCountry')->name('showManagerEditCountry')->middleware('auth', 'manager');
Route::post('/manager/country/update', 'WebController@handleManagerUpdateCountry')->name('handleManagerUpdateCountry')->middleware('auth', 'manager');
Route::post('/manager/country/add', 'WebController@handleManagerAddCountry')->name('handleManagerAddCountry')->middleware('auth', 'manager');




    
    


});


