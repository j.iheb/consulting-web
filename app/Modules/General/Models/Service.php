<?php

namespace App\Modules\General\Models;


use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;


use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use Notifiable;
    use SoftDeletes;

 

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'services';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'lang',
        'short_description',
        'status', 
        'created_at',
        'photo',
        'price', 
   
    ];





}
