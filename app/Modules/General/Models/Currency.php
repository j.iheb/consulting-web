<?php

namespace App\Modules\General\Models;

use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class Currency extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'currencies';

    protected $appends = ['hashed_id'];

    public function getHashedIdAttribute()
    {
        return $this->attributes['hashed_id'] = Hashids::encodeHex($this->attributes['id']);
    }

    protected $hidden = [
        'id'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'priority',
        'iso_code',
        'symbol',
        'subunit',
        'subunit_to_unit',
        'symbol_first',
        'html_entity',
        'decimal_mark',
        'thousands_separator',
        'iso_numeric',
    ];



}
